package com.yfhms.Utils;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;
 
/**
 * 自己定义的文本
 * @author Administrator
 */
public class MyDocument {
 
    /**
     * 能输入小数的文本
     */
    public static class DoubleOnlyDocument extends PlainDocument {
 
        @Override
        public void insertString(int offset, String s, AttributeSet attrSet) throws BadLocationException {
            //获得文本框中的值
            String text = this.getText(0, offset);
            String reg = "\\.";
            Pattern pat = Pattern.compile(reg);
            Matcher mat = pat.matcher(text);
            
            //判断文本框中是否含有小数点
            boolean point = false;
            if(mat.find()) {
                point = true;
            }
            
            //如果偏移量不为0，并且无小数点时，如果当前输入的为小数点，则添加进文本框并返回，不进行之后的操作
            if(offset != 0 && !point) {
                if(s.equals(".")) {
                    //调用父类方法将字符插入文本框中
                    super.insertString(offset, s, attrSet);
                    return;
                }
            }
            
            //将字符串解析成数字是否会抛出异常，抛出异常则说明该字符不为数字，返回，否则插入文本框中
            try {
                Integer.parseInt(s);
            } catch (NumberFormatException ex) {
                return;
            }
            super.insertString(offset, s, attrSet);
        }
    }
 
    /**
     * 只能输入数字
     */
    public static class NumOnlyDocument extends PlainDocument {
        @Override
        public void insertString(int offset, String s, AttributeSet attrSet) throws BadLocationException {
            try {
                Integer.parseInt(s);
            } catch (NumberFormatException ex) {
                return;
            }
            super.insertString(offset, s, attrSet);
        }
    }
    //使用正则表达式限制swing (JTextField等) 的输入
    public static class MyRegExp extends PlainDocument {
        private Pattern pattern;
        private Matcher m;
        public MyRegExp(String pat)
        {
            super();
            this.pattern=Pattern.compile(pat);
        }
        @Override
        public void insertString
        (int offset, String str, AttributeSet attr)
                throws BadLocationException {   
            if (str == null){
                return;
            }
            String tmp=getText(0, offset).concat(str);
            m=pattern.matcher(tmp);
            if(m.matches())
                super.insertString(offset, str, attr);
        }
    }
//   public class TextFieldInputListener implements CaretListener {
//	    @Override
//   	    public void caretUpdate(CaretEvent e) {
//   	        JTextField textField = (JTextField) e.getSource(); // 获得触发事件的 JTextField
//   	        String text = textField.getText();
//   	        if (text.length() == 0) {
//   	            return;
//   	        }
//   	        char ch = text.charAt(text.length() - 1);
//   	        if (!(ch >= '0' && ch <= '9' // 数字
//   	                || ch >= 'A' && ch <= 'z' // 字母
//   	                || ch >= '\u4E00' && ch <= '\u9FA5')) { // 中文，最常用的范围是 U+4E00～U+9FA5，也有使用 U+4E00～ U+9FFF 的，但目前 U+9FA6～U+9FFF 之间的字符还属于空码，暂时还未定义，但不能保证以后不会被定义
//   	            JOptionPane.showMessageDialog(textField, "只能输入中文,字母,数字", "提示", JOptionPane.INFORMATION_MESSAGE);
//   	            SwingUtilities.invokeLater(new Runnable() {
//   	                @Override
//   	                public void run() {
//   	                    // 去掉 JTextField 中的末尾字符
//   	                    textField.setText(text.substring(0, text.length() - 1));
//   	                }
//   	            });
//   	        }
//   	    }
//    }
    
    
}