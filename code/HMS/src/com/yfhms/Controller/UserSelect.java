package com.yfhms.Controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.yfhms.Bean.Customertype;
import com.yfhms.Bean.UserInfo;
import com.yfhms.DAO.DbConnection;

public class UserSelect {
	//查询操作员方法
	public static Object[][] geUserInfo(){
		String sql="SELECT account, usertype from userinfo";
		ResultSet resultSet=DbConnection.query(sql);
		ArrayList<UserInfo> list=new ArrayList<UserInfo>();
		try {
			while (resultSet.next()) {
				UserInfo userinfo=new UserInfo();
				userinfo.setAccount(resultSet.getString(1));
				userinfo.setUserType(resultSet.getString(2));
				list.add(userinfo);
			}
	}catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		//泛型集合，转化为二维数组
	Object[][] body=new Object[list.size()][2];
	for (int i = 0; i < list.size(); i++) {
		body[i][0]=list.get(i).getAccount();
		body[i][1]=list.get(i).getUserType();
	}
	return body;
}
}
