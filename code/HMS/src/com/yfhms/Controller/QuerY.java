package com.yfhms.Controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.yfhms.Bean.Checkout;
import com.yfhms.Bean.Customerinfo;
import com.yfhms.Bean.Customertype;
import com.yfhms.Bean.Field;
import com.yfhms.Bean.Livein;
import com.yfhms.Bean.P_state;
import com.yfhms.Bean.Query2;
import com.yfhms.Bean.Query3;
import com.yfhms.Bean.R_State;
import com.yfhms.Bean.Reserve;
import com.yfhms.Bean.RoomInfo;
import com.yfhms.Bean.RoomType;
import com.yfhms.Bean.UserInfo;
import com.yfhms.DAO.DbConnection;

//查询数据
public class QuerY {
	//结账单查询
	

		public static Object[][] getSelect1(String sql1){
			DbConnection dbc =new DbConnection();
			ResultSet rst=DbConnection.query(sql1);
			ArrayList<Query2> ls=new ArrayList<Query2> ();
			try {
				while (rst.next()) {
				    Query2 query2=new Query2();
				    query2.setC_id(rst.getInt(1));
				    query2.setRoomid(rst.getInt(2));
				    query2.setcName(rst.getString(3));
				    query2.setC_sex(rst.getString(4));
				    query2.setD_type(rst.getString(5));
				    query2.setNo(rst.getString(6));
				    query2.setNumber(rst.getInt(7));
				    query2.setForegift(rst.getDouble(8));
				    query2.setDays(rst.getInt(9));
				    query2.setState(rst.getString(10));
				    query2.setIn_time(rst.getDate(11));
				    query2.setChk_time(rst.getDate(12));
				    query2.setChk_no(rst.getInt(13));
					ls.add(query2);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Object[][] objects=new Object[ls.size()][13];
			for(int i=0;i<ls.size();i++) {
				objects[i][0]=ls.get(i).getC_id();
				objects[i][1]=ls.get(i).getRoomid();
				objects[i][2]=ls.get(i).getcName();
				objects[i][3]=ls.get(i).getC_sex();
				objects[i][4]=ls.get(i).getD_type();
				objects[i][5]=ls.get(i).getNo();
				objects[i][6]=ls.get(i).getNumber();
				objects[i][7]=ls.get(i).getForegift();
				objects[i][8]=ls.get(i).getDays();
				objects[i][9]=ls.get(i).getState();
				objects[i][10]=ls.get(i).getIn_time();
				objects[i][11]=ls.get(i).getChk_time();
				objects[i][12]=ls.get(i).getChk_no();
			}
			return objects;
		}

		
		public static Object[][] getSelect(String sql){
			DbConnection dbc =new DbConnection();
			ResultSet resultSet=DbConnection.query(sql);
			ArrayList<Field> list=new ArrayList<Field> ();
			try {
				while (resultSet.next()) {
				    Field field=new Field();
				    field.setChkNo(resultSet.getInt(1));
				    field.setRoomid(resultSet.getInt(2));
				    field.setcName(resultSet.getString(3));
				    field.setForegift(resultSet.getDouble(4));
				    field.setMoney(resultSet.getDouble(5));
				    field.setChk_time(resultSet.getDate(6));
				    field.setRemark(resultSet.getString(7));
					list.add(field);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			Object[][] objects=new Object[list.size()][7];
			for(int i=0;i<list.size();i++) {
				objects[i][0]=list.get(i).getChkNo();
				objects[i][1]=list.get(i).getRoomid();
				objects[i][2]=list.get(i).getcName();
				objects[i][3]=list.get(i).getForegift();
				objects[i][4]=list.get(i).getMoney();
				objects[i][5]=list.get(i).getChk_time();
				objects[i][6]=list.get(i).getRemark();
						
			}
			return objects;
		}

		
		
		
	//第三个表	
		
		public static Object[][] getSelect3(String sql3){
			DbConnection dbc =new DbConnection();
			ResultSet rset=DbConnection.query(sql3);
			ArrayList<Query3> list=new ArrayList<Query3> ();
			try {
				while (rset.next()) {
				    Query3 query3=new Query3();
				    query3.setRoomid(rset.getInt(1));
				    query3.setR_type(rset.getString(2));
				    query3.setPrice1(rset.getDouble(3));
				    query3.setDiscount(rset.getInt(4));
				    query3.setZhprice1(rset.getDouble(5));
				    query3.setYhprice1(rset.getDouble(6));
				    query3.setIn_time(rset.getDate(7));
					list.add(query3);
//					
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			Object[][] objects=new Object[list.size()][7];
			for(int i=0;i<list.size();i++) {
				objects[i][0]=list.get(i).getRoomid();
				objects[i][1]=list.get(i).getR_type();
				objects[i][2]=list.get(i).getPrice1();
				objects[i][3]=list.get(i).getDiscount();
				objects[i][4]=list.get(i).getZhprice1();
				objects[i][5]=list.get(i).getYhprice1();
				objects[i][6]=list.get(i).getIn_time();
						
			}
			return objects;
		}		
		
		
		
		
		
		public static Object[][] getSelect4(String sql4){
			DbConnection dbc =new DbConnection();
			ResultSet rst=DbConnection.query(sql4);
			ArrayList<Query2> ls=new ArrayList<Query2> ();
			try {
				while (rst.next()) {
				    Query2 query2=new Query2();
				    query2.setC_id(rst.getInt(1));
				    query2.setRoomid(rst.getInt(2));
				    query2.setcName(rst.getString(3));
				    query2.setC_sex(rst.getString(4));
				    query2.setD_type(rst.getString(5));
				    query2.setNo(rst.getString(6));
				    query2.setNumber(rst.getInt(7));
				    query2.setForegift(rst.getDouble(8));
				    query2.setDays(rst.getInt(9));
				    query2.setState(rst.getString(10));
				    query2.setIn_time(rst.getDate(11));
				    query2.setChk_time(rst.getDate(12));
				    query2.setChk_no(rst.getInt(13));
					ls.add(query2);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Object[][] objects=new Object[ls.size()][13];
			for(int i=0;i<ls.size();i++) {
				objects[i][0]=ls.get(i).getC_id();
				objects[i][1]=ls.get(i).getRoomid();
				objects[i][2]=ls.get(i).getcName();
				objects[i][3]=ls.get(i).getC_sex();
				objects[i][4]=ls.get(i).getD_type();
				objects[i][5]=ls.get(i).getNo();
				objects[i][6]=ls.get(i).getNumber();
				objects[i][7]=ls.get(i).getForegift();
				objects[i][8]=ls.get(i).getDays();
				objects[i][9]=ls.get(i).getState();
				objects[i][10]=ls.get(i).getIn_time();
				objects[i][11]=ls.get(i).getChk_time();
				objects[i][12]=ls.get(i).getChk_no();
			}
			return objects;
		}
		public static Object[][] getSelect5(String date){
			String sql ="  ";
			DbConnection dbc =new DbConnection();
			ResultSet rst=DbConnection.query(sql);
			ArrayList<Query2> ls=new ArrayList<Query2> ();
			try {
				while (rst.next()) {
					Query2 query2=new Query2();
					query2.setC_id(rst.getInt(1));
					query2.setRoomid(rst.getInt(2));
					query2.setcName(rst.getString(3));
					query2.setC_sex(rst.getString(4));
					query2.setD_type(rst.getString(5));
					query2.setNo(rst.getString(6));
					query2.setNumber(rst.getInt(7));
					query2.setForegift(rst.getDouble(8));
					query2.setDays(rst.getInt(9));
					query2.setState(rst.getString(10));
					query2.setIn_time(rst.getDate(11));
					query2.setChk_time(rst.getDate(12));
					query2.setChk_no(rst.getInt(13));
					ls.add(query2);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Object[][] objects=new Object[ls.size()][13];
			for(int i=0;i<ls.size();i++) {
				objects[i][0]=ls.get(i).getC_id();
				objects[i][1]=ls.get(i).getRoomid();
				objects[i][2]=ls.get(i).getcName();
				objects[i][3]=ls.get(i).getC_sex();
				objects[i][4]=ls.get(i).getD_type();
				objects[i][5]=ls.get(i).getNo();
				objects[i][6]=ls.get(i).getNumber();
				objects[i][7]=ls.get(i).getForegift();
				objects[i][8]=ls.get(i).getDays();
				objects[i][9]=ls.get(i).getState();
				objects[i][10]=ls.get(i).getIn_time();
				objects[i][11]=ls.get(i).getChk_time();
				objects[i][12]=ls.get(i).getChk_no();
			}
			return objects;
		}
		
		
		
	}



