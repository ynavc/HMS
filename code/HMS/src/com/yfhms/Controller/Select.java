package com.yfhms.Controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.yfhms.Bean.Checkout_info;
import com.yfhms.Bean.Customerinfo;
import com.yfhms.Bean.MainAllRoomtype;
import com.yfhms.Bean.MainRoomTypeInfo;
import com.yfhms.Bean.Reserveadd;
import com.yfhms.Bean.RoomInfo;
import com.yfhms.DAO.DbConnection;
import com.yfhms.View.Checkout;


public class Select {
	//查询数据条数
	public static int getCount(String sql) {
		ResultSet resultSet=DbConnection.query(sql);
		try {
			if (resultSet.next()) {
				return resultSet.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
	//返回String类型的值
	public String getString(String sql) {
		ResultSet resultSet = DbConnection.query(sql);
		ArrayList list = new ArrayList<>();
		try {
			while (resultSet.next()) {
				list.add(resultSet.getString(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		String data = list.get(0).toString();
		return data;
	}
	/**=======================================================================**
	 *			散客开单界面
	 **=======================================================================**
	 */
	//得到目前可用的房间
	public Object[][] getForTheRoom() {
		String sql="SELECT roomid from r_state ,roominfo WHERE number=roominfo.state AND r_state.state='可用'";
		ResultSet resultSet = DbConnection.query(sql);
		ArrayList list = new ArrayList<>();
		try {
			while (resultSet.next()) {
				list.add(resultSet.getInt(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		Object [][] data = new Object[list.size()][1];
		for (int i = 0; i < list.size(); i++) {
			data[i][0]=list.get(i);
		}
		return data;
	}
	//得到证件类型
	public Object[][] getIDType() {
		//获取证件类型
		String sql="SELECT d_type from customerinfo";
		ResultSet resultSet = DbConnection.query(sql);
		//查询证件类型数量
		String indexSql = "SELECT COUNT(*) from customerinfo";
		int num = getCount(indexSql);
		System.out.println(num);
		
		ArrayList list = new ArrayList<>();
		try {
			while (resultSet.next()) {
				list.add(resultSet.getString(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		Object [][] data = new Object[list.size()][1];
		for (int i = 0; i < list.size(); i++) {
			data[i][0]=list.get(i);
		}
		return data;
	}
	//得到宾客类型
	public Object[][] getGuestsType() {
		//获取宾客类型
		String sql="SELECT c_type from customertype";
		ResultSet resultSet = DbConnection.query(sql);
		//查询宾客类型数量
		String indexSql = "SELECT COUNT(*) from customertype";
		int num = getCount(indexSql);
		System.out.println(num);
		
		ArrayList list = new ArrayList<>();
		try {
			while (resultSet.next()) {
				list.add(resultSet.getString(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		Object [][] data = new Object[list.size()][1];
		for (int i = 0; i < list.size(); i++) {
			data[i][0]=list.get(i);
		}
		return data;
	}
	//得到对应的折扣比率
	public String getDiscount(String guestsType) {
		String sql="SELECT discount from customertype WHERE c_type='"+guestsType+"'";
		ResultSet resultSet = DbConnection.query(sql);
		ArrayList list = new ArrayList<>();
		try {
			while (resultSet.next()) {
				list.add(resultSet.getString(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		String discount = list.get(0).toString();
		System.out.println(discount);
		return discount;
	}
	//得到房间类型
	public String getRoomtype(String roomId) {
		String sql="SELECT r_type from roomtype ,roominfo WHERE roomtype.roomtype = roominfo.roomtype AND roomid='"+roomId+"'";
		ResultSet resultSet = DbConnection.query(sql);
		ArrayList list = new ArrayList();
		try {
			while (resultSet.next()) {
				list.add(resultSet.getString(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		String data = new String();
		data = list.get(0).toString();
		return data;
	}
	//得到预设单价
	public String getprice (String roomId) {
		String sql="SELECT price from roomtype ,roominfo WHERE roomtype.roomtype = roominfo.roomtype AND roomid='"+roomId+"'";
		ResultSet resultSet = DbConnection.query(sql);
		ArrayList list = new ArrayList();
		try {
			while (resultSet.next()) {
				list.add(resultSet.getString(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		String data = new String();
		data = list.get(0).toString();
		return data;
	}
	
	/**=======================================================================**
	 *			主界面
	 **=======================================================================**
	 */
	public ArrayList getMainInfoRoom(String roomTypeID) {
		String sql = "SELECT roomid,state FROM roominfo WHERE roomtype='"+roomTypeID+"';";
		ResultSet resultSet = DbConnection.query(sql);
		ArrayList<RoomInfo> list = new ArrayList<RoomInfo>();
		try {
			while (resultSet.next()) {
				RoomInfo roomInfo = new RoomInfo();
				roomInfo.setRoomID(Integer.parseInt(resultSet.getString(1)));
				roomInfo.setState(Integer.parseInt(resultSet.getString(2)));
				list.add(roomInfo);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	public Object[][] getAllCheckoutInfo1() {
		String sql="SELECT livein.in_no,roominfo.roomid,price,r_type,discount,money,checkout.chk_time,account FROM userinfo,checkout,livein,customerinfo,customertype,roominfo,roomtype WHERE userinfo.id=checkout.u_id AND checkout.in_no=livein.in_no AND livein.roomid=roominfo.roomid AND livein.c_id=customerinfo.c_id AND roominfo.roomtype=roomtype.roomtype AND customerinfo.c_type=customertype.c_id ";
		ResultSet resultSet = DbConnection.query(sql);
		ArrayList list = new ArrayList();
		try {
			while (resultSet.next()) {
				list.add(resultSet.getString(1));
				list.add(resultSet.getString(2));
				list.add(resultSet.getString(3));
				list.add(resultSet.getString(4));
				list.add(resultSet.getString(6));
				list.add(resultSet.getString(6));
				list.add(resultSet.getString(7));
				list.add(resultSet.getString(8));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		Object [][] data = new Object[list.size()][8];
		for (int i = 0; i < list.size(); i++) {
			data[i][0]=list.get(i);
			data[i][1]=list.get(i);
			data[i][2]=list.get(i);
			data[i][3]=list.get(i);
			data[i][4]=list.get(i);
			data[i][5]=list.get(i);
			data[i][6]=list.get(i);
			data[i][7]=list.get(i);
		}
		return data;
	}
	public Object[][] getAllCheckoutInfo() {
		String sql="SELECT livein.in_no,roominfo.roomid,price,r_type,discount,money,checkout.chk_time,account FROM userinfo,checkout,livein,customerinfo,customertype,roominfo,roomtype WHERE userinfo.id=checkout.u_id AND checkout.in_no=livein.in_no AND livein.roomid=roominfo.roomid AND livein.c_id=customerinfo.c_id AND roominfo.roomtype=roomtype.roomtype AND customerinfo.c_type=customertype.c_id ";
		ResultSet resultSet = DbConnection.query(sql);
		ArrayList<MainAllRoomtype> list = new ArrayList<MainAllRoomtype>();
		try {
			while (resultSet.next()) {
				MainAllRoomtype allRoomtype = new MainAllRoomtype();
				allRoomtype.setIn_no(resultSet.getString(1));
				allRoomtype.setRoomid(resultSet.getString(2));
				allRoomtype.setPrice(resultSet.getString(3));
				allRoomtype.setR_type(resultSet.getString(4));
				allRoomtype.setDiscount(resultSet.getString(5));
				allRoomtype.setMoney(resultSet.getString(6));
				allRoomtype.setChk_time(resultSet.getString(7));
				allRoomtype.setAccount(resultSet.getString(8));
				list.add(allRoomtype);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		Object [][] data = new Object[list.size()][8];
		for (int i = 0; i < list.size(); i++) {
			data[i][0]=list.get(i).getIn_no();
			data[i][1]=list.get(i).getRoomid();
			data[i][2]=list.get(i).getPrice();
			data[i][3]=list.get(i).getR_type();
			data[i][4]=list.get(i).getDiscount();
			data[i][5]=list.get(i).getMoney();
			data[i][6]=list.get(i).getChk_time();
			data[i][7]=list.get(i).getAccount();
		}
		return data;
	}
	public MainRoomTypeInfo getMainRoomTypeInfo(String roomid) {
		String sql = "SELECT c_name,price,room_phone,location,DATE_FORMAT(in_time,'%Y-%m-%d'),timestampdiff(hour,in_time,NOW()),foregift FROM livein,customerinfo,customertype,roominfo,roomtype WHERE livein.roomid=roominfo.roomid AND livein.c_id=customerinfo.c_id AND roominfo.roomtype=roomtype.roomtype AND customerinfo.c_type=customertype.c_id AND roominfo.roomid='"+roomid+"'";
		ResultSet resultSet = DbConnection.query(sql);
		MainRoomTypeInfo mainRoomTypeInfo = new MainRoomTypeInfo();
		try {
			while (resultSet.next()) {
				mainRoomTypeInfo.setC_name(resultSet.getString(1));
				mainRoomTypeInfo.setPrice(resultSet.getString(2));
				mainRoomTypeInfo.setRoom_phone(resultSet.getString(3));
				mainRoomTypeInfo.setLocation(resultSet.getString(4));
				mainRoomTypeInfo.setIn_time(resultSet.getString(5));
				mainRoomTypeInfo.setElapsed_time(resultSet.getString(6));
				mainRoomTypeInfo.setForegift(resultSet.getString(7));
				return mainRoomTypeInfo;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return mainRoomTypeInfo;
	}
	public MainRoomTypeInfo getMainRoomTypeInfo1(String roomid) {
		String sql = "SELECT price,room_phone,location FROM roominfo,roomtype WHERE roominfo.roomtype=roomtype.roomtype AND roomid='"+roomid+"';";
		ResultSet resultSet = DbConnection.query(sql);
		MainRoomTypeInfo mainRoomTypeInfo = new MainRoomTypeInfo();
		try {
			while (resultSet.next()) {
				mainRoomTypeInfo.setPrice(resultSet.getString(1));
				mainRoomTypeInfo.setRoom_phone(resultSet.getString(2));
				mainRoomTypeInfo.setLocation(resultSet.getString(3));
				return mainRoomTypeInfo;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return mainRoomTypeInfo;
	}
	/**=======================================================================**
	 *			预订、结算、添加预订、删除预订
	 **=======================================================================
	 * @return **
	 */
	//查询结算信息
	public Object[][] getCheckoutInfo() {
		String sql = "SELECT roominfo.roomid,price,discount,price*discount,checkout.days,money,checkout.chk_time FROM livein,customerinfo,customertype,roominfo,roomtype,checkout WHERE livein.roomid=roominfo.roomid AND livein.c_id=customerinfo.c_id AND roominfo.roomtype=roomtype.roomtype AND customerinfo.c_type=customertype.c_id AND checkout.in_no=livein.in_no";
		ResultSet rest=DbConnection.query(sql);	
		ArrayList<Checkout_info> list=new ArrayList<Checkout_info>();
		try {
			while (rest.next()) {
				//房间号 单价 折扣 折扣价 消费天数 消费金额 消费时间
				Checkout_info che = new Checkout_info();
				che.setRoomid(rest.getString(1));
				che.setPrice(rest.getString(2));
				che.setDiscount(rest.getString(3));
				che.setDiscount_price(rest.getString(4));
				che.setConsumption_days(rest.getString(5));
				che.setConsumption_money(rest.getString(6));
				che.setConsumption_time(rest.getString(7));
				list.add(che);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		Object [][] data = new Object[list.size()][7];
		for (int i = 0; i < list.size(); i++) {
			data[i][0]=list.get(i).getRoomid();
			data[i][1]=list.get(i).getPrice();
			data[i][2]=list.get(i).getDiscount();
			data[i][3]=list.get(i).getDiscount_price();
			data[i][4]=list.get(i).getConsumption_days();
			data[i][5]=list.get(i).getConsumption_money();
			data[i][6]=list.get(i).getConsumption_time();
		}
		return data;
	}
	//查询结算信息
	public Checkout_info getCheckoutInfo1(String roomID) {
		String sql = "SELECT livein.in_no,roominfo.roomid,c_name,timestampdiff(DAY,in_time,NOW()),foregift,price,discount FROM livein,customerinfo,customertype,roominfo,roomtype,checkout WHERE livein.roomid=roominfo.roomid AND livein.c_id=customerinfo.c_id AND roominfo.roomtype=roomtype.roomtype AND customerinfo.c_type=customertype.c_id AND roominfo.roomid='"+roomID+"'";
		ResultSet rest=DbConnection.query(sql);	
		try {
			while (rest.next()) {
				//livein.in_no,roominfo.roomid,c_name,timestampdiff(DAY,in_time,NOW()),foregift
				Checkout_info che = new Checkout_info();
				che.setStatement_no(rest.getString(1));//结账单号（入住id）
				che.setRoomid(rest.getString(2));//房间号
				che.setGuest_name(rest.getString(3));//客户姓名
				che.setConsumption_days(rest.getString(4));//入住天数
				che.setDeposit(rest.getString(5));//已交押金
				che.setPrice(rest.getString(6));//预订价格
				che.setDiscount(rest.getString(7));//折扣信息
				return che;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println("5556454654fyu fyufyu ufyu f546");
		return null;
	}
	
	public Object[][] getcustinfo( String sql) {
		 
		ResultSet rest=DbConnection.query(sql);	
		ArrayList<Customerinfo> list=new ArrayList<Customerinfo>();
		
		try {
			while (rest.next()) {
				Customerinfo custinfo = new Customerinfo();
				custinfo.setcId(rest.getInt(1));
				custinfo.setcName(rest.getString(2));
				custinfo.setcSex(rest.getString(3));
				custinfo.setNo(rest.getString(4));
				custinfo.setvPhone(rest.getString(5));
				custinfo.setAddress(rest.getString(6));
				list.add(custinfo);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		Object[][] ob1=new Object[list.size()][6];
		for (int i = 0; i < list.size(); i++) {
			ob1[i][0] = list.get(i).getcId();
			ob1[i][1] = list.get(i).getcName();
			ob1[i][2] = list.get(i).getcSex();
			ob1[i][3] = list.get(i).getNo();
			ob1[i][4] = list.get(i).getvPhone();
			ob1[i][5] = list.get(i).getAddress();
		}
		return ob1;												

	}
	
	public Object[][] getcustinfo2(String sqlx) {
		
		ResultSet rest=DbConnection.query(sqlx);	
		ArrayList<Customerinfo> list=new ArrayList<Customerinfo>();
		
		try {
			while (rest.next()) {
				Customerinfo custinfo = new Customerinfo();
				custinfo.setcId(rest.getInt(1));
				custinfo.setcName(rest.getString(2));
				custinfo.setcSex(rest.getString(3));
				custinfo.setNo(rest.getString(4));
				custinfo.setvPhone(rest.getString(5));
				custinfo.setAddress(rest.getString(6));
				list.add(custinfo);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		Object[][] ob2=new Object[list.size()][6];
		for (int i = 0; i < list.size(); i++) {
			ob2[i][0] = list.get(i).getcId();
			ob2[i][1] = list.get(i).getcName();
			ob2[i][2] = list.get(i).getcSex();
			ob2[i][3] = list.get(i).getNo();
			ob2[i][4] = list.get(i).getvPhone();
			ob2[i][5] = list.get(i).getAddress();
		}
		return ob2;												

	}

	
	//查询房间信息
		public Object[][] getRoomInfoAndRoomtype() {
			String sql="SELECT r_type,reserve.roomid FROM reserve,roominfo,roomtype\r\n" + 
					"WHERE reserve.roomid = roominfo.roomid \r\n" + 
					"AND roominfo.roomtype=roomtype.roomtype";
			ResultSet rest=DbConnection.query(sql);	
			ArrayList<Reserveadd> list=new ArrayList<Reserveadd>();
			
			try {
				while (rest.next()) {
					Reserveadd reserveadd = new Reserveadd();
					reserveadd.setR_Type(rest.getString(1));
					reserveadd.setRoomid(rest.getString(2));
					list.add(reserveadd);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			Object[][] ob1=new Object[list.size()][2];
			for (int i = 0; i < list.size(); i++) {
				ob1[i][0] = list.get(i).getR_Type();
				ob1[i][1] = list.get(i).getRoomid();
			}
			return ob1;												
		}
		
		public Object[][] getRoom(String content) {
			String sql="SELECT r_name,r_phone,r_type,reserve.roomid,pa_time,keep_time,remark FROM reserve,roominfo,roomtype\r\n" + 
					"WHERE reserve.roomid = roominfo.roomid \r\n" + 
					"AND roominfo.roomtype=roomtype.roomtype AND reserve.roomid='"+content+"'";
			ResultSet rest=DbConnection.query(sql);	
			ArrayList<Reserveadd> list=new ArrayList<Reserveadd>();
			
			try {
				while (rest.next()) {
					Reserveadd reserveadd = new Reserveadd();
					reserveadd.setR_name(rest.getString(1));
					reserveadd.setR_phone(rest.getString(2));
					reserveadd.setR_Type(rest.getString(3));
					reserveadd.setRoomid(rest.getString(4));
					reserveadd.setPa_time(rest.getString(5));
					reserveadd.setKeep_time(rest.getString(6));
					reserveadd.setRemark(rest.getString(7));
					list.add(reserveadd);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			Object[][] ob1=new Object[list.size()][7];
			for (int i = 0; i < list.size(); i++) {
				ob1[i][0] = list.get(i).getR_name();
				ob1[i][1] = list.get(i).getR_phone();
				ob1[i][2] = list.get(i).getR_Type();
				ob1[i][3] = list.get(i).getRoomid();
				ob1[i][4] = list.get(i).getPa_time();
				ob1[i][5] = list.get(i).getKeep_time();
				ob1[i][6] = list.get(i).getRemark();
			}
			return ob1;
			
		}
		//添加预订信息单独页面
		public Object[][] getReserveadd(String sql) {
			ResultSet rest=DbConnection.query(sql);	
			ArrayList list=new ArrayList();
			
			try {
				while (rest.next()) {
					list.add(rest.getString(1));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			Object[][] ob1=new Object[list.size()][1];
			for (int i = 0; i < list.size(); i++) {
				ob1[i][0] = list.get(i);
				System.out.println(ob1[i][0]);
			}
			return ob1;	
		}
		//添加预订信息单独页面
		public String getroomtype(String sql) {
			ResultSet rest=DbConnection.query(sql);	
			ArrayList list=new ArrayList();
			try {
				while (rest.next()) {
					list.add(rest.getString(1));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			String ob1=new String();
			ob1 = (String) list.get(0);
			return ob1;	
		}
		/**=======================================================================**
		 *			登录界面
		 **=======================================================================**
		 */
		
		//用户登录
				public int Select(String account ,String password) {
					String sql="select * from UserInfo where password='"+password+"'  and account='"+account+"'";
					ResultSet resultSet=DbConnection.query(sql);
					int a=0;
					try {
						while (resultSet.next()) {
							a=1;
						}
					} catch (SQLException e) {
						e.printStackTrace();
					}
					return a;
				}
				public String[] getUserID() {
					String sql="select Account from UserInfo";
					ResultSet resultSet=DbConnection.query(sql);
					ArrayList list = new ArrayList();
					try {
						while (resultSet.next()) {
							list.add(resultSet.getString(1));
						}
					} catch (SQLException e) {
						e.printStackTrace();
					}
					String[] data = new String[list.size()];
					for (int i = 0; i < list.size(); i++) {
						data[i] = (String) list.get(i);
					}
					return data;
				}
}
