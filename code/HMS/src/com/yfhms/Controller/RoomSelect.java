package com.yfhms.Controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.yfhms.Bean.RoomInfoTable;
import com.yfhms.Bean.RoomType;
import com.yfhms.DAO.DbConnection;

public class RoomSelect {
	//查询房间类型，预设单价，钟点价格，床位数量，能否按钟点计费
		public static Object[][] getRoomType(){
			String sql="SELECT * from roomtype";
			ResultSet resultSet=DbConnection.query(sql);
			ArrayList<RoomType> list=new ArrayList<RoomType>();
			try {
				while (resultSet.next()) {
					RoomType roomtype=new RoomType();
					roomtype.setR_Type(resultSet.getString(2));
					roomtype.setPrice(resultSet.getInt(3));
					roomtype.setHour_Price(resultSet.getInt(4));
					roomtype.setBed(resultSet.getInt(5));
					roomtype.setWhethe(resultSet.getString(6));
					list.add(roomtype);
				}
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			//泛型集合，转化为二维数组
		Object[][] body=new Object[list.size()][5];
		for (int i = 0; i < list.size(); i++) {
			body[i][0]=list.get(i).getR_Type();
			body[i][1]=list.get(i).getPrice();
			body[i][2]=list.get(i).getHour_Price();
			body[i][3]=list.get(i).getBed();
			body[i][4]=list.get(i).getWhethe();			
		}
		return body;
}
		//查询房间信息
		public static Object[][] getRoomInfo(){
			String sql="SELECT roomid,roomtype.r_type,r_state.state,location,room_phone from roominfo ,r_state ,roomtype WHERE r_state.number=roominfo.state and roominfo.roomtype=roomtype.roomtype";
			ResultSet resultSet=DbConnection.query(sql);
			ArrayList<RoomInfoTable> list=new ArrayList<RoomInfoTable>();
			try {
				while (resultSet.next()) {
					RoomInfoTable roominfo=new RoomInfoTable();
					roominfo.setRoomID(resultSet.getInt(1));
					roominfo.setToomType(resultSet.getString(2));
					roominfo.setState(resultSet.getString(3));
					roominfo.setLocation(resultSet.getString(4));
					roominfo.setRoom_phone(resultSet.getString(5));
					list.add(roominfo);
				}
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			//泛型集合，转化为二维数组
		Object[][] body=new Object[list.size()][5];
		for (int i = 0; i < list.size(); i++) {
			body[i][0]=list.get(i).getRoomID();
			body[i][1]=list.get(i).getToomType();
			body[i][2]=list.get(i).getState();
			body[i][3]=list.get(i).getLocation();
			body[i][4]=list.get(i).getRoom_phone();			
		}
		return body;
}
		//修改房间类型的下拉框获取数据库的值
		//把数据库的值查询后循环添加到list集合里，再把list里的数据循环添加到一个数组。
		public static Object[] getRoomComboBox(){
			String sql="SELECT r_type FROM roomtype";
			ResultSet resultSet=DbConnection.query(sql);
			ArrayList list=new ArrayList();
			try {
				while (resultSet.next()) {
					list.add(resultSet.getString(1));
				}
		}catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			//泛型集合，转化为一维数组
		Object[] body=new Object[list.size()];
		for (int i = 0; i < list.size(); i++) {
			body[i]=list.get(i);
		}
		return body;
}		
}
