package com.yfhms.Controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.yfhms.Bean.Customertype;
import com.yfhms.Bean.RoomType;
import com.yfhms.DAO.DbConnection;

public class CustomerTable {
	//查询客户类型
	public static Object[][] getRoomType(){
		String sql="SELECT * from customertype";
		ResultSet resultSet=DbConnection.query(sql);
		ArrayList<Customertype> list=new ArrayList<Customertype>();
		try {
			while (resultSet.next()) {
				Customertype customertype=new Customertype();
				customertype.setcId(resultSet.getInt(1));
				customertype.setcType(resultSet.getString(2));
				customertype.setDiscount(resultSet.getString(3));
				list.add(customertype);
			}
	}catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		//泛型集合，转化为二维数组
	Object[][] body=new Object[list.size()][3];
	for (int i = 0; i < list.size(); i++) {
		body[i][0]=list.get(i).getcId();
		body[i][1]=list.get(i).getcType();
		body[i][2]=list.get(i).getDiscount();	
	}
	return body;
}
	//房间费打折表格
	public static Object[][] getRoomDiscount(){
		String sql="SELECT * FROM roomtype";
		ResultSet resultSet=DbConnection.query(sql);
		ArrayList<RoomType> list=new ArrayList<RoomType>();
		try {
			while (resultSet.next()) {
				RoomType roomtype=new RoomType();
				roomtype.setR_Type(resultSet.getString(2));
				roomtype.setPrice(resultSet.getDouble(3));
				list.add(roomtype);
			}
	}catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		//泛型集合，转化为二维数组
	Object[][] body=new Object[list.size()][2];
	for (int i = 0; i < list.size(); i++) {
		body[i][0]=list.get(i).getR_Type();
		body[i][1]=list.get(i).getPrice();
	}
	return body;
}
}
