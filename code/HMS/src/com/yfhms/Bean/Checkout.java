package com.yfhms.Bean;
//顾客信息表
public class Checkout {
	private int chkNo;//结算时间
	private int inNo;//入住单号
	private int days;//入住天数
	private int money;//金额
	private String datetime;//结算时间
	private int u_id;//操作员id
	
	public Checkout() {
		super();
	}

	public Checkout(int chkNo, int inNo, int days, int money, String datetime, int u_id) {
		super();
		this.chkNo = chkNo;
		this.inNo = inNo;
		this.days = days;
		this.money = money;
		this.datetime = datetime;
		this.u_id = u_id;
	}

	public int getChkNo() {
		return chkNo;
	}

	public void setChkNo(int chkNo) {
		this.chkNo = chkNo;
	}

	public int getInNo() {
		return inNo;
	}

	public void setInNo(int inNo) {
		this.inNo = inNo;
	}

	public int getDays() {
		return days;
	}

	public void setDays(int days) {
		this.days = days;
	}

	public int getMoney() {
		return money;
	}

	public void setMoney(int money) {
		this.money = money;
	}

	public String getDatetime() {
		return datetime;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}

	public int getU_id() {
		return u_id;
	}

	public void setU_id(int u_id) {
		this.u_id = u_id;
	}

	@Override
	public String toString() {
		return "Checkout [chkNo=" + chkNo + ", inNo=" + inNo + ", days=" + days + ", money=" + money + ", datetime="
				+ datetime + ", u_id=" + u_id + "]";
	}
	
}
