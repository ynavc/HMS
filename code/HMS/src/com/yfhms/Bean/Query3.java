package com.yfhms.Bean;

import java.sql.Date;

public class Query3 {
		private int roomid;//房间号
		private String r_type;//房间类型
		private double price1;//单价
		private int discount;//折扣
		private double zhprice1;//折后单价
		private double yhprice1;//优惠金额
		public Query3() {
			super();
		}
		public Query3(int roomid, String r_type, double price1, int discount, double zhprice1, double yhprice1,
				Date in_time) {
			super();
			this.roomid = roomid;
			this.r_type = r_type;
			this.price1 = price1;
			this.discount = discount;
			this.zhprice1 = zhprice1;
			this.yhprice1 = yhprice1;
			this.in_time = in_time;
		}
		public int getRoomid() {
			return roomid;
		}
		public void setRoomid(int roomid) {
			this.roomid = roomid;
		}
		public String getR_type() {
			return r_type;
		}
		public void setR_type(String r_type) {
			this.r_type = r_type;
		}
		public double getPrice1() {
			return price1;
		}
		public void setPrice1(double price1) {
			this.price1 = price1;
		}
		public int getDiscount() {
			return discount;
		}
		public void setDiscount(int discount) {
			this.discount = discount;
		}
		public double getZhprice1() {
			return zhprice1;
		}
		public void setZhprice1(double zhprice1) {
			this.zhprice1 = zhprice1;
		}
		public double getYhprice1() {
			return yhprice1;
		}
		public void setYhprice1(double yhprice1) {
			this.yhprice1 = yhprice1;
		}
		public Date getIn_time() {
			return in_time;
		}
		public void setIn_time(Date in_time) {
			this.in_time = in_time;
		}
		private Date in_time;//入住时间
}