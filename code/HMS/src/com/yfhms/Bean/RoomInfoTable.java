package com.yfhms.Bean;

public class RoomInfoTable {
	private int roomID;//房间编号
	private String toomType;//房间类型
	private String state;//房间状态
	private String location;//所在区域
	private String room_phone;//房间号码
	public int getRoomID() {
		return roomID;
	}
	public void setRoomID(int roomID) {
		this.roomID = roomID;
	}
	public String getToomType() {
		return toomType;
	}
	public void setToomType(String toomType) {
		this.toomType = toomType;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getRoom_phone() {
		return room_phone;
	}
	public void setRoom_phone(String room_phone) {
		this.room_phone = room_phone;
	}
	public RoomInfoTable(int roomID, String toomType, String state, String location, String room_phone) {
		super();
		this.roomID = roomID;
		this.toomType = toomType;
		this.state = state;
		this.location = location;
		this.room_phone = room_phone;
	}
	public RoomInfoTable() {
		super();
	}
	@Override
	public String toString() {
		return "RoomInfoTable [roomID=" + roomID + ", toomType=" + toomType + ", state=" + state + ", location="
				+ location + ", room_phone=" + room_phone + "]";
	}
	
	
}
