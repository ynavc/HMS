package com.yfhms.Bean;
//房间信息表
public class RoomInfo {
	private int roomID;//房间编号
	private String roomType;//房间类型编号
	private int state;//房间状态
	private String location;//所在区域
	private String room_phone;//房间号码
	//无参构造
	public RoomInfo() {
		super();
	}
	//有参构造
	public RoomInfo(int roomID, String roomType, int state, String location, String room_phone) {
		super();
		this.roomID = roomID;
		this.roomType = roomType;
		this.state = state;
		this.location = location;
		this.room_phone = room_phone;
	}
	public int getRoomID() {
		return roomID;
	}
	public void setRoomID(int roomID) {
		this.roomID = roomID;
	}
	public String getRoomType() {
		return roomType;
	}
	public void setToomType(String roomType) {
		this.roomType = roomType;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getRoom_phone() {
		return room_phone;
	}
	public void setRoom_phone(String room_phone) {
		this.room_phone = room_phone;
	}
	
	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}
	@Override
	public String toString() {
		return "RoomInfo [roomID=" + roomID + ", roomType=" + roomType + ", state=" + state + ", location=" + location
				+ ", room_phone=" + room_phone + "]";
	}
	
}
