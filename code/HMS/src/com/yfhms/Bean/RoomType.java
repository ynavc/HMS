package com.yfhms.Bean;
//房间类型表
public class RoomType {
	private int roomTypeId;//房间类型ID
	private String r_Type;//房间类型
	private double price;//预设单价
	private int roomId;//房间编号
	private int hour_Price;//钟点价格/小时
	private int bed;//床位数量
	private String whethe;//能否按小时计费
	
	public RoomType() {
		super();
	}
	public int getRoomTypeId() {
		return roomTypeId;
	}
	public void setRoomTypeId(int roomTypeId) {
		this.roomTypeId = roomTypeId;
	}
	public String getR_Type() {
		return r_Type;
	}
	public void setR_Type(String r_Type) {
		this.r_Type = r_Type;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public int getRoomId() {
		return roomId;
	}
	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}
	public int getHour_Price() {
		return hour_Price;
	}
	public void setHour_Price(int hour_Price) {
		this.hour_Price = hour_Price;
	}
	public int getBed() {
		return bed;
	}
	public void setBed(int bed) {
		this.bed = bed;
	}
	
	public RoomType(int roomTypeId, String r_Type, double price, int roomId, int hour_Price, int bed, String whethe) {
		super();
		this.roomTypeId = roomTypeId;
		this.r_Type = r_Type;
		this.price = price;
		this.roomId = roomId;
		this.hour_Price = hour_Price;
		this.bed = bed;
		this.whethe = whethe;
	}
	
	public String getWhethe() {
		return whethe;
	}
	public void setWhethe(String whethe) {
		this.whethe = whethe;
	}
	@Override
	public String toString() {
		return "RoomType [roomTypeId=" + roomTypeId + ", r_Type=" + r_Type + ", price=" + price + ", roomId=" + roomId
				+ ", hour_Price=" + hour_Price + ", bed=" + bed + "]";
	}
	
}
