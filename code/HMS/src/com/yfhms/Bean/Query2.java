package com.yfhms.Bean;

import java.sql.Date;

public class Query2 {
	private int c_id;//会员编号
	private int roomid;//房间号
	private String cName;//客户名字
	private String c_sex;//客户性别
	private String d_type;//证件类型
	private String no;//证件编号
	private int number;//人数
	private double foregift;//押金
	private int days;//预住天数
	private String state;//当前状态
	private Date in_time;//入住时间
	private Date chk_time;//结账时间
	private int chk_no;//结算单号
	@Override
	public String toString() {
		return "Query2 [c_id=" + c_id + ", roomid=" + roomid + ", cName=" + cName + ", c_sex=" + c_sex + ", d_type="
				+ d_type + ", no=" + no + ", number=" + number + ", foregift=" + foregift + ", days=" + days
				+ ", state=" + state + ", in_time=" + in_time + ", chk_time=" + chk_time + ", chk_no=" + chk_no + "]";
	}
	public Query2() {
		super();
	}
	public Query2(int c_id, int roomid, String cName, String c_sex, String d_type, String no, int number, double foregift,
			int days, String state, Date in_time, Date chk_time, int chk_no) {
		super();
		this.c_id = c_id;
		this.roomid = roomid;
		this.cName = cName;
		this.c_sex = c_sex;
		this.d_type = d_type;
		this.no = no;
		this.number = number;
		this.foregift = foregift;
		this.days = days;
		this.state = state;
		this.in_time = in_time;
		this.chk_time = chk_time;
		this.chk_no = chk_no;
	}
	public int getC_id() {
		return c_id;
	}
	public void setC_id(int c_id) {
		this.c_id = c_id;
	}
	public int getRoomid() {
		return roomid;
	}
	public void setRoomid(int roomid) {
		this.roomid = roomid;
	}
	public String getcName() {
		return cName;
	}
	public void setcName(String cName) {
		this.cName = cName;
	}
	public String getC_sex() {
		return c_sex;
	}
	public void setC_sex(String c_sex) {
		this.c_sex = c_sex;
	}
	public String getD_type() {
		return d_type;
	}
	public void setD_type(String d_type) {
		this.d_type = d_type;
	}
	public String getNo() {
		return no;
	}
	public void setNo(String string) {
		this.no = string;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public double getForegift() {
		return foregift;
	}
	public void setForegift(double foregift) {
		this.foregift = foregift;
	}
	public int getDays() {
		return days;
	}
	public void setDays(int days) {
		this.days = days;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public Date getIn_time() {
		return in_time;
	}
	public void setIn_time(Date in_time) {
		this.in_time = in_time;
	}
	public Date getChk_time() {
		return chk_time;
	}
	public void setChk_time(Date chk_time) {
		this.chk_time = chk_time;
	}
	public int getChk_no() {
		return chk_no;
	}
	public void setChk_no(int chk_no) {
		this.chk_no = chk_no;
	}
	
}
