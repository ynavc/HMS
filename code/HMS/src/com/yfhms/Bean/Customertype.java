package com.yfhms.Bean;

public class Customertype {
	private int cId;//客户类型编号
	private String cType;//客户类型
	private String discount;//打折比例
	public int getcId() {
		return cId;
	}
	public void setcId(int cId) {
		this.cId = cId;
	}
	public String getcType() {
		return cType;
	}
	public void setcType(String cType) {
		this.cType = cType;
	}
	public String getDiscount() {
		return discount;
	}
	public void setDiscount(String discount) {
		this.discount = discount;
	}
	public Customertype(int cId, String cType, String discount) {
		super();
		this.cId = cId;
		this.cType = cType;
		this.discount = discount;
	}
	public Customertype() {
		super();
	}
	@Override
	public String toString() {
		return "Customertype [cId=" + cId + ", cType=" + cType + ", discount=" + discount + "]";
	}
	
	
}
