package com.yfhms.Bean;
//入住信息表
public class Livein {
	private int in_no;//入住单号
	private int roomid;//房间编号
	private int c_id;//客户编号
	private int number;//人数
	private double foregift;//押金
	private int days;//天数
	private  int status;//当前状态
	private String in_time;//入住时间
	private String chk_time;//结账时间
	private String chk_no;//结算单号
	public int getIn_no() {
		return in_no;
	}
	public void setIn_no(int in_no) {
		this.in_no = in_no;
	}
	public int getRoomid() {
		return roomid;
	}
	public void setRoomid(int roomid) {
		this.roomid = roomid;
	}
	public int getC_id() {
		return c_id;
	}
	public void setC_id(int c_id) {
		this.c_id = c_id;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public double getForegift() {
		return foregift;
	}
	public void setForegift(double foregift) {
		this.foregift = foregift;
	}
	public int getDays() {
		return days;
	}
	public void setDays(int days) {
		this.days = days;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getIn_time() {
		return in_time;
	}
	public void setIn_time(String in_time) {
		this.in_time = in_time;
	}
	public String getChk_time() {
		return chk_time;
	}
	public void setChk_time(String chk_time) {
		this.chk_time = chk_time;
	}
	public String getChk_no() {
		return chk_no;
	}
	public void setChk_no(String chk_no) {
		this.chk_no = chk_no;
	}
	public Livein(int in_no, int roomid, int c_id, int number, double foregift, int days, int status, String in_time,
			String chk_time, String chk_no) {
		super();
		this.in_no = in_no;
		this.roomid = roomid;
		this.c_id = c_id;
		this.number = number;
		this.foregift = foregift;
		this.days = days;
		this.status = status;
		this.in_time = in_time;
		this.chk_time = chk_time;
		this.chk_no = chk_no;
	}
	public Livein() {
		super();
	}
	@Override
	public String toString() {
		return "Livein [in_no=" + in_no + ", roomid=" + roomid + ", c_id=" + c_id + ", number=" + number + ", foregift="
				+ foregift + ", days=" + days + ", status=" + status + ", in_time=" + in_time + ", chk_time=" + chk_time
				+ ", chk_no=" + chk_no + "]";
	}
	

}
