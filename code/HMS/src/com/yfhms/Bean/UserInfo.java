package com.yfhms.Bean;
//操作员信息表
public class UserInfo {
	private int id;//操作员编号
	private String account;//操作员账号
	private String password;//操作员密码
	private String userType;//操作员类型
	public UserInfo() {
		super();
	}
	public UserInfo(int id, String account, String password, String userType) {
		super();
		this.id = id;
		this.account = account;
		this.password = password;
		this.userType = userType;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	@Override
	public String toString() {
		return "UserInfo [id=" + id + ", account=" + account + ", password=" + password + ", userType=" + userType
				+ "]";
	}
}
