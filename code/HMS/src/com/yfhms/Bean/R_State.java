package com.yfhms.Bean;
	//房间状态表
	//实体类与数据库对应
public class R_State {
	private int number ;//状态编号
	private String state;//房间状态
 
	//get、set访问器
	public int getnumber() {
		return number;
	}
	public void setnumber(int number) {
		this.number = number;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	//有参构造方法
	public R_State(int number, String state) {
		super();
		this.number = number;
		this.state = state;
	}
	//无参构造方法
	public R_State() {
		super();
	}
	@Override
	public String toString() {
		return "R_State [number=" + number + ", state=" + state + "]";
	}			
 
}
