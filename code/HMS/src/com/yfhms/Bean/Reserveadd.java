package com.yfhms.Bean;
//预订信息界面功能实现构造类
public class Reserveadd {
	private int booking_id;//预定信息订单号
	private String r_name;//预定客户名称
	private String r_phone;//联系电话
	private String roomid;//房间编号
	private String cName;//客户名字
	private String vPhone;//客户联系电话
	private String r_Type;//房间类型
	private String pa_time;//抵达时间
	private String keep_time;//保留时间
	private String remark;//备注
	public int getBooking_id() {
		return booking_id;
	}
	public void setBooking_id(int booking_id) {
		this.booking_id = booking_id;
	}
	public String getR_name() {
		return r_name;
	}
	public void setR_name(String r_name) {
		this.r_name = r_name;
	}
	public String getR_phone() {
		return r_phone;
	}
	public void setR_phone(String r_phone) {
		this.r_phone = r_phone;
	}
	public String getRoomid() {
		return roomid;
	}
	public void setRoomid(String roomid) {
		this.roomid = roomid;
	}
	public String getcName() {
		return cName;
	}
	public void setcName(String cName) {
		this.cName = cName;
	}
	public String getvPhone() {
		return vPhone;
	}
	public void setvPhone(String vPhone) {
		this.vPhone = vPhone;
	}
	public String getR_Type() {
		return r_Type;
	}
	public void setR_Type(String r_Type) {
		this.r_Type = r_Type;
	}
	public String getPa_time() {
		return pa_time;
	}
	public void setPa_time(String pa_time) {
		this.pa_time = pa_time;
	}
	public String getKeep_time() {
		return keep_time;
	}
	public void setKeep_time(String keep_time) {
		this.keep_time = keep_time;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Reserveadd(int booking_id, String r_name, String r_phone, String roomid, String cName, String vPhone,
			String r_Type, String pa_time, String keep_time, String remark) {
		super();
		this.booking_id = booking_id;
		this.r_name = r_name;
		this.r_phone = r_phone;
		this.roomid = roomid;
		this.cName = cName;
		this.vPhone = vPhone;
		this.r_Type = r_Type;
		this.pa_time = pa_time;
		this.keep_time = keep_time;
		this.remark = remark;
	}
	public Reserveadd() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "Reserveadd [booking_id=" + booking_id + ", r_name=" + r_name + ", r_phone=" + r_phone + ", roomid="
				+ roomid + ", cName=" + cName + ", vPhone=" + vPhone + ", r_Type=" + r_Type + ", pa_time=" + pa_time
				+ ", keep_time=" + keep_time + ", remark=" + remark + "]";
	}
	
	

}

