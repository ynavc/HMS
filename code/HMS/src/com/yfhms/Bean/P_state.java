package com.yfhms.Bean;
//客户状态表
public class P_state {
	private int pnumber;//客户状态编号
	private String state;//客户状态
	
	public int getpnumber() {
		return pnumber;
	}
	public void setpnumber(int pnumber) {
		this.pnumber = pnumber;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	public P_state(int pnumber, String state) {
		super();
		this.pnumber = pnumber;
		this.state = state;
	}
	public P_state() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "P_state [pnumber=" + pnumber + ", state=" + state + "]";
	}
	
	
	
	

}
