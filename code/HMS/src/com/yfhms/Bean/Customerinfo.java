package com.yfhms.Bean;
//顾客信息表
public class Customerinfo {
	private int cId;//客户编号
	private int cType;//客户类型编号
	private String cName;//客户名字
	private String cSex;//客户性别
	private String dType;//证件类型
	private String no;//证件编号
	private String address;//详细地址
	private String vPhone;//客户联系电话
	private String notes;//客户联系电话
	public Customerinfo() {
		super();
	}
	public Customerinfo(int cId, int cType, String cName, String cSex, String dType, String no, String address,
			String vPhone, String notes) {
		super();
		this.cId = cId;
		this.cType = cType;
		this.cName = cName;
		this.cSex = cSex;
		this.dType = dType;
		this.no = no;
		this.address = address;
		this.vPhone = vPhone;
		this.notes = notes;
	}
	@Override
	public String toString() {
		return "Customerinfo [cId=" + cId + ", cType=" + cType + ", cName=" + cName + ", cSex=" + cSex + ", dType="
				+ dType + ", no=" + no + ", address=" + address + ", vPhone=" + vPhone + ", notes=" + notes + "]";
	}
	public int getcId() {
		return cId;
	}
	public void setcId(int cId) {
		this.cId = cId;
	}
	public int getcType() {
		return cType;
	}
	public void setcType(int cType) {
		this.cType = cType;
	}
	public String getcName() {
		return cName;
	}
	public void setcName(String cName) {
		this.cName = cName;
	}
	public String getcSex() {
		return cSex;
	}
	public void setcSex(String cSex) {
		this.cSex = cSex;
	}
	public String getdType() {
		return dType;
	}
	public void setdType(String dType) {
		this.dType = dType;
	}
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getvPhone() {
		return vPhone;
	}
	public void setvPhone(String vPhone) {
		this.vPhone = vPhone;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	
}
