package com.yfhms.Bean;

public class MainRoomTypeInfo {
	//宾客姓名 预设单价 房间电话 所在区域 进店时间 已用时间 已交押金 应收金额
	//c_name,price,room_phone,location,in_time,foregift
	private String c_name;//宾客姓名
	private String price;//预设单价
	private String room_phone;//房间电话
	private String location;//所在区域
	private String in_time;//进店时间
	private String elapsed_time;//已用时间
	private String foregift;//已交押金
	private String amount_receivable;//应收金额
	@Override
	public String toString() {
		return "MainRoomTypeInfo [c_name=" + c_name + ", price=" + price + ", room_phone=" + room_phone + ", location="
				+ location + ", in_time=" + in_time + ", elapsed_time=" + elapsed_time + ", foregift=" + foregift
				+ ", amount_receivable=" + amount_receivable + "]";
	}
	public MainRoomTypeInfo() {
		super();
	}
	public MainRoomTypeInfo(String c_name, String price, String room_phone, String location, String in_time,
			String elapsed_time, String foregift, String amount_receivable) {
		super();
		this.c_name = c_name;
		this.price = price;
		this.room_phone = room_phone;
		this.location = location;
		this.in_time = in_time;
		this.elapsed_time = elapsed_time;
		this.foregift = foregift;
		this.amount_receivable = amount_receivable;
	}
	public String getC_name() {
		return c_name;
	}
	public void setC_name(String c_name) {
		this.c_name = c_name;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getRoom_phone() {
		return room_phone;
	}
	public void setRoom_phone(String room_phone) {
		this.room_phone = room_phone;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getIn_time() {
		return in_time;
	}
	public void setIn_time(String in_time) {
		this.in_time = in_time;
	}
	public String getElapsed_time() {
		return elapsed_time;
	}
	public void setElapsed_time(String elapsed_time) {
		this.elapsed_time = elapsed_time;
	}
	public String getForegift() {
		return foregift;
	}
	public void setForegift(String foregift) {
		this.foregift = foregift;
	}
	public String getAmount_receivable() {
		return amount_receivable;
	}
	public void setAmount_receivable(String amount_receivable) {
		this.amount_receivable = amount_receivable;
	}
	
}
