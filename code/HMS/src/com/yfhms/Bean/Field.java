package com.yfhms.Bean;

import java.sql.Date;

public class Field {
	private int chkNo;//结算时间                //结算表
	private int roomid;//房间编号
	private String cName;//客户名字
	private double foregift;//押金
	private double money;//金额
	private Date chk_time;//结账时间
	private String remark;//beizhu
	public int getChkNo() {
		return chkNo;
	}
	public void setChkNo(int chkNo) {
		this.chkNo = chkNo;
	}
	public int getRoomid() {
		return roomid;
	}
	public void setRoomid(int roomid) {
		this.roomid = roomid;
	}
	public String getcName() {
		return cName;
	}
	public void setcName(String cName) {
		this.cName = cName;
	}
	public double getForegift() {
		return foregift;
	}
	public void setForegift(double foregift) {
		this.foregift = foregift;
	}
	public double getMoney() {
		return money;
	}
	public void setMoney(double money) {
		this.money = money;
	}
	public Date getChk_time() {
		return chk_time;
	}
	public void setChk_time(Date date) {
		this.chk_time = date;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Field(int chkNo, int roomid, String cName, double foregift, double money, Date chk_time, String remark) {
		super();
		this.chkNo = chkNo;
		this.roomid = roomid;
		this.cName = cName;
		this.foregift = foregift;
		this.money = money;
		this.chk_time = chk_time;
		this.remark = remark;
	}
	public Field() {
		super();
	}
	
	
}
	
