package com.yfhms.Bean;

public class MainAllRoomtype {
	private String account;
	private String money;
	private String chk_time;
	private String in_no;
	private String discount;
	private String roomid;
	private String price;
	private String r_type;
	public MainAllRoomtype() {
		// account,money,checkout.chk_time,livein.in_no,discount,roominfo.roomid,price,r_type
	}
	public MainAllRoomtype(String account, String money, String chk_time, String in_no, String discount, String roomid,
			String price, String r_type) {
		super();
		this.account = account;
		this.money = money;
		this.chk_time = chk_time;
		this.in_no = in_no;
		this.discount = discount;
		this.roomid = roomid;
		this.price = price;
		this.r_type = r_type;
	}
	@Override
	public String toString() {
		return "MainAllRoomtype [account=" + account + ", money=" + money + ", chk_time=" + chk_time + ", in_no="
				+ in_no + ", discount=" + discount + ", roomid=" + roomid + ", price=" + price + ", r_type=" + r_type
				+ "]";
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getMoney() {
		return money;
	}
	public void setMoney(String money) {
		this.money = money;
	}
	public String getChk_time() {
		return chk_time;
	}
	public void setChk_time(String chk_time) {
		this.chk_time = chk_time;
	}
	public String getIn_no() {
		return in_no;
	}
	public void setIn_no(String in_no) {
		this.in_no = in_no;
	}
	public String getDiscount() {
		return discount;
	}
	public void setDiscount(String discount) {
		this.discount = discount;
	}
	public String getRoomid() {
		return roomid;
	}
	public void setRoomid(String roomid) {
		this.roomid = roomid;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getR_type() {
		return r_type;
	}
	public void setR_type(String r_type) {
		this.r_type = r_type;
	}
	
}
