package com.yfhms.Bean;

public class Checkout_info {
	//roominfo.roomid,price,discount,price*discount,checkout.days,money,checkout.chk_time
	//房间号 单价 折扣 折扣价 消费天数 消费金额 消费时间
	private String roomid;//房间号
	private String price;//单价
	private String discount;//折扣
	private String discount_price;//折扣价
	private String consumption_days;//消费天数
	private String consumption_money;//消费金额
	private String consumption_time;//消费时间
	private String statement_no;//结账单号
	private String statement_roomId;//结账房间
	private String guest_name;//宾客姓名
	private String should_money;//应收金额
	private String deposit;//以后押金
	private String actual_money;//实收金额
	@Override
	public String toString() {
		return "Checkout_info [roomid=" + roomid + ", price=" + price + ", discount=" + discount + ", discount_price="
				+ discount_price + ", consumption_days=" + consumption_days + ", consumption_money=" + consumption_money
				+ ", consumption_time=" + consumption_time + ", statement_no=" + statement_no + ", statement_roomId="
				+ statement_roomId + ", guest_name=" + guest_name + ", should_money=" + should_money + ", deposit="
				+ deposit + ", actual_money=" + actual_money + "]";
	}
	public Checkout_info() {
		super();
	}
	public Checkout_info(String roomid, String price, String discount, String discount_price, String consumption_days,
			String consumption_money, String consumption_time, String statement_no, String statement_roomId,
			String guest_name, String should_money, String deposit, String actual_money) {
		super();
		this.roomid = roomid;
		this.price = price;
		this.discount = discount;
		this.discount_price = discount_price;
		this.consumption_days = consumption_days;
		this.consumption_money = consumption_money;
		this.consumption_time = consumption_time;
		this.statement_no = statement_no;
		this.statement_roomId = statement_roomId;
		this.guest_name = guest_name;
		this.should_money = should_money;
		this.deposit = deposit;
		this.actual_money = actual_money;
	}
	public String getRoomid() {
		return roomid;
	}
	public void setRoomid(String roomid) {
		this.roomid = roomid;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getDiscount() {
		return discount;
	}
	public void setDiscount(String discount) {
		this.discount = discount;
	}
	public String getDiscount_price() {
		return discount_price;
	}
	public void setDiscount_price(String discount_price) {
		this.discount_price = discount_price;
	}
	public String getConsumption_days() {
		return consumption_days;
	}
	public void setConsumption_days(String consumption_days) {
		this.consumption_days = consumption_days;
	}
	public String getConsumption_money() {
		return consumption_money;
	}
	public void setConsumption_money(String consumption_money) {
		this.consumption_money = consumption_money;
	}
	public String getConsumption_time() {
		return consumption_time;
	}
	public void setConsumption_time(String consumption_time) {
		this.consumption_time = consumption_time;
	}
	public String getStatement_no() {
		return statement_no;
	}
	public void setStatement_no(String statement_no) {
		this.statement_no = statement_no;
	}
	public String getStatement_roomId() {
		return statement_roomId;
	}
	public void setStatement_roomId(String statement_roomId) {
		this.statement_roomId = statement_roomId;
	}
	public String getGuest_name() {
		return guest_name;
	}
	public void setGuest_name(String guest_name) {
		this.guest_name = guest_name;
	}
	public String getShould_money() {
		return should_money;
	}
	public void setShould_money(String should_money) {
		this.should_money = should_money;
	}
	public String getDeposit() {
		return deposit;
	}
	public void setDeposit(String deposit) {
		this.deposit = deposit;
	}
	public String getActual_money() {
		return actual_money;
	}
	public void setActual_money(String actual_money) {
		this.actual_money = actual_money;
	}
	
}
