package com.yfhms.View;

import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import javax.swing.border.EtchedBorder;
import javax.swing.table.DefaultTableModel;

import java.awt.Font;
import java.awt.Image;

import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.border.BevelBorder;

import com.yfhms.Bean.Customerinfo;
import com.yfhms.Bean.Livein;
import com.yfhms.Controller.Select;
import com.yfhms.Controller.Updata;
import com.yfhms.Utils.MyDocument;
import com.yfhms.Utils.TimeOrder;
import com.yfhms.Utils.ValidateUtils;

import javax.swing.border.CompoundBorder;

public class IndividualOrdersAdd extends JFrame{
	Select select = new Select();
	Updata updata = new Updata();
	ValidateUtils v = new ValidateUtils();
	private final JPanel panel_1 = new JPanel();
	private JTextField textField_zjbm;
	private JTextField textField_zkxm;
	private JTextField textField_bkrs;
	private JTextField textField_ddxx;
	private JTextField textField_sjhm;
	private JTextField textField_zkbl;
	private JTextField textField_sjdj;
	private JTextField textField_yzts;
	private JTextField textField_ssyj;
	String t1,t2,t3;//主客房间数据、房间类型数据、预报单价数
//	double t3;
	JLabel t_zkfj,t_fjlx,t_price;//主客房间、房间类型、预预报单
	String guestsType;//获取宾客类型
	double discount=1,price,actualPrice;//获取折扣比率,//实际价格
	String roomNum;//保存表格中的数据
	int row,col;//获取表格的行和列
	private JTextField textField_bzxx;
	public IndividualOrdersAdd() {
		super("散客开单");
		this.setBounds(0, 0, 770, 750);
		this.setLocationRelativeTo(null);//让窗口在屏幕中间显示
		this.setResizable(false);//让窗口大小不可改变
		getContentPane().setLayout(null);
//		this.getContentPane().setBackground(Color.decode("#E7D7B7"));//设置背景颜色
		
//		panel_1.setBackground(Color.decode("#C7B78F"));
		panel_1.setBounds(0, 0, 765, 30);
		getContentPane().add(panel_1);
		
		JLabel kdxx = new JLabel("开单信息");
		kdxx.setFont(new Font("微软雅黑 Light", Font.BOLD, 16));
		panel_1.add(kdxx);
		
		JLabel zklx = new JLabel("主客房间");
		zklx.setFont(new Font("宋体", Font.BOLD, 15));
		zklx.setBounds(39, 34, 188, 33);
		getContentPane().add(zklx);
		
		JLabel fjlx = new JLabel("房间类型");
		fjlx.setFont(new Font("宋体", Font.BOLD, 15));
		fjlx.setBounds(267, 34, 188, 30);
		getContentPane().add(fjlx);
		
		JLabel ybdj = new JLabel("预报单价");
		ybdj.setToolTipText("");
		ybdj.setFont(new Font("宋体", Font.BOLD, 15));
		ybdj.setBounds(533, 37, 188, 33);
		getContentPane().add(ybdj);
		
		t_zkfj = new JLabel(t1);
		t_zkfj.setForeground(Color.BLUE);
		t_zkfj.setFont(new Font("宋体", Font.BOLD, 18));
		t_zkfj.setBounds(137, 34, 72, 33);
		getContentPane().add(t_zkfj);
//		t_zkfj.setVisible(true);
		
		t_fjlx = new JLabel(t2);
		t_fjlx.setForeground(Color.BLUE);
		t_fjlx.setFont(new Font("宋体", Font.BOLD, 17));
		t_fjlx.setBounds(357, 34, 98, 33);
		getContentPane().add(t_fjlx);
//		t_fjlx.setVisible(true);
		
		t_price = new JLabel("");
		t_price.setFont(new Font("宋体", Font.BOLD, 18));
		t_price.setForeground(Color.RED);
		t_price.setBounds(622, 34, 99, 33);
		getContentPane().add(t_price);
//		t_price.setVisible(true);
		
		JPanel panel = new JPanel();
		panel.setBorder(new EtchedBorder(EtchedBorder.RAISED, null, null));
//		panel.setBackground(new Color(231, 215, 183));
		panel.setBounds(-12, 69, 777, 284);
		getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel zjlx = new JLabel("证件类型：");
		zjlx.setFont(new Font("宋体", Font.PLAIN, 15));
		zjlx.setBounds(25, 16, 89, 18);
		panel.add(zjlx);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(111, 13, 96, 24);
//		Object[][] idtype = select.getIDType();
//		for (int i = 0; i < idtype.length; i++) {
//			System.out.println(idtype[i][0]);
//			comboBox.addItem((idtype[i][0]));
//		}
		comboBox.addItem("身份证");
		comboBox.addItem("居住证");
		comboBox.addItem("签证");
		comboBox.addItem("护照");
		comboBox.addItem("户口本");
		comboBox.addItem("军人证");
		panel.add(comboBox);
		
		JLabel bklx = new JLabel("宾客类型：");
		bklx.setFont(new Font("宋体", Font.PLAIN, 15));
		bklx.setBounds(25, 55, 89, 18);
		panel.add(bklx);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setBounds(111, 52, 96, 24);
		Object[][] guestsType = select.getGuestsType();
		for (int i = 0; i < guestsType.length; i++) {
			System.out.println(guestsType[i][0]);
			comboBox_1.addItem((guestsType[i][0]));
		}
		panel.add(comboBox_1);
		comboBox_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String guestsType = comboBox_1.getSelectedItem().toString();
				discount = Double.parseDouble(select.getDiscount(guestsType));
				textField_zkbl.setText(String.valueOf(discount));
			}
		});
		
		JLabel zjbm = new JLabel("证件编码：");
		zjbm.setFont(new Font("宋体", Font.PLAIN, 15));
		zjbm.setBounds(270, 13, 89, 18);
		panel.add(zjbm);
		
		textField_zjbm = new JTextField();
//		textField_zjbm.setDocument(new MyDocument.DoubleOnlyDocument());
		textField_zjbm.setBounds(354, 10, 152, 24);
		panel.add(textField_zjbm);
		textField_zjbm.setColumns(10);
		
		
		JLabel zkxm = new JLabel("主客姓名：");
		zkxm.setFont(new Font("宋体", Font.PLAIN, 15));
		zkxm.setBounds(270, 55, 89, 18);
		panel.add(zkxm);
		
		textField_zkxm = new JTextField();
		textField_zkxm.setColumns(10);
		textField_zkxm.setBounds(354, 52, 152, 24);
		panel.add(textField_zkxm);
		
		JLabel zkxb = new JLabel("主客性别：");
		zkxb.setFont(new Font("宋体", Font.PLAIN, 15));
		zkxb.setBounds(554, 15, 101, 21);
		panel.add(zkxb);
		
		JComboBox comboBox_zkxb = new JComboBox();
		comboBox_zkxb.setBounds(639, 10, 96, 24);
		comboBox_zkxb.addItem("男");
		comboBox_zkxb.addItem("女");
		panel.add(comboBox_zkxb);
		
		JLabel bkrs = new JLabel("宾客人数：");
		bkrs.setFont(new Font("宋体", Font.PLAIN, 15));
		bkrs.setBounds(554, 55, 89, 18);
		panel.add(bkrs);
		
		textField_bkrs = new JTextField();
		textField_bkrs.setDocument(new MyDocument.DoubleOnlyDocument());
		textField_bkrs.setColumns(10);
		textField_bkrs.setBounds(639, 52, 96, 24);
		textField_bkrs.setText("1");
		panel.add(textField_bkrs);
		
		JLabel dzxx = new JLabel("地址信息：");
		dzxx.setFont(new Font("宋体", Font.PLAIN, 15));
		dzxx.setBounds(25, 98, 89, 18);
		panel.add(dzxx);
		
		textField_ddxx = new JTextField();
		textField_ddxx.setBounds(111, 91, 622, 32);
		panel.add(textField_ddxx);
		textField_ddxx.setColumns(10);
		
		JLabel sjhm = new JLabel("手机号码：");
		sjhm.setFont(new Font("宋体", Font.PLAIN, 15));
		sjhm.setBounds(25, 145, 89, 18);
		panel.add(sjhm);
		
		textField_sjhm = new JTextField();
//		textField_sjhm.setDocument(new MyDocument.DoubleOnlyDocument());
		textField_sjhm.setColumns(10);
		textField_sjhm.setBounds(111, 138, 204, 32);
		panel.add(textField_sjhm);
		
		JLabel kzxx = new JLabel("备注信息：");
		kzxx.setFont(new Font("宋体", Font.PLAIN, 15));
		kzxx.setBounds(354, 145, 89, 18);
		panel.add(kzxx);
		
		textField_bzxx = new JTextField();
		textField_bzxx.setColumns(10);
		textField_bzxx.setBounds(440, 138, 295, 32);
		panel.add(textField_bzxx);
		
		JLabel zkbl = new JLabel("折扣比例:");
		zkbl.setFont(new Font("宋体", Font.PLAIN, 15));
		zkbl.setBounds(25, 191, 89, 18);
		panel.add(zkbl);
		
		textField_zkbl = new JTextField();
		textField_zkbl.setEnabled(false);
		textField_zkbl.setFont(new Font("宋体", Font.BOLD, 20));
		textField_zkbl.setBounds(111, 188, 70, 24);
		panel.add(textField_zkbl);
		textField_zkbl.setText(String.valueOf(discount));
//		textField_zkbl.setColumns(10);
		
		JLabel sjdj = new JLabel("实际单价：");
		sjdj.setFont(new Font("宋体", Font.PLAIN, 15));
		sjdj.setBounds(211, 191, 89, 18);
		panel.add(sjdj);
		
		textField_sjdj = new JTextField();
		textField_sjdj.setForeground(Color.RED);
		textField_sjdj.setFont(new Font("宋体", Font.BOLD, 20));
		textField_sjdj.setEditable(false);
		textField_sjdj.setEnabled(false);
		textField_sjdj.setColumns(10);
		textField_sjdj.setBounds(297, 188, 86, 24);
		panel.add(textField_sjdj);
		
		JLabel ybts = new JLabel("预祝天数：");
		ybts.setFont(new Font("宋体", Font.PLAIN, 15));
		ybts.setBounds(400, 194, 89, 18);
		panel.add(ybts);
		
		textField_yzts = new JTextField();
		textField_yzts.setDocument(new MyDocument.NumOnlyDocument());
		textField_yzts.setFont(new Font("宋体", Font.BOLD, 20));
		textField_yzts.setColumns(10);
		textField_yzts.setBounds(489, 191, 70, 24);
		panel.add(textField_yzts);
		textField_yzts.setText("1");
		textField_yzts.addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent e) {
				//获取输入文本框中字符的长度
				int length=textField_yzts.getText().length();
				if(length>10){
					JOptionPane.showMessageDialog(null,"内容长度不能超过6位");
				}
				actualPrice = Double.parseDouble(textField_yzts.getText())*price;
				textField_ssyj.setText(String.valueOf(actualPrice));
			}
			@Override
			public void focusGained(FocusEvent e) {
				textField_ssyj.setText(String.valueOf(actualPrice));
			}
		});;
		
		JLabel ssyj = new JLabel("实收押金：");
		ssyj.setFont(new Font("宋体", Font.PLAIN, 15));
		ssyj.setBounds(573, 194, 89, 18);
		panel.add(ssyj);
		
		textField_ssyj = new JTextField();
		textField_ssyj.setDocument(new MyDocument.MyRegExp("\\d*(\\.\\d{0,2})?"));
		textField_ssyj.setFont(new Font("宋体", Font.BOLD, 20));
		textField_ssyj.setColumns(10);
		textField_ssyj.setBounds(649, 191, 86, 24);
		panel.add(textField_ssyj);
		
		JCheckBox checkBox = new JCheckBox("钟点房");
//		checkBox.setBackground(Color.decode("#E7D7B7"));
		checkBox.setBounds(74, 237, 133, 27);
		panel.add(checkBox);
		
		JCheckBox checkBox_1 = new JCheckBox("到预祝天数时自动提醒");
		checkBox_1.setSelected(true);
//		checkBox_1.setBackground(new Color(231, 215, 183));
		checkBox_1.setBounds(211, 237, 186, 27);
		panel.add(checkBox_1);
		
		//创建选项卡面板
		JTabbedPane tabbedPane=new JTabbedPane();
		tabbedPane.setBounds(14, 366, 724, 236);
		getContentPane().add(tabbedPane);
		tabbedPane.setBackground(Color.decode("#DBC9ED"));
		
		JPanel p1 = new JPanel() ;  //创建面板组件容器
//		p1.setBackground(new Color(231, 215, 183));
		p1.setBorder(new CompoundBorder());
		p1.setLayout(null);
		tabbedPane.addTab("追加房间", p1);
		
		Object[] columnNames = {"可供房间"};
		Object[][] data = select.getForTheRoom();//查询房间信息
		DefaultTableModel df = new DefaultTableModel(data,columnNames);
		JTable table = new JTable(df);
		int v=ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED;//水平滚动
		int h=ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED;//垂直滚动
		JScrollPane scr = new JScrollPane(table,v,h);
		scr.setBounds(47, 13, 254, 178);
		p1.add(scr);
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				row = table.getSelectedRow();
				col =table.getSelectedColumn();
				roomNum = table.getValueAt(row, col).toString();
				t1 = roomNum;
				t_zkfj.setText(t1);
				t2 = select.getRoomtype(t1);
				System.out.println(t2);
				t_fjlx.setText(t2);
				t3 = select.getprice(t1);
				t_price.setText(String.valueOf(t3));
				price = discount*Double.parseDouble(t3);
				System.out.println(t3);
				System.out.println(discount);
				System.out.println(price);
				textField_sjdj.setText(String.valueOf(price));
				
				actualPrice = Double.parseDouble(textField_yzts.getText())*price;
				textField_ssyj.setText(String.valueOf(actualPrice));
			}
		});
		
//		table.getSelectedColumn();
		
		Object[] columnNames2 = {"开单房间"};
		Object[][] data2 = {};
		DefaultTableModel df2 = new DefaultTableModel(data2, columnNames2);
		JTable table2 = new JTable(df2);
		JScrollPane scr2 = new JScrollPane(table2,v,h);
		scr2.setBounds(415, 13, 254, 178);
		p1.add(scr2);
		table2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
			}
		});
		
		
		ImageIcon right = new ImageIcon(this.getClass().getResource("img/right.gif"));
		right.setImage(right.getImage().getScaledInstance(20,20,Image.SCALE_DEFAULT));
		JButton btnNewButton1 = new JButton(right);
		btnNewButton1.setContentAreaFilled(false);//设置按钮透明背景
		btnNewButton1.setFocusPainted(false);
		btnNewButton1.setBounds(344, 64, 20, 20);
		p1.add(btnNewButton1);
		btnNewButton1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Vector<Vector<Object>> vector =new Vector<Vector<Object>>();//创建一个保存的集合
				Vector<Object> object1 = new Vector<Object>();//创建一个集合，用于创建列名
				object1.add("开单房间");//将列名添加到集合object1
				Vector<Object> object =new Vector<Object>();//再创建一个集合，用于加入到第一个集合
				System.out.println(roomNum);
				object.add(roomNum);//数据取出放入第二个集合中
				vector.add(object);//将第二集合中的数据加入到第一个集合中
				DefaultTableModel model = (DefaultTableModel)table2.getModel();//取得table2的模板
				model.setDataVector(vector, object1);//为模板添加内容
				table2.setModel(model);//将模板添加到table2中
//				df.removeRow(row);//删除某行
			}
		});
		
		ImageIcon left = new ImageIcon(this.getClass().getResource("img/left.gif"));
		left.setImage(left.getImage().getScaledInstance(20,20,Image.SCALE_DEFAULT));
		JButton btnNewButton_1 = new JButton(left);
		btnNewButton_1.setFocusPainted(false);
		btnNewButton_1.setContentAreaFilled(false);
		btnNewButton_1.setBounds(344, 110, 20, 20);
		p1.add(btnNewButton_1);
		btnNewButton_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
			}
		}); 
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
//		panel_2.setBackground(new Color(231, 215, 183));
		panel_2.setBounds(-12, 598, 777, 41);
		getContentPane().add(panel_2);
		panel_2.setLayout(null);
		
		JLabel label_7 = new JLabel("注：只能追加同类房间，最多1间！若要追加不同类型的房间请选择“团体开单“");
		label_7.setForeground(new Color(255, 0, 0));
		label_7.setFont(new Font("微软雅黑", Font.BOLD, 15));
		label_7.setBounds(34, 13, 613, 18);
		panel_2.add(label_7);
		
		Font f = new Font("微软雅黑", Font.BOLD, 17);
		
		ImageIcon i1 = new ImageIcon(this.getClass().getResource("img/modi3.gif"));
		JButton qd = new JButton("   确定",i1);
		qd.setFont(f);
		qd.setBounds(211, 652, 130, 38);
//		qd.setContentAreaFilled(false);//设置按钮透明背景
		qd.setFocusPainted(false);
		getContentPane().add(qd);
		Customerinfo c = new Customerinfo();
		qd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String time = TimeOrder.getTime();
				ValidateUtils v = new ValidateUtils();//调用封装工具类验证输入的数据
				//得到客户类型对应的ID
				String c_IdSQL = "SELECT c_id FROM customertype WHERE c_type='"+comboBox_1.getSelectedItem()+"';";
				String ctype_id = select.getString(c_IdSQL);
				String cid="";//客户编号
				c.setcType(Integer.valueOf(ctype_id));//客户类型编号
				c.setcName(textField_zkxm.getText());//客户姓名
				c.setcSex(comboBox_zkxb.getSelectedItem().toString());//客户性别
				c.setdType(comboBox.getSelectedItem().toString());//证件类型
				boolean c1,c2;
				c1=v.IDcard(textField_zjbm.getText());
				c2=v.Mobile(textField_sjhm.getText());
				if (c1) {
					c.setNo(textField_zjbm.getText());//证件编码
				} else {
					JOptionPane.showMessageDialog(null, "身份证号码错误！请检查！");
				}
				c.setAddress(textField_ddxx.getText());//地址信息
				if (c2) {
					c.setvPhone(textField_sjhm.getText());//电话信息
				} else {
					JOptionPane.showMessageDialog(null, "手机号码错误！请检查！");
				}
				c.setNotes(textField_bzxx.getText());//备注信息
				if (c1&&c2) {
					String c_sql = "INSERT INTO customerinfo VALUES (null, '"+c.getcType()+"', '"+c.getcName()+"', '"+c.getcSex()+"', '"+c.getdType()+"', '"+c.getNo()+"', '"+c.getAddress()+"', '"+c.getvPhone()+"', '"+c.getNotes()+"');";
					int reselt1 = updata.addData(c_sql);
					if (reselt1>0) {
						SimpleDateFormat sfDate = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss");
						String strDate = sfDate.format(new Date());
						Livein l = new Livein();
//						l.setIn_no(Integer.parseInt(time));//入住单号
						l.setRoomid(Integer.parseInt(roomNum));//房间编号
						String live_ctypeidSQl = "SELECT c_id FROM customerinfo WHERE no='"+c.getNo()+"';";
						String live_ctypeid = select.getString(live_ctypeidSQl);
						l.setC_id(Integer.parseInt(live_ctypeid));//客户编号
						l.setNumber(Integer.parseInt(textField_bkrs.getText()));//人数
						l.setForegift(Double.valueOf(textField_ssyj.getText()));//押金
						l.setDays(Integer.parseInt(textField_yzts.getText()));//预祝天数
						l.setStatus(1);//当前状态
						l.setIn_time(strDate);//入住时间
						l.setChk_time(strDate);//结算时间
						System.out.println(time);
						String i_sql = "INSERT INTO livein VALUES (null, '"+l.getRoomid()+"', '"+l.getC_id()+"', '"+l.getNumber()+"', '"+l.getForegift()+"', '"+l.getDays()+"', '1', '"+l.getIn_time()+"', '"+l.getChk_time()+"');";
						int reselt2 = updata.addData(i_sql);
						if (reselt2>0) {
							String updateRoomintoSQL = "UPDATE roominfo set state=4 WHERE roomid='"+l.getRoomid()+"'";
							int reselt3 = updata.addData(updateRoomintoSQL);
							if (reselt3>0) {
								JOptionPane.showMessageDialog(null, "添加成功！");
								MainJFrame m = new MainJFrame();
								m.dispose();
								m.setVisible(true);
								dispose();
							} else {
								JOptionPane.showMessageDialog(null, "添加失败！");
							}
						} else {
							JOptionPane.showMessageDialog(null, "房间信息，添加失败！");
						}
					} else {
						JOptionPane.showMessageDialog(null, "客户信息，添加失败！");
					}
					
//					String 
				} else {
					JOptionPane.showMessageDialog(null, "请检查添加的信息！！");
				}
			}
		});
		
		ImageIcon i2 = new ImageIcon(this.getClass().getResource("img/cancel.gif"));
		JButton qx = new JButton("   取消",i2);
		qx.setFont(f);
		qx.setBounds(398, 652, 130, 38);
//		qx.setContentAreaFilled(false);//设置按钮透明背景
		qx.setFocusPainted(false);
		getContentPane().add(qx);
		qx.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
	}
}
