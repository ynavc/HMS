package com.yfhms.View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import java.awt.FlowLayout;
import javax.swing.JMenuBar;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.JCheckBox;
import javax.swing.JScrollPane;
import java.awt.GridLayout;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JRadioButton;
import javax.swing.border.EtchedBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.BevelBorder;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.table.DefaultTableModel;

import com.yfhms.Controller.QuerY;

import javax.swing.ScrollPaneConstants;
import javax.swing.JToolBar;

public class Business extends JFrame {

	private JPanel contentPane;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField textField_8;
	private JTextField textField_9;
	private JTable table_2;
	private JTable table_3;
	private JTable table_1;
	
	Object[] header = {"账单号","房间号","宾客姓名","已收押金","实收金额","结算时间","备注"};
	Object[][] data;
	JTable jTable;
	DefaultTableModel dt;
	
	//表二
	Object[] header1= {"会员编号","房间号","宾客姓名","性别","证件类型","证件编号","人数","押金","预住天数","当前状态","入住时间","结算时间","结算单号"};
	DefaultTableModel dt1;
	Object[][] data1;
	JTable jT1;

	
	Object[] header2= {"房间号","房间类型","单价","折扣比例","折扣单价","优惠金额","入住时间"};
	Object[][] data2;
	//创建表模型
	DefaultTableModel dt2;
	JTable jT2;
	
	
	Object[][] data3 ;
	//创建表模型
	DefaultTableModel dt3 ;	
	JTable jT3 ;
	Object[] header3= {"会员编号","房间号","宾客姓名","性别","证件类型","证件编号","人数","押金","预住天数","当前状态","入住时间","结算时间","结算单号"};
	public Business() {
//		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 900, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		this.setTitle("营业查询");
		this.setResizable(false);//让窗口大小不可改变
		this.setLocationRelativeTo(null);
		getContentPane().setLayout(null);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		contentPane.add(panel);
		
		//创建面板
		JTabbedPane tabbedPane=new JTabbedPane();
		tabbedPane.setBounds(0, 0, 880,580);
		getContentPane().add(tabbedPane);
		
		JCheckBox chckbxNewCheckBox = new JCheckBox("\u7ED3\u8D26\u65F6\u95F4\uFF1A");
//		chckbxNewCheckBox.setVerticalAlignment(SwingConstants.BOTTOM);
		chckbxNewCheckBox.setBounds(20, 100, 300, 50);
		chckbxNewCheckBox.setContentAreaFilled(false);

		
		JPanel bkselect = new JPanel();//创建面板组件容器
//		bkselect.setBackground(Color.decode("#E7D7B7"));
		tabbedPane.addTab("全部宾客查询",  new ImageIcon(this.getClass().getResource("img/u02.gif")), bkselect, null);
//		tabbedPane.setBackgroundAt(0,  Color.decode("#B8AD97"));
		bkselect.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("\u5BBE\u5BA2\u59D3\u540D/\u8BC1\u4EF6\u7F16\u53F7/\u623F\u95F4\u53F7\uFF1A");
		lblNewLabel.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		lblNewLabel.setBounds(44, 13, 196, 25);
		bkselect.add(lblNewLabel);
		
		textField = new JTextField();
		textField.setBounds(254, 14, 104, 24);
		bkselect.add(textField);
		textField.setColumns(10);
		
		JButton button_2 = new JButton("   查询", new ImageIcon(this.getClass().getResource("img/b1.gif")));//查询按钮
		button_2.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		button_2.setBounds(400, 13, 113, 27);
		bkselect.add(button_2);
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {//查询
				String fjh=textField.getText();
				if (fjh.equals("")) {
					String sql="SELECT 入住信息表.in_no 账单号,房间信息表.roomid 房间号, c_name 宾客姓名,foregift 已收押金,money 实收金额, 结算表.chk_time,remark from \r\n" + 
							"checkout 结算表,\r\n" + 
							"customerinfo 客户信息表,\r\n" + 
							"customertype 客户类型表,\r\n" + 
							"livein 入住信息表,\r\n" + 
							"p_state 客户状态表,\r\n" + 
							"r_state 房间状态表,\r\n" + 
							"roominfo 房间信息表,\r\n" + 
							"roomtype 房间类型表\r\n" + 
							"WHERE 入住信息表.status = 客户状态表.pnumber\r\n" + 
							"AND 入住信息表.roomid = 房间信息表.roomid\r\n" + 
							"AND 入住信息表.c_id = 客户类型表.c_id\r\n" + 
							"AND 客户信息表.c_type = 客户类型表.c_id\r\n" + 
							"AND 房间信息表.roomtype = 房间类型表.roomtype\r\n" + 
							"AND 房间信息表.state = 房间状态表.number"
							;
					data = QuerY.getSelect(sql);
					dt.setDataVector(data, header);
				} else {
					String sql="SELECT 入住信息表.in_no 账单号,房间信息表.roomid 房间号, c_name 宾客姓名,foregift 已收押金,money 实收金额, 结算表.chk_time,remark from \r\n" + 
							"checkout 结算表,\r\n" + 
							"customerinfo 客户信息表,\r\n" + 
							"customertype 客户类型表,\r\n" + 
							"livein 入住信息表,\r\n" + 
							"p_state 客户状态表,\r\n" + 
							"r_state 房间状态表,\r\n" + 
							"roominfo 房间信息表,\r\n" + 
							"roomtype 房间类型表\r\n" + 
							"WHERE 入住信息表.status = 客户状态表.pnumber\r\n" + 
							"AND 入住信息表.roomid = 房间信息表.roomid\r\n" + 
							"AND 入住信息表.c_id = 客户类型表.c_id\r\n" + 
							"AND 客户信息表.c_type = 客户类型表.c_id\r\n" + 
							"AND 房间信息表.roomtype = 房间类型表.roomtype\r\n" + 
							"AND 房间信息表.state = 房间状态表.number\r\n" + 
							"AND (c_name='"+fjh+" ' or "+"房间信息表.roomid='"+fjh+"' or "+" no='"+fjh+"')";
					data = QuerY.getSelect(sql);
					dt.setDataVector(data, header);
					
				}
//				
			}
		});
		
		
		
		
		JButton btnNewButton = new JButton("   刷新", new ImageIcon(this.getClass().getResource("img/find.gif")));
		btnNewButton.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		btnNewButton.setBounds(534, 13, 113, 27);
		bkselect.add(btnNewButton);
		btnNewButton.addActionListener(new ActionListener() { //刷新监听
			public void actionPerformed(ActionEvent e) {
				
			
					String sql="SELECT 入住信息表.in_no 账单号,房间信息表.roomid 房间号, c_name 宾客姓名,foregift 已收押金,money 实收金额, 结算表.chk_time,remark from \r\n" + 
							"checkout 结算表,\r\n" + 
							"customerinfo 客户信息表,\r\n" + 
							"customertype 客户类型表,\r\n" + 
							"livein 入住信息表,\r\n" + 
							"p_state 客户状态表,\r\n" + 
							"r_state 房间状态表,\r\n" + 
							"roominfo 房间信息表,\r\n" + 
							"roomtype 房间类型表\r\n" + 
							"WHERE 入住信息表.status = 客户状态表.pnumber\r\n" + 
							"AND 入住信息表.roomid = 房间信息表.roomid\r\n" + 
							"AND 入住信息表.c_id = 客户类型表.c_id\r\n" + 
							"AND 客户信息表.c_type = 客户类型表.c_id\r\n" + 
							"AND 房间信息表.roomtype = 房间类型表.roomtype\r\n" + 
							"AND 房间信息表.state = 房间状态表.number"
							;
					data = QuerY.getSelect(sql);
					dt.setDataVector(data, header);
			
//				
			}
		});
		
		JButton btnNewButton_1 = new JButton("今日来宾", new ImageIcon(this.getClass().getResource("img/今日来宾.gif")));
		btnNewButton_1.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		btnNewButton_1.setBounds(670, 13, 113, 27);
		bkselect.add(btnNewButton_1);
		
		String sql="SELECT 入住信息表.in_no 账单号,房间信息表.roomid 房间号, c_name 宾客姓名,foregift 已收押金,money 实收金额, 结算表.chk_time,remark from \r\n" + 
				"checkout 结算表,\r\n" + 
				"customerinfo 客户信息表,\r\n" + 
				"customertype 客户类型表,\r\n" + 
				"livein 入住信息表,\r\n" + 
				"p_state 客户状态表,\r\n" + 
				"r_state 房间状态表,\r\n" + 
				"roominfo 房间信息表,\r\n" + 
				"roomtype 房间类型表\r\n" + 
				"WHERE 入住信息表.status = 客户状态表.pnumber\r\n" + 
				"AND 入住信息表.roomid = 房间信息表.roomid\r\n" + 
				"AND 入住信息表.c_id = 客户类型表.c_id\r\n" + 
				"AND 客户信息表.c_type = 客户类型表.c_id\r\n" + 
				"AND 房间信息表.roomtype = 房间类型表.roomtype\r\n" + 
				"AND 房间信息表.state = 房间状态表.number\r\n" + 
				"";
		data=QuerY.getSelect(sql);
		//创建表模型
		dt=new DefaultTableModel(data,header);

		jTable=new JTable(dt);
		JScrollPane scrollPane_2 = new JScrollPane(jTable);
		scrollPane_2.setBounds(14, 153, 839, 258);
		bkselect.add(scrollPane_2);
		
		
		
		
		JPanel panel_5_1_1_1 = new JPanel();
		panel_5_1_1_1.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panel_5_1_1_1.setBounds(0, 80, 867, 36);
		bkselect.add(panel_5_1_1_1);
		
		JLabel label_5_1_1 = new JLabel("\u5168\u90E8\u5BBE\u5BA2\u67E5\u8BE2");
		label_5_1_1.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		panel_5_1_1_1.add(label_5_1_1);
	//	textField.setColumns(20);
		
		JPanel zdselect = new JPanel();
//		zdselect.setBackground(Color.decode("#E7D7B7"));
		tabbedPane.addTab("在店宾客消费查询",  new ImageIcon(this.getClass().getResource("img/u03.gif")), zdselect, null);
		zdselect.setLayout(null);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("\u5165\u4F4F\u65F6\u95F4\uFF1A");
		rdbtnNewRadioButton.setFont(new Font("微软雅黑", Font.PLAIN, 15));
//		rdbtnNewRadioButton.setBackground(Color.decode("#E7D7B7"));
		rdbtnNewRadioButton.setBounds(108, 26, 103, 27);
		zdselect.add(rdbtnNewRadioButton);
		
		JLabel label = new JLabel("\u8D77\u59CB\u65F6\u95F4");
		label.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		label.setBounds(219, 31, 72, 18);
		zdselect.add(label);
		
		textField_4 = new JTextField();
		textField_4.setBounds(305, 28, 170, 24);
		zdselect.add(textField_4);
		textField_4.setColumns(10);
		
		JLabel label_4 = new JLabel("\u7EC8\u6B62\u65F6\u95F4");
		label_4.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		label_4.setBounds(524, 35, 72, 18);
		zdselect.add(label_4);
		
		textField_5 = new JTextField();
		textField_5.setBounds(610, 28, 170, 24);
		zdselect.add(textField_5);
		textField_5.setColumns(10);
		
		JRadioButton radioButton = new JRadioButton("\u623F\u95F4\u53F7\uFF1A");
		radioButton.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		radioButton.setBounds(108, 84, 103, 27);
//		radioButton.setBackground(Color.decode("#E7D7B7"));
		zdselect.add(radioButton);
		
		textField_6 = new JTextField();
		textField_6.setBounds(205, 85, 150, 24);
		zdselect.add(textField_6);
		textField_6.setColumns(10);
		
		JButton button_3 = new JButton("   \u67E5\u8BE2", new ImageIcon(this.getClass().getResource("img/b1.gif")));
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String cxString=textField_6.getText();
				if (cxString.equals("")) {

					//第二页数据表		
							
							String sql1="SELECT 客户类型表.c_id,房间信息表.roomid 房间号, c_name 宾客姓名,c_sex ,d_type,no,`入住信息表`.number,foregift 已收押金,入住信息表.days,`客户状态表`.state,入住信息表.in_time  ,结算表.chk_time,`结算表`.chk_no from \r\n" + 
									"checkout 结算表,\r\n" + 
									"customerinfo 客户信息表,\r\n" + 
									"customertype 客户类型表,\r\n" + 
									"livein 入住信息表,\r\n" + 
									"p_state 客户状态表,\r\n" + 
									"r_state 房间状态表,\r\n" + 
									"roominfo 房间信息表,\r\n" + 
									"roomtype 房间类型表\r\n" + 
									"WHERE \r\n" + 
									" 入住信息表.status = 客户状态表.pnumber\r\n" + 
									"AND 入住信息表.roomid = 房间信息表.roomid\r\n" + 
									"AND 入住信息表.c_id = 客户信息表.c_id\r\n" + 
									"AND 客户信息表.c_type = 客户类型表.c_id\r\n" + 
									"AND 房间信息表.roomtype = 房间类型表.roomtype\r\n" + 
									"AND 房间信息表.state = 房间状态表.number\r\n" + 
									"and  `客户状态表`.state='入住中'";
							data1=QuerY.getSelect1(sql1);
							 dt1.setDataVector(data1,header1);
							
				} else {
					//第二页数据表		
				
					String sql1="SELECT 客户类型表.c_id,房间信息表.roomid 房间号, c_name 宾客姓名,c_sex ,d_type,no,`入住信息表`.number,foregift 已收押金,入住信息表.days,`客户状态表`.state,入住信息表.in_time  ,结算表.chk_time,`结算表`.chk_no from \r\n" + 
							"checkout 结算表,\r\n" + 
							"customerinfo 客户信息表,\r\n" + 
							"customertype 客户类型表,\r\n" + 
							"livein 入住信息表,\r\n" + 
							"p_state 客户状态表,\r\n" + 
							"r_state 房间状态表,\r\n" + 
							"roominfo 房间信息表,\r\n" + 
							"roomtype 房间类型表\r\n" + 
							"WHERE \r\n" + 
							" 入住信息表.status = 客户状态表.pnumber\r\n" + 
							"AND 入住信息表.roomid = 房间信息表.roomid\r\n" + 
							"AND 入住信息表.c_id = 客户信息表.c_id\r\n" + 
							"AND 客户信息表.c_type = 客户类型表.c_id\r\n" + 
							"AND 房间信息表.roomtype = 房间类型表.roomtype\r\n" + 
							"AND 房间信息表.state = 房间状态表.number\r\n" + 
							"and  `客户状态表`.state='入住中'"+
					       " AND 房间信息表.roomid='"+cxString+"'";
					data1=QuerY.getSelect1(sql1);
				   dt1.setDataVector(data1,header1);
				}
				
			
			}
		});
		button_3.setFont(new Font("微软雅黑", Font.PLAIN, 15));
//		button_3.setFocusPainted(false);
//		button_3.setContentAreaFilled(false);
		button_3.setBounds(514, 84, 100, 30);
		zdselect.add(button_3);
		
		JButton button_1_1 = new JButton("   刷新", new ImageIcon(this.getClass().getResource("img/find.gif")));
		button_1_1.addActionListener(new ActionListener() {//刷新按钮监听
			public void actionPerformed(ActionEvent e) {
					//第二页数据表		
							
							String sql1="SELECT 客户类型表.c_id,房间信息表.roomid 房间号, c_name 宾客姓名,c_sex ,d_type,no,`入住信息表`.number,foregift 已收押金,入住信息表.days,`客户状态表`.state,入住信息表.in_time  ,结算表.chk_time,`结算表`.chk_no from \r\n" + 
									"checkout 结算表,\r\n" + 
									"customerinfo 客户信息表,\r\n" + 
									"customertype 客户类型表,\r\n" + 
									"livein 入住信息表,\r\n" + 
									"p_state 客户状态表,\r\n" + 
									"r_state 房间状态表,\r\n" + 
									"roominfo 房间信息表,\r\n" + 
									"roomtype 房间类型表\r\n" + 
									"WHERE \r\n" + 
									" 入住信息表.status = 客户状态表.pnumber\r\n" + 
									"AND 入住信息表.roomid = 房间信息表.roomid\r\n" + 
									"AND 入住信息表.c_id = 客户信息表.c_id\r\n" + 
									"AND 客户信息表.c_type = 客户类型表.c_id\r\n" + 
									"AND 房间信息表.roomtype = 房间类型表.roomtype\r\n" + 
									"AND 房间信息表.state = 房间状态表.number\r\n" + 
									"and  `客户状态表`.state='入住中'";
							 data1=QuerY.getSelect1(sql1);
							 dt1.setDataVector(data1,header1);
							
				
			}
		});
		button_1_1.setFont(new Font("微软雅黑", Font.PLAIN, 15));
//		button_1_1.setFocusPainted(false);//去除焦点框
//		button_1_1.setContentAreaFilled(false);//去除背景
		button_1_1.setBounds(666, 84, 100, 30);
		zdselect.add(button_1_1);
		
		
//第二页数据表		
		
		String sql1="SELECT 客户类型表.c_id,房间信息表.roomid 房间号, c_name 宾客姓名,c_sex ,d_type,no,`入住信息表`.number,foregift 已收押金,入住信息表.days,`客户状态表`.state,入住信息表.in_time  ,结算表.chk_time,`结算表`.chk_no from \r\n" + 
				"checkout 结算表,\r\n" + 
				"customerinfo 客户信息表,\r\n" + 
				"customertype 客户类型表,\r\n" + 
				"livein 入住信息表,\r\n" + 
				"p_state 客户状态表,\r\n" + 
				"r_state 房间状态表,\r\n" + 
				"roominfo 房间信息表,\r\n" + 
				"roomtype 房间类型表\r\n" + 
				"WHERE \r\n" + 
				" 入住信息表.status = 客户状态表.pnumber\r\n" + 
				"AND 入住信息表.roomid = 房间信息表.roomid\r\n" + 
				"AND 入住信息表.c_id = 客户信息表.c_id\r\n" + 
				"AND 客户信息表.c_type = 客户类型表.c_id\r\n" + 
				"AND 房间信息表.roomtype = 房间类型表.roomtype\r\n" + 
				"AND 房间信息表.state = 房间状态表.number\r\n" + 
				"and  `客户状态表`.state='入住中'";
		 data1=QuerY.getSelect1(sql1);
		//创建表模型
		 dt1=new DefaultTableModel(data1,header1);

		jT1=new JTable(dt1);
	
		
		
		JScrollPane jsc = new JScrollPane(jT1);
		jsc.setBounds(10, 185, 839, 258);
		zdselect.add(jsc);
		
		
		
		
		
		
		JPanel panel_5_1_1_2 = new JPanel();
		panel_5_1_1_2.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panel_5_1_1_2.setBounds(0, 136, 867, 36);
		zdselect.add(panel_5_1_1_2);
		
		JLabel label_5_1_2 = new JLabel("\u5728\u5E97\u5BBE\u5BA2\u67E5\u8BE2");
		label_5_1_2.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		panel_5_1_1_2.add(label_5_1_2);
//		tabbedPane.setBackgroundAt(1, Color.decode("#B8AD97"));
		

	
		JPanel jsselect = new JPanel() ;  //创建面板组件容器
//		jsselect.setBackground(Color.decode("#E7D7B7"));
		jsselect.setLayout(null);
		tabbedPane.addTab("结算单查询", new ImageIcon(this.getClass().getResource("img/u04.gif")),jsselect );//添加按钮
//		tabbedPane.setBackgroundAt(2,  Color.decode("#B8AD97"));
		
			
			JCheckBox jssj = new JCheckBox("\u7ED3\u7B97\u65F6\u95F4\uFF1A");
//			jssj.setBackground(Color.decode("#E7D7B7"));
			jssj.setFont(new Font("微软雅黑", Font.PLAIN, 15));
			jssj.setBounds(43, 34, 104, 27);
			jsselect.add(jssj);
			
			JLabel qssj = new JLabel("\u8D77\u59CB\u65F6\u95F4");
			qssj.setFont(new Font("微软雅黑", Font.PLAIN, 15));
			qssj.setBounds(155, 38, 75, 18);
			jsselect.add(qssj);
			
			textField_1 = new JTextField();
			textField_1.setBounds(244, 36, 150, 24);
			jsselect.add(textField_1);
			textField_1.setColumns(10);
			
			JLabel zzsj = new JLabel("\u7EC8\u6B62\u65F6\u95F4");
			zzsj.setFont(new Font("微软雅黑", Font.PLAIN, 15));
			zzsj.setBounds(444, 38, 72, 18);
			jsselect.add(zzsj);
			
			textField_2 = new JTextField();
			textField_2.setBounds(530, 33, 150, 24);
			jsselect.add(textField_2);
			textField_2.setColumns(10);
			
			JCheckBox chckbxNewCheckBox_2 = new JCheckBox("\u59D3\u540D \u623F\u95F4\u53F7/\u8D26\u5355\u53F7\uFF1A");
//			chckbxNewCheckBox_2.setBackground(Color.decode("#E7D7B7"));
			chckbxNewCheckBox_2.setFont(new Font("微软雅黑", Font.PLAIN, 15));
			chckbxNewCheckBox_2.setBounds(43, 89, 187, 27);
			jsselect.add(chckbxNewCheckBox_2);
			
			textField_3 = new JTextField();
			textField_3.setBounds(244, 90, 150, 24);
			jsselect.add(textField_3);
			textField_3.setColumns(10);
			
			JButton button = new JButton("   查询",new ImageIcon(this.getClass().getResource("img/find.gif")));//查询
			button.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					String  cx=textField_3.getText();
					if (cx.equals("")) {
						String sql3="SELECT 房间信息表.roomid 房间号, 房间类型表.r_type,price,客户类型表.discount*10 折扣比列,price*discount 折后单价,price-(price*discount) ,`入住信息表`.in_time   from \r\n" + 
								"checkout 结算表,\r\n" + 
								"customerinfo 客户信息表,\r\n" + 
								"customertype 客户类型表,\r\n" + 
								"livein 入住信息表,\r\n" + 
								"p_state 客户状态表,\r\n" + 
								"r_state 房间状态表,\r\n" + 
								"roominfo 房间信息表,\r\n" + 
								"roomtype 房间类型表\r\n" + 
								"WHERE \r\n" + 
								" 入住信息表.status = 客户状态表.pnumber\r\n" + 
								"AND 入住信息表.roomid = 房间信息表.roomid\r\n" + 
								"AND 入住信息表.c_id = 客户信息表.c_id\r\n" + 
								"AND 客户信息表.c_type = 客户类型表.c_id\r\n" + 
								"AND 房间信息表.roomtype = 房间类型表.roomtype\r\n" + 
								"AND 房间信息表.state = 房间状态表.number";
						data2=QuerY.getSelect3(sql3);
						//创建表模型
						 dt2.setDataVector(data2,header2);
					} else {
						String sql3="SELECT 房间信息表.roomid 房间号, 房间类型表.r_type,price,客户类型表.discount*10 折扣比列,price*discount 折后单价,price-(price*discount) ,`入住信息表`.in_time   from \r\n" + 
								"checkout 结算表,\r\n" + 
								"customerinfo 客户信息表,\r\n" + 
								"customertype 客户类型表,\r\n" + 
								"livein 入住信息表,\r\n" + 
								"p_state 客户状态表,\r\n" + 
								"r_state 房间状态表,\r\n" + 
								"roominfo 房间信息表,\r\n" + 
								"roomtype 房间类型表\r\n" + 
								"WHERE \r\n" + 
								" 入住信息表.status = 客户状态表.pnumber\r\n" + 
								"AND 入住信息表.roomid = 房间信息表.roomid\r\n" + 
								"AND 入住信息表.c_id = 客户信息表.c_id\r\n" + 
								"AND 客户信息表.c_type = 客户类型表.c_id\r\n" + 
								"AND 房间信息表.roomtype = 房间类型表.roomtype\r\n" + 
								"AND 房间信息表.state = 房间状态表.number"+
								" AND 房间信息表.roomid='"+cx+"'";
						data2=QuerY.getSelect3(sql3);
						//创建表模型
						 dt2.setDataVector(data2,header2);
					}
					
					
				}
			});
			button.setBounds(444, 88, 100, 30);
//			button.setFocusPainted(false);//去掉按钮周围的焦点框
//			button.setContentAreaFilled(false);//设置按钮透明背景
			jsselect.add(button);
			
			JButton button_1 = new JButton("   刷新",new ImageIcon(this.getClass().getResource("img/b1.gif")));
			button_1.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
				
						String sql3="SELECT 房间信息表.roomid 房间号, 房间类型表.r_type,price,客户类型表.discount*10 折扣比列,price*discount 折后单价,price-(price*discount) ,`入住信息表`.in_time   from \r\n" + 
								"checkout 结算表,\r\n" + 
								"customerinfo 客户信息表,\r\n" + 
								"customertype 客户类型表,\r\n" + 
								"livein 入住信息表,\r\n" + 
								"p_state 客户状态表,\r\n" + 
								"r_state 房间状态表,\r\n" + 
								"roominfo 房间信息表,\r\n" + 
								"roomtype 房间类型表\r\n" + 
								"WHERE \r\n" + 
								" 入住信息表.status = 客户状态表.pnumber\r\n" + 
								"AND 入住信息表.roomid = 房间信息表.roomid\r\n" + 
								"AND 入住信息表.c_id = 客户信息表.c_id\r\n" + 
								"AND 客户信息表.c_type = 客户类型表.c_id\r\n" + 
								"AND 房间信息表.roomtype = 房间类型表.roomtype\r\n" + 
								"AND 房间信息表.state = 房间状态表.number"
							;
						data2=QuerY.getSelect3(sql3);
						//创建表模型
						 dt2.setDataVector(data2,header2);
				}
			});
//			button_1.setFocusPainted(false);//去掉按钮周围的焦点框
//			button_1.setContentAreaFilled(false);//设置按钮透明背景
			button_1.setBounds(580, 88, 100, 30);
			jsselect.add(button_1);
			
			JPanel panel_5_1_1_3 = new JPanel();
			panel_5_1_1_3.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
			panel_5_1_1_3.setBounds(0, 147, 867, 36);
			jsselect.add(panel_5_1_1_3);
			
			JLabel label_5_1_3 = new JLabel("\u7ED3\u8D26\u72B6\u6001\u4FE1\u606F");
			label_5_1_3.setFont(new Font("微软雅黑", Font.PLAIN, 15));
			panel_5_1_1_3.add(label_5_1_3);
//第三页表			
			
			String sql3="SELECT 房间信息表.roomid 房间号, 房间类型表.r_type,price,客户类型表.discount*10 折扣比列,price*discount 折后单价,price-(price*discount) ,`入住信息表`.in_time   from \r\n" + 
					"checkout 结算表,\r\n" + 
					"customerinfo 客户信息表,\r\n" + 
					"customertype 客户类型表,\r\n" + 
					"livein 入住信息表,\r\n" + 
					"p_state 客户状态表,\r\n" + 
					"r_state 房间状态表,\r\n" + 
					"roominfo 房间信息表,\r\n" + 
					"roomtype 房间类型表\r\n" + 
					"WHERE \r\n" + 
					" 入住信息表.status = 客户状态表.pnumber\r\n" + 
					"AND 入住信息表.roomid = 房间信息表.roomid\r\n" + 
					"AND 入住信息表.c_id = 客户信息表.c_id\r\n" + 
					"AND 客户信息表.c_type = 客户类型表.c_id\r\n" + 
					"AND 房间信息表.roomtype = 房间类型表.roomtype\r\n" + 
					"AND 房间信息表.state = 房间状态表.number";
			data2=QuerY.getSelect3(sql3);
			//创建表模型
			 dt2=new DefaultTableModel(data2,header2);
			
			jT2=new JTable(dt2);
			
			
			
			
			JScrollPane scrollPane = new JScrollPane(jT2);
			scrollPane.setBounds(14, 196, 839, 194);
			jsselect.add(scrollPane);
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
		
		JPanel lkselect = new JPanel();
//		lkselect.setForeground(Color.WHITE);
//		lkselect.setBackground(Color.decode("#E7D7B7"));
		tabbedPane.addTab("离店宾客消费查询", null, lkselect, null);
		lkselect.setLayout(null);
		
		JCheckBox chckbxNewCheckBox_1_1 = new JCheckBox("\u7ED3\u7B97\u65F6\u95F4\uFF1A");
		chckbxNewCheckBox_1_1.setFont(new Font("微软雅黑", Font.PLAIN, 15));
//		chckbxNewCheckBox_1_1.setBackground(new Color(231, 215, 183));
		chckbxNewCheckBox_1_1.setBounds(70, 14, 104, 27);
		lkselect.add(chckbxNewCheckBox_1_1);
		
		JLabel label_1_1 = new JLabel("\u8D77\u59CB\u65F6\u95F4");
		label_1_1.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		label_1_1.setBounds(182, 18, 75, 18);
		lkselect.add(label_1_1);
		
		textField_7 = new JTextField();
		textField_7.setColumns(10);
		textField_7.setBounds(271, 16, 179, 24);
		lkselect.add(textField_7);
		
		JLabel label_2_1 = new JLabel("\u7EC8\u6B62\u65F6\u95F4");
		label_2_1.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		label_2_1.setBounds(521, 18, 72, 18);
		lkselect.add(label_2_1);
		
		textField_8 = new JTextField();
		textField_8.setColumns(10);
		textField_8.setBounds(607, 16, 179, 24);
		lkselect.add(textField_8);
		
		JCheckBox js = new JCheckBox("\u67E5\u8BE2\u6761\u4EF6\uFF1A");
		js.setFont(new Font("微软雅黑", Font.PLAIN, 15));
//		js.setBackground(new Color(231, 215, 183));
		js.setBounds(70, 72, 104, 27);
		lkselect.add(js);
		
		JComboBox comboBox = new JComboBox();
//		comboBox.setBackground(Color.decode("#E7D7B7"));
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"按结账单号查询", "\u6309\u623F\u95F4\u53F7\u67E5\u8BE2", "\u6309\u623F\u95F4\u7C7B\u578B\u67E5\u8BE2", "\u6309\u5355\u4EF7\u67E5\u8BE2"}));
		comboBox.setBounds(178, 74, 142, 24);
		lkselect.add(comboBox);
		
		JLabel label_1_1_1 = new JLabel("\u5173\u952E\u5B57\uFF1A");
		label_1_1_1.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		label_1_1_1.setBounds(340, 77, 82, 18);
		lkselect.add(label_1_1_1);
		
		textField_9 = new JTextField();
		textField_9.setColumns(10);
		textField_9.setBounds(429, 75, 117, 24);
		lkselect.add(textField_9);
		
		JButton button_4 = new JButton("   \u67E5\u8BE2", new ImageIcon(this.getClass().getResource("img/find.gif")));
		button_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String csan=textField_9.getText();
						
				if (csan.equals("")) {
					String sql4="SELECT 客户类型表.c_id,房间信息表.roomid 房间号, c_name 宾客姓名,c_sex ,d_type,no,`入住信息表`.number,foregift 已收押金,入住信息表.days,`客户状态表`.state,入住信息表.in_time  ,结算表.chk_time,`结算表`.chk_no from \r\n" + 
							"checkout 结算表,\r\n" + 
							"customerinfo 客户信息表,\r\n" + 
							"customertype 客户类型表,\r\n" + 
							"livein 入住信息表,\r\n" + 
							"p_state 客户状态表,\r\n" + 
							"r_state 房间状态表,\r\n" + 
							"roominfo 房间信息表,\r\n" + 
							"roomtype 房间类型表\r\n" + 
							"WHERE \r\n" + 
							" 入住信息表.status = 客户状态表.pnumber\r\n" + 
							"AND 入住信息表.roomid = 房间信息表.roomid\r\n" + 
							"AND 入住信息表.c_id = 客户信息表.c_id\r\n" + 
							"AND 客户信息表.c_type = 客户类型表.c_id\r\n" + 
							"AND 客户类型表.c_id = 客户信息表.c_type\r\n" + 
							"AND 房间信息表.roomtype = 房间类型表.roomtype\r\n" + 
							"AND 房间信息表.state = 房间状态表.number\r\n" + 
							"and  `客户状态表`.state='已离店'";
					 data3=QuerY.getSelect4(sql4);
					//创建表模型
					 dt3.setDataVector(data3,header3);	
				} else {
					String sql4="SELECT 客户类型表.c_id,房间信息表.roomid 房间号, c_name 宾客姓名,c_sex ,d_type,no,`入住信息表`.number,foregift 已收押金,入住信息表.days,`客户状态表`.state,入住信息表.in_time  ,结算表.chk_time,`结算表`.chk_no from \r\n" + 
							"checkout 结算表,\r\n" + 
							"customerinfo 客户信息表,\r\n" + 
							"customertype 客户类型表,\r\n" + 
							"livein 入住信息表,\r\n" + 
							"p_state 客户状态表,\r\n" + 
							"r_state 房间状态表,\r\n" + 
							"roominfo 房间信息表,\r\n" + 
							"roomtype 房间类型表\r\n" + 
							"WHERE \r\n" + 
							" 入住信息表.status = 客户状态表.pnumber\r\n" + 
							"AND 入住信息表.roomid = 房间信息表.roomid\r\n" + 
							"AND 入住信息表.c_id = 客户信息表.c_id\r\n" + 
							"AND 客户信息表.c_type = 客户类型表.c_id\r\n" + 
							"AND 客户类型表.c_id = 客户信息表.c_type\r\n" + 
							"AND 房间信息表.roomtype = 房间类型表.roomtype\r\n" + 
							"AND 房间信息表.state = 房间状态表.number\r\n" + 
							"and  `客户状态表`.state='已离店'"+
							"AND (c_name='"+csan+" ' or "+"房间信息表.roomid='"+csan+"' or "+" `结算表`.chk_no='"+csan+"')";
					 data3=QuerY.getSelect4(sql4);
					//创建表模型
					 dt3.setDataVector(data3,header3);	
				}
			}
		});
//		button_4.setFocusPainted(false);
//		button_4.setContentAreaFilled(false);
		button_4.setBounds(566, 72, 100, 30);
		lkselect.add(button_4);
		
		JButton button_1_2 = new JButton("   \u5237\u65B0", new ImageIcon(this.getClass().getResource("img/b1.gif")));
		button_1_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String sql4="SELECT 客户类型表.c_id,房间信息表.roomid 房间号, c_name 宾客姓名,c_sex ,d_type,no,`入住信息表`.number,foregift 已收押金,入住信息表.days,`客户状态表`.state,入住信息表.in_time  ,结算表.chk_time,`结算表`.chk_no from \r\n" + 
						"checkout 结算表,\r\n" + 
						"customerinfo 客户信息表,\r\n" + 
						"customertype 客户类型表,\r\n" + 
						"livein 入住信息表,\r\n" + 
						"p_state 客户状态表,\r\n" + 
						"r_state 房间状态表,\r\n" + 
						"roominfo 房间信息表,\r\n" + 
						"roomtype 房间类型表\r\n" + 
						"WHERE \r\n" + 
						" 入住信息表.status = 客户状态表.pnumber\r\n" + 
						"AND 入住信息表.roomid = 房间信息表.roomid\r\n" + 
						"AND 入住信息表.c_id = 客户信息表.c_id\r\n" + 
						"AND 客户信息表.c_type = 客户类型表.c_id\r\n" + 
						"AND 客户类型表.c_id = 客户信息表.c_type\r\n" + 
						"AND 房间信息表.roomtype = 房间类型表.roomtype\r\n" + 
						"AND 房间信息表.state = 房间状态表.number\r\n" + 
						"and  `客户状态表`.state='已离店'";
				 data3=QuerY.getSelect4(sql4);
				//创建表模型
				 dt3.setDataVector(data3,header3);	
			}
		});
//		button_1_2.setFocusPainted(false);
//		button_1_2.setContentAreaFilled(false);
		button_1_2.setBounds(702, 72, 100, 30);
		lkselect.add(button_1_2);
		
		JPanel panel_5_1_1 = new JPanel();
		panel_5_1_1.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
//		panel_5_1_1.setBackground(new Color(199, 183, 143));
		panel_5_1_1.setBounds(0, 135, 867, 36);
		lkselect.add(panel_5_1_1);
		
		JLabel label_5_1 = new JLabel("\u79BB\u5E97\u5BBE\u5BA2\u6D88\u8D39");
		label_5_1.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		panel_5_1_1.add(label_5_1);
		
		
		
		
		
		
		
		
		//第四页数据表		
			
				String sql4="SELECT 客户类型表.c_id,房间信息表.roomid 房间号, c_name 宾客姓名,c_sex ,d_type,no,`入住信息表`.number,foregift 已收押金,入住信息表.days,`客户状态表`.state,入住信息表.in_time  ,结算表.chk_time,`结算表`.chk_no from \r\n" + 
						"checkout 结算表,\r\n" + 
						"customerinfo 客户信息表,\r\n" + 
						"customertype 客户类型表,\r\n" + 
						"livein 入住信息表,\r\n" + 
						"p_state 客户状态表,\r\n" + 
						"r_state 房间状态表,\r\n" + 
						"roominfo 房间信息表,\r\n" + 
						"roomtype 房间类型表\r\n" + 
						"WHERE \r\n" + 
						" 入住信息表.status = 客户状态表.pnumber\r\n" + 
						"AND 入住信息表.roomid = 房间信息表.roomid\r\n" + 
						"AND 入住信息表.c_id = 客户信息表.c_id\r\n" + 
						"AND 客户信息表.c_type = 客户类型表.c_id\r\n" + 
						"AND 客户类型表.c_id = 客户信息表.c_type\r\n" + 
						"AND 房间信息表.roomtype = 房间类型表.roomtype\r\n" + 
						"AND 房间信息表.state = 房间状态表.number\r\n" + 
						"and  `客户状态表`.state='已离店'";
				 data3=QuerY.getSelect4(sql4);
				//创建表模型
				 dt3=new DefaultTableModel(data3,header3);	
				 jT3=new JTable(dt3);
		
		JScrollPane scrollPane_1 = new JScrollPane(jT3);
		scrollPane_1.setBounds(14, 187, 839, 129);
		lkselect.add(scrollPane_1);
		

//		table_3.getColumnModel().getColumn(7).setPreferredWidth(80);
//		scrollPane_1.setViewportView(table_3);
//		tabbedPane.setBackgroundAt(3, Color.decode("#B8AD97"));

		
	}
	
}
