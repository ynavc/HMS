package com.yfhms.View;

import java.awt.BorderLayout;

import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.UnsupportedLookAndFeelException;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.yfhms.Controller.Select;

import javax.swing.border.SoftBevelBorder;
import javax.swing.border.BevelBorder;

public class ClientManagement extends JFrame {
	Select select = new Select();
	private JTextField textField;
	private JTable table;
	private JTextField textField_1;
	 Object[][] ob1;
	 Object[][] ob2;
	 DefaultTableModel dt;
	 DefaultTableModel dt2;
	private JTable table_1;
	public ClientManagement() {
		setBounds(100, 100, 900, 600);
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		this.setLocationRelativeTo(null);
		getContentPane().setLayout(null);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(tabbedPane, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("会员基本信息维护",  new ImageIcon(this.getClass().getResource("img/u02.gif")), panel, null);
		panel.setLayout(null);
		
		JButton button = new JButton("  增加");
		button.setIcon(new ImageIcon(this.getClass().getResource("img\\new.gif")));
		button.setBounds(35, 13, 100, 27);
		panel.add(button);
		
		JButton button_1 = new JButton("   修改");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		button_1.setIcon(new ImageIcon(this.getClass().getResource("img\\modi0.gif")));
		button_1.setBounds(149, 13, 100, 27);
		panel.add(button_1);
		
		JButton button_2 = new JButton("  删除");
		button_2.setIcon(new ImageIcon(this.getClass().getResource("img\\del.gif")));
		button_2.setBounds(263, 13, 100, 27);
		panel.add(button_2);
		
		JLabel label = new JLabel("\u4F1A\u5458\u7F16\u53F7/\u59D3\u540D\uFF1A");
		label.setBounds(377, 17, 125, 18);
		panel.add(label);
		
		textField = new JTextField("");
		textField.setBounds(502, 14, 86, 24);
		panel.add(textField);
		textField.setColumns(10);
		
		JButton button_2_1 = new JButton("查询",   new ImageIcon(this.getClass().getResource("img/find.gif")));
		button_2_1.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		button_2_1.setBounds(602, 13, 113, 27);
		panel.add(button_2_1);
		
		
		JButton btnNewButton = new JButton("\u5237\u65B0",  new ImageIcon(this.getClass().getResource("img/b1.gif")));
		btnNewButton.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		btnNewButton.setFocusPainted(false);//去掉按钮周围的焦点框
		btnNewButton.setContentAreaFilled(false);//设置按钮透明背景

		btnNewButton.setBounds(736, 13, 113, 27);
		panel.add(btnNewButton);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(14, 128, 835, 290);
		panel.add(scrollPane);
		
		String[] heads= {"客户编号", "客户名字", "性别", "身份证号", "联系电话","详细地址"};
		Object[][] data= {
	
				
		};
	    
	   String sql="SELECT c_id,c_name,c_sex,no,v_phone,address FROM customerinfo WHERE c_type='3';";
	   ob1=select.getcustinfo(sql);
	      dt=new DefaultTableModel(ob1,heads);	
	   
         button_2_1.addActionListener(new ActionListener() {
        	 
			@Override
			public void actionPerformed(ActionEvent e) {
				String a=textField.getText();
				String sql2=null;
				if (a.equals("")) {
					sql2="SELECT c_id,c_name,c_sex,no,v_phone,address FROM customerinfo WHERE c_type='3' \"";
					ob1=select.getcustinfo(sql2);
					dt.setDataVector(ob1, heads);
				} else {
					sql2="SELECT c_id,c_name,c_sex,no,v_phone,address FROM customerinfo  WHERE c_name='"+a+"' ";
					System.out.println("shi"+a);
					ob1=select.getcustinfo(sql2);
					dt.setDataVector(ob1, heads);
					 
				} 

			
			
				
			
			}
		});
	    JTable jTable = new JTable(dt);
		int v=ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED;
		int h=ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED;
	    JScrollPane jsp=new JScrollPane(jTable,v,h);
	    jsp.setBounds(0, 0, 350, 280);
	 
		scrollPane.setViewportView(jTable);
		
		JPanel panel_5_1_1_1 = new JPanel();
		panel_5_1_1_1.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));

		panel_5_1_1_1.setBounds(0, 73, 867, 36);
		panel.add(panel_5_1_1_1);
		
		JLabel label_5_1_1 = new JLabel("\u4F1A\u5458\u4FE1\u606F");
		label_5_1_1.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		panel_5_1_1_1.add(label_5_1_1);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("来宾信息一览表",   new ImageIcon(this.getClass().getResource("img/u03.gif")), panel_1, null);
		panel_1.setLayout(null);
		
		JLabel label_1 = new JLabel("\u6765\u5BBE\u59D3\u540D/\u8BC1\u4EF6\u7F16\u53F7");
		label_1.setBounds(179, 17, 144, 18);
		panel_1.add(label_1);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(323, 14, 113, 24);
		panel_1.add(textField_1);
		
		JButton button_2_1_1 = new JButton("  查询",    new ImageIcon(this.getClass().getResource("img/find.gif")));
		
		
		
		button_2_1_1.setFont(new Font("微软雅黑", Font.PLAIN, 15));
//		button_2_1_1.setFocusPainted(false);//去掉按钮周围的焦点框
//		button_2_1_1.setContentAreaFilled(false);//设置按钮透明背景

		button_2_1_1.setBounds(479, 13, 113, 27);
		panel_1.add(button_2_1_1);
		
		JButton btnNewButton_1 = new JButton("   刷新",    new ImageIcon(this.getClass().getResource("img/b1.gif")));
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton_1.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		btnNewButton_1.setBounds(613, 13, 113, 27);
		panel_1.add(btnNewButton_1);
		
		JPanel panel_5_1_1 = new JPanel();
		panel_5_1_1.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		panel_5_1_1.setBounds(0, 65, 867, 36);
		panel_1.add(panel_5_1_1);
		
		JLabel label_5_1 = new JLabel("来宾信息");
		label_5_1.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		panel_5_1_1.add(label_5_1);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(14, 140, 839, 227);
		panel_1.add(scrollPane_1);
		
		String[] heads2= {"客户编号", "客户名字", "性别", "身份证号", "联系电话","详细地址"};
		Object[][] data2= {
	
				
		};
		
		String sqlx="SELECT c_id,c_name,c_sex,no,v_phone,address FROM customerinfo" ;
		 ob2=select.getcustinfo2(sqlx);
		 dt2=new DefaultTableModel(ob2,heads2);
	
	    button_2_1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				String b=textField_1.getText();
				String sql3=null;
				if (b.equals("")) {
					sql3="SELECT c_id,c_name,c_sex,no,v_phone,address FROM customerinfo";
					ob2=select.getcustinfo2(sql3);
					dt2.setDataVector(ob2, heads2);
				} else {
					sql3="SELECT c_id,c_name,c_sex,no,v_phone,address FROM customerinfo  WHERE c_name='"+b+"' ";
					System.out.println("shi"+b);
					ob2=select.getcustinfo2(sql3);
					dt2.setDataVector(ob2, heads2);
					 
				} 
			}
		});
		
	   
		
	    JTable jTable2 = new JTable(dt2);
		int v2=ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED;
		int h2=ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED;
	    JScrollPane jsp2=new JScrollPane(jTable2,v2,h2);
	    jsp.setBounds(0, 0, 350, 280);
	 
		scrollPane_1.setViewportView(jTable2);
		
		JCheckBox chckbxNewCheckBox = new JCheckBox("\u7ED3\u8D26\u65F6\u95F4\uFF1A");
//		chckbxNewCheckBox.setVerticalAlignment(SwingConstants.BOTTOM);
		chckbxNewCheckBox.setBounds(20, 100, 300, 50);
		chckbxNewCheckBox.setContentAreaFilled(false);

	}
	
	public static void main(String[] args) {
		try {
			javax.swing.UIManager.setLookAndFeel("com.jtattoo.plaf.smart.SmartLookAndFeel");
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}

		ClientManagement frame = new ClientManagement();
		frame.setVisible(true);
	}
}
