package com.yfhms.View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import java.awt.Font;
import java.awt.Image;

import javax.swing.border.LineBorder;
import java.awt.SystemColor;
import javax.swing.UIManager;
import java.awt.Component;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.FlowLayout;
import javax.swing.JTextField;
import javax.swing.JCheckBox;

public class GroupOrdersAdd  extends JFrame{
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	public GroupOrdersAdd() {
		super("团体开单");
		this.setBounds(0, 0, 770, 750);
		this.setLocationRelativeTo(null);//让窗口在屏幕中间显示
		this.setResizable(false);//让窗口大小不可改变
		getContentPane().setLayout(null);
//		this.getContentPane().setBackground(Color.decode("#E7D7B7"));//设置背景颜色
		
		JPanel panel = new JPanel();
//		panel.setBackground(Color.decode("#E7D7B7"));
		panel.setBounds(0, 0, 372, 331);
		getContentPane().add(panel);
		panel.setLayout(null);
		
		JPanel panel_2 = new JPanel();
//		panel_2.setBackground(new Color(199, 183, 143));
		panel_2.setBounds(0, 0, 372, 33);
		panel.add(panel_2);
		panel_2.setLayout(null);
		
		JLabel label_2 = new JLabel("房间信息");
		label_2.setBounds(140, 0, 64, 31);
		label_2.setFont(new Font("宋体", Font.BOLD, 15));
		panel_2.add(label_2);
		
		JLabel label = new JLabel("房间类型：");
		label.setBounds(79, 53, 75, 18);
		panel.add(label);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(159, 50, 104, 24);
		panel.add(comboBox);
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"标准单人间", "标准双人间", "豪华单人间", "豪华双人间", "商务套房", "总统套房"}));
		
		String[] columnNames = {"房间类型","标准单价"};
		Object[][] data = {{"BD1001","781.0"},{"BD1001","781.0"}};
		DefaultTableModel df = new DefaultTableModel(data, columnNames);
		JTable table = new JTable(df);
		DefaultTableCellRenderer render = new DefaultTableCellRenderer();//建立一个renderer对象
		render.setHorizontalAlignment(SwingConstants.CENTER);//设置表数据居中显示
		table.getTableHeader().getColumnModel().getColumn(0).setCellRenderer(render);//将表格设定为此方式的renderer
		table.setSurrendersFocusOnKeystroke(true);
		table.setBounds(1, 1, 410, 327);
		table.getTableHeader().setFont(new Font(null, Font.CENTER_BASELINE, 14));  // 设置表头名称字体样式
	    table.getTableHeader().setForeground(Color.black);                // 设置表头名称字体颜色
	    table.getTableHeader().setResizingAllowed(false);               // 设置不允许手动改变列宽
	    table.getTableHeader().setReorderingAllowed(false);             // 设置不允许拖动重新排序各列
		int v=ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED;//水平滚动条
		int h=ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED;//垂直滚动条
		JScrollPane scr = new JScrollPane(table,v,h);
		table.setPreferredSize(new Dimension(200,200));//设置滚动条的长度
		scr.setBounds(14, 87, 336, 195);
		panel.add(scr);
		
		JButton button = new JButton("添加到开单区");
//		button.setContentAreaFilled(false);//设置按钮透明背景
//		button.setFocusPainted(false);//去掉按钮周围的焦点框
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		button.setBounds(106, 290, 123, 33);
		panel.add(button);
		
		JPanel panel_1 = new JPanel();
		panel_1.setLayout(null);
		panel_1.setBorder(new LineBorder(new Color(0, 0, 0)));
//		panel_1.setBackground(new Color(231, 215, 183));
		panel_1.setBounds(392, 0, 372, 331);
		getContentPane().add(panel_1);
		
		JPanel panel_2_1 = new JPanel();
		panel_2_1.setBorder(new LineBorder(new Color(0, 0, 0)));
//		panel_2_1.setBackground(new Color(199, 183, 143));
		panel_2_1.setBounds(0, 0, 372, 33);
		panel_1.add(panel_2_1);
		panel_2_1.setLayout(null);
		
		JLabel label_2_1 = new JLabel("开单房间");
		label_2_1.setFont(new Font("宋体", Font.BOLD, 15));
		label_2_1.setBounds(144, 0, 64, 31);
		panel_2_1.add(label_2_1);
		
		String[] columnNames2 = {"房间类型","标准单价"};
		Object[][] data2 = {{"BD1001","781.0"},{"BD1001","781.0"}};
		DefaultTableModel df2 = new DefaultTableModel(data2, columnNames2);
		JTable table2 = new JTable(df2);
		table2.getTableHeader().getColumnModel().getColumn(0).setCellRenderer(render);//将表格设定为此方式的renderer
		table2.setSurrendersFocusOnKeystroke(true);
		table2.setBounds(100, 1100, 410, 327);
		table2.getTableHeader().setFont(new Font(null, Font.CENTER_BASELINE, 14));// 设置表头名称字体样式
	    table2.getTableHeader().setForeground(Color.black);// 设置表头名称字体颜色
	    table2.getTableHeader().setResizingAllowed(false); // 设置不允许手动改变列宽
	    table2.getTableHeader().setReorderingAllowed(false);// 设置不允许拖动重新排序各列
		JScrollPane scr2 = new JScrollPane(table2,v,h);
		scr2.setBounds(14, 32, 336, 195);
		panel_1.add(scr2);
		
		JButton button_1 = new JButton("从开单区删除");
//		button_1.setFocusPainted(false);//去掉按钮周围的焦点框
//		button_1.setContentAreaFilled(false);//设置按钮透明背景
		button_1.setBounds(99, 290, 123, 33);
		panel_1.add(button_1);
		
		JPanel panel_3 = new JPanel();
//		panel_3.setBackground(Color.decode("#E7D7B7"));
		panel_3.setBounds(-13, 355, 787, 304);
		getContentPane().add(panel_3);
		panel_3.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("证件类型：");
		lblNewLabel.setBounds(57, 26, 92, 18);
		panel_3.add(lblNewLabel);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.addItem("身份证");
		comboBox_1.addItem("居住证");
		comboBox_1.addItem("签证");
		comboBox_1.addItem("护照");
		comboBox_1.addItem("户口本");
		comboBox_1.addItem("军人证");
		comboBox_1.setBounds(139, 23, 92, 24);
		panel_3.add(comboBox_1);
		
		JLabel lblNewLabel_1 = new JLabel("宾客类型：");
		lblNewLabel_1.setBounds(292, 26, 92, 18);
		panel_3.add(lblNewLabel_1);
		
		JComboBox comboBox_1_1 = new JComboBox();
		comboBox_1_1.addItem("普通宾客");
		comboBox_1_1.addItem("团体宾客");
		comboBox_1_1.addItem("会员宾客");
		comboBox_1_1.addItem("协议单位");
		comboBox_1_1.setBounds(374, 23, 92, 24);
		panel_3.add(comboBox_1_1);
		
		JLabel lblNewLabel_1_1 = new JLabel("主客姓名：");
		lblNewLabel_1_1.setBounds(526, 26, 92, 18);
		panel_3.add(lblNewLabel_1_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(608, 64, 92, 24);
		panel_3.add(textField_2);
		
		JLabel lblNewLabel_2 = new JLabel("证件编码：");
		lblNewLabel_2.setBounds(57, 67, 92, 18);
		panel_3.add(lblNewLabel_2);
		
		textField = new JTextField();
		textField.setBounds(139, 64, 139, 24);
		panel_3.add(textField);
		textField.setColumns(10);
		
		JLabel lblNewLabel_1_2 = new JLabel("主客姓名：");
		lblNewLabel_1_2.setBounds(292, 67, 92, 18);
		panel_3.add(lblNewLabel_1_2);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(374, 64, 113, 24);
		panel_3.add(textField_1);
		
		JComboBox comboBox_1_1_2 = new JComboBox();
		comboBox_1_1_2.setEditable(true);
		comboBox_1_1_2.setBounds(374, 64, 92, 24);
		panel_3.add(comboBox_1_1_2);
		
		JLabel lblNewLabel_1_1_1 = new JLabel("宾客人数：");
		lblNewLabel_1_1_1.setBounds(526, 67, 92, 18);
		panel_3.add(lblNewLabel_1_1_1);
		
		JComboBox comboBox_1_1_1 = new JComboBox();
		comboBox_1_1_1.setModel(new DefaultComboBoxModel(new String[] {"男", "女"}));
		comboBox_1_1_1.setBounds(608, 23, 92, 24);
		panel_3.add(comboBox_1_1_1);
		
		JLabel lblNewLabel_2_1 = new JLabel("实收押金");
		lblNewLabel_2_1.setBounds(57, 109, 92, 18);
		panel_3.add(lblNewLabel_2_1);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(139, 106, 92, 24);
		panel_3.add(textField_3);
		
		JLabel lblNewLabel_1_2_1 = new JLabel("主客房间：");
		lblNewLabel_1_2_1.setBounds(292, 109, 92, 18);
		panel_3.add(lblNewLabel_1_2_1);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(374, 106, 92, 24);
		panel_3.add(textField_4);
		
		JLabel lblNewLabel_1_1_1_1 = new JLabel("预留天数：");
		lblNewLabel_1_1_1_1.setBounds(526, 109, 92, 18);
		panel_3.add(lblNewLabel_1_1_1_1);
		
		textField_5 = new JTextField();
		textField_5.setColumns(10);
		textField_5.setBounds(608, 106, 92, 24);
		panel_3.add(textField_5);
		
		JLabel label_1 = new JLabel("地址信息：");
		label_1.setBounds(57, 165, 86, 18);
		panel_3.add(label_1);
		
		textField_6 = new JTextField();
		textField_6.setBounds(139, 162, 561, 24);
		panel_3.add(textField_6);
		textField_6.setColumns(10);
		
		textField_7 = new JTextField();
		textField_7.setColumns(10);
		textField_7.setBounds(139, 199, 561, 24);
		panel_3.add(textField_7);
		
		JLabel label_1_1 = new JLabel("备注信息：");
		label_1_1.setBounds(57, 202, 86, 18);
		panel_3.add(label_1_1);
		
		JCheckBox checkBox = new JCheckBox("到预住天数时自动提醒");
//		checkBox.setBackground(Color.decode("#E7D7B7"));
		checkBox.setSelected(true);
		checkBox.setBounds(57, 252, 206, 27);
		panel_3.add(checkBox);
		
		Font f = new Font("微软雅黑", Font.BOLD, 18);
		
		ImageIcon i1 = new ImageIcon(this.getClass().getResource("img/modi3.gif"));
//		i1.setImage(i1.getImage().getScaledInstance(30,30,Image.SCALE_DEFAULT));
		JButton qd = new JButton("   确定",i1);
		qd.setFont(f);
		qd.setBounds(210, 672, 130, 38);
//		qd.setContentAreaFilled(false);
//		qd.setFocusPainted(false);
		getContentPane().add(qd);
		
		ImageIcon i2 = new ImageIcon(this.getClass().getResource("img/cancel.gif"));
//		i2.setImage(i2.getImage().getScaledInstance(30,30,Image.SCALE_DEFAULT));
		JButton qx = new JButton("   取消",i2);
		qx.setFont(f);
		qx.setBounds(392, 672, 130, 38);
//		qx.setContentAreaFilled(false);//设置按钮透明背景
//		qx.setFocusPainted(false);
		getContentPane().add(qx);
		qx.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
		
		
	}
}
