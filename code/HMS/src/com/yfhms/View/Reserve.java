package com.yfhms.View;
//修改预订信息
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import com.yfhms.Controller.Select;
import com.yfhms.Controller.Updata;

public class Reserve extends  JFrame{
	Updata up=new Updata();
	Select select=new Select();

	public Reserve() {
		Font fBold = new Font("微软雅黑",Font.PLAIN,15);//设置字体
		Font fBold1 = new Font("微软雅黑",Font.PLAIN,18);//设置字体
		
		this.setLayout(null);
		this.setTitle("客房预订");//标题
		this.setBounds(0, 0, 500, 600);
		this.setLocationRelativeTo(null);//让窗口在屏幕中间显示
		this.setResizable(false);//让窗口大小不可改变
		//this.getContentPane().setBackground(Color.decode("#E7D7B7"));//设置背景颜色
		
		
		JLabel lablex=new JLabel("基本预订信息");
		lablex.setBounds(195, 0, 500, 30);
		lablex.setFont(fBold);
		this.add(lablex);
		
		JLabel lable1=new JLabel("宾客姓名：");
		lable1.setBounds(100, 40, 100, 30);
		lable1.setFont(fBold1);
		JTextField text1 = new JTextField();
		text1.setBounds(190,40, 150,30);
		this.add(lable1);
		this.add(text1);
		
		
		JLabel lable2=new JLabel("联系电话：");
		lable2.setBounds(100, 90, 100, 30);
		lable2.setFont(fBold1);
		JTextField text2 = new JTextField();
		text2.setBounds(190,90, 150,30);
		this.add(lable2);
		this.add(text2);
		
		JLabel lable3=new JLabel("预订规格：");
		lable3.setBounds(100, 140, 100, 30);
		lable3.setFont(fBold1);
		JComboBox jc=new JComboBox();
		jc.setBounds(190,140, 150,30);
		jc.setFont(fBold);

		this.add(lable3);
		this.add(jc);
		
		
		JLabel lable4=new JLabel("房间编号：");
		lable4.setBounds(100, 190, 100, 30);
		lable4.setFont(fBold1);
		JComboBox jc2=new JComboBox();
		jc2.setBounds(190,190, 150,30);
		jc2.setFont(fBold);

		this.add(jc2);
		this.add(lable4);
		
		JLabel lable5=new JLabel("预抵时间：");
		lable5.setBounds(100, 240, 100, 30);
		lable5.setFont(fBold1);
		JTextField text5 = new JTextField();
		text5.setBounds(190,240, 150,30);
		this.add(lable5);
		this.add(text5);
		
		JLabel lable6=new JLabel("保留时间：");
		lable6.setBounds(100, 290, 100, 30);
		lable6.setFont(fBold1);
		JTextField text6 = new JTextField();
		text6.setBounds(190,290, 150,30);
		this.add(lable6);
		this.add(text6);
		
		
		JLabel lable7=new JLabel("备注:");
	    lable7.setBounds(90, 350, 40, 40);
	    lable7.setFont(fBold1);
	    JTextField text7 = new JTextField();
	    text7.setBounds(160, 350, 200, 40);
	    JCheckBox jk=new JCheckBox("到达时间是否自动取消预订");
	    jk.setBounds(150, 415, 200,40);
	    //Sjk.setBackground(Color.decode("#E7D7B7"));
	    this.add(lable7);
	    this.add(text7);
	    this.add(jk);
	    
	    ImageIcon img=new ImageIcon(this.getClass().getResource("img/save.gif"));
		JButton jb2=new JButton("保存",img);
	    jb2.setBounds(115, 480, 90, 40);
	    this.add(jb2);
	   
	    ImageIcon img2=new ImageIcon(this.getClass().getResource("img/cancel.gif"));
	    JButton jb3=new JButton("取消",img2);
	    jb3.setBounds(265, 480, 90, 40);
	    this.add(jb3);
	    jb3.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
//				Custom_type cust=new Custom_type();
//				cust.setVisible(true);						
				dispose();
			}
		});
	    String sql1="SELECT r_type FROM  roomtype";
	    Object[][] object=select.getReserveadd(sql1);
	    String sql2="SELECT roomid FROM roominfo";
	    Object[][]  object2=select.getReserveadd(sql2);
	  
	    for (int i = 0; i < object.length; i++) {
	    	System.out.println(object[i][0]);
	    	jc.addItem(object[i][0]);
	    }
        	    
	    for (int i = 0; i < object2.length; i++) {
	    	System.out.println(object2[i][0]);
	    	jc2.addItem(object2[i][0]);		
		}

	    
	    jb2.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {

				String r_name=text1.getText();
				String r_phone=text2.getText();
				String Pa_time=text5.getText();
				String Keep_time=text6.getText();
				String Remark=text7.getText();
				String roomtypesql = "SELECT roomtype FROM roomtype WHERE r_type='"+jc.getSelectedItem()+"'";
				String roomtype = select.getroomtype(roomtypesql);
				System.out.println(roomtype);
				// TODO Auto-generated method stub
			    int choose = JOptionPane.showConfirmDialog(null,"您确定要添加预订信息吗？",	"提示",0,1);
				if(choose == JOptionPane.OK_OPTION){
					String sql="INSERT reserve VALUES(null,'"+r_name+"','"+roomtype+"','"+r_phone+"','"+jc2.getSelectedItem()+"','"+Pa_time+"','"+Keep_time+"','"+Remark+"')";
					int result1=up.addData(sql);
					if (result1>0) {
						String updateRoomintoSQL = "UPDATE roominfo set state=2 WHERE roomid='"+jc2.getSelectedItem()+"'";
						int reselt2 = up.addData(updateRoomintoSQL);
						if(reselt2>0){
							JOptionPane.showMessageDialog(null, "添加预订信息成功！");
						}else {
							JOptionPane.showMessageDialog(null, "添加预订信息失败！");
						}
					} else {
						JOptionPane.showMessageDialog(null, "添加预订信息失败！");
					}
                }
			}
		});
	
	}
}
