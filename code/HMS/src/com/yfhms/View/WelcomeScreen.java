package com.yfhms.View;

import java.awt.Cursor;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public  class WelcomeScreen extends JFrame{
	public WelcomeScreen() {
	    super.setTitle("系统登录");
		this.setBounds(0, 0, 700, 550);//设置大小
		this.setLocationRelativeTo(null);//让窗口在屏幕中间显示
 	    this.setResizable(false);//让窗口大小不可改变
		this.setLayout(null);
		this.setUndecorated(true);//隐藏边框和控件
        //添加背景图片
		ImageIcon icon=new ImageIcon(this.getClass().getResource("img/欢迎界面.jpg"));
		JLabel label=new JLabel(icon);//将图片放入label中
		label.setBounds(0,0,700,550);//设置label的大小
		this.add(label);
		this.setVisible(true);//窗口显示
		//设置窗口显示时间
		setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));//设置启动bai界面的光标样式
		
	}
	
}
