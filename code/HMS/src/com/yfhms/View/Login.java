package com.yfhms.View;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.SwingConstants;

import com.jtattoo.plaf.aluminium.AluminiumBorderFactory;
import com.yfhms.Bean.UserInfo;
import com.yfhms.DAO.DbConnection;
import com.yfhms.Controller.Select;
import com.yfhms.Controller.Updata;

import javax.swing.JSeparator;

public class Login extends JFrame {
	static String user;
	Select select = new Select();
	Updata updata = new Updata();
	JComboBox box;
 public  Login() {
	    super.setTitle("系统登录");
		this.setBounds(0, 0, 700, 550);//设置大小
		this.setLocationRelativeTo(null);//让窗口在屏幕中间显示
  	    this.setResizable(false);//让窗口大小不可改变
		getContentPane().setLayout(null);
		ImageIcon icon=new ImageIcon(this.getClass().getResource("img/艺蜂管理系统.jpg"));//加载图片
		
//	Image im=new Image(icon);
	JLabel label=new JLabel(icon);//将图片放入label中
	label.setBounds(0,0,700,150);//设置label的大小
	label.setBounds(0,0,icon.getIconWidth(),icon.getIconHeight());//设置label的大小
	this.getLayeredPane().add(label,new Integer(Integer.MIN_VALUE));//获取窗口的第二层，将label放入
	JPanel j=(JPanel)this.getContentPane();//获取frame的顶层容器,并设置为透明
	j.setOpaque(false);
	
	//
	JLabel uSerJLabel=new JLabel("用 户 名:");
	uSerJLabel.setBounds(70, 200, 100, 40);
	uSerJLabel.setFont(new Font("微软雅黑",Font.PLAIN, 20));
    getContentPane().add(uSerJLabel);
    //
	JLabel pwdJLabel=new JLabel("登录密码：");
	pwdJLabel.setBounds(70, 280, 100, 40);
	pwdJLabel.setFont(new Font("微软雅黑",Font.PLAIN, 20));
    getContentPane().add(pwdJLabel);
	
  //多选框
    
    box =new JComboBox();
	box.setBounds(170, 200, 400, 40);
	String[] userId = select.getUserID();
	for (int i = 0; i < userId.length; i++) {
		box.addItem(userId[i]);
	}
	
	box.setFont(new Font("宋体", Font.BOLD, 18));
	getContentPane().add(box);
	
	//密码框
	//密码输入框
    JPasswordField passwordField=new JPasswordField();
    passwordField.setBounds(170,280, 400, 40);
    getContentPane().add(passwordField);
	
    
  //创建图片容器并赋予图片路径
       ImageIcon icon1 = new ImageIcon(this.getClass().getResource("img/登录/key.gif"));
         //创建按钮
       	JButton button = new JButton("     登录",icon1);
       	//设置图片大小
       	button.setBounds(180, 350, 120, 40);
       	button.setFocusPainted(false);//去掉按钮周围的焦点框
//    	button.setContentAreaFilled(false);//设置按钮透明背景
 
        getContentPane().add(button);

    
        //创建图片容器并赋予图片路径
        ImageIcon icon2 = new ImageIcon(this.getClass().getResource("img/登录/exit.gif"));
          //创建按钮
        JButton button2 = new JButton("    退出",icon2);
        	//设置图片大小
        button2.setBounds(380, 350, 120, 40);
        button2.setFocusPainted(false);//去掉按钮周围的焦点框
//     	button2.setContentAreaFilled(false);//设置按钮透明背景
         getContentPane().add(button2);
         
         JSeparator separator = new JSeparator();
         separator.setBounds(0, 428, 694, 16);
         getContentPane().add(separator);
         
         JSeparator separator_1 = new JSeparator();
         separator_1.setBounds(0, 457, 700, 2);
         getContentPane().add(separator_1);
         
         if (passwordField.getText().equals("")) {
        	    JButton tisi =new JButton("提示：请输入登录密码...");        
        	    tisi.setBounds(30, 460, 300, 50);
        	    tisi.setContentAreaFilled(false);//设置按钮透明背景
        	    tisi.setFocusPainted(false);//去掉按钮周围的焦点框
        	    tisi.setBorderPainted(false);//去边框
        	    getContentPane().add(tisi);
		}
         
         //退出系统监听事件
         button2.addActionListener(new ActionListener() {
 			@Override
 			public void actionPerformed(ActionEvent e) {
 				int result = JOptionPane.showConfirmDialog(null,"您现在要关闭系统吗?","退出提示",0,1);
 				if(result == JOptionPane.OK_OPTION){
 					JOptionPane.showMessageDialog(null, "已退出系统，欢迎下次使用！");
                     System.exit(0);
                 }
 			}
 		});
         
         //登录监听事件
         button.addActionListener(new ActionListener() {
     		@Override
     		public void actionPerformed(ActionEvent e) {
     			
     			String account=(String) box.getSelectedItem();
     			
				String password=passwordField.getText();
				
				int a=select.Select(account ,password);
				if (a==0) {
					JOptionPane.showMessageDialog(null, "登录失败");
					user = "1";
				} else {
					user = select.getString("SELECT id FROM userinfo WHERE account='"+box.getSelectedItem()+"'");
					int i = updata.addData("UPDATE login_status SET state='已登录' WHERE s_id=1;");
					JOptionPane.showMessageDialog(null, "登录成功");
					//跳转到另外的界面
					MainJFrame admf=new MainJFrame();
					admf.setVisible(true);
					dispose();//销毁
				}
				
			}
		});
	}	
	public String getUser() {
		user = select.getString("SELECT id FROM userinfo WHERE account='"+box.getSelectedItem()+"'");
		return user;
	}
}
     
// public static void main(String[] args) {
//     Login login =new Login();
//     login.setVisible(true);
//	}
//}
