package com.yfhms.View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;

import javax.script.ScriptContext;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JSplitPane;
import javax.swing.JInternalFrame;
import javax.swing.JList;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.SystemColor;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.JComboBox;
import javax.swing.JToggleButton;
import javax.swing.JSeparator;
import javax.swing.JTextPane;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionEvent;
import java.awt.Component;
import javax.swing.JTree;
import javax.swing.DefaultComboBoxModel;
import javax.swing.SwingConstants;
import javax.swing.JScrollBar;
import javax.swing.tree.DefaultTreeModel;

import com.yfhms.Bean.Customerinfo;
import com.yfhms.Bean.Customertype;
import com.yfhms.Bean.RoomInfo;
import com.yfhms.Bean.RoomType;
import com.yfhms.Bean.UserInfo;
import com.yfhms.Controller.CustomerTable;
import com.yfhms.Controller.RoomSelect;
import com.yfhms.Controller.Updata;
import com.yfhms.Controller.UserSelect;

import javax.swing.tree.DefaultMutableTreeNode;

public class Operator extends JFrame {
	private JTextField textField;
	private JPasswordField passwordField;
	private JPasswordField passwordField_1;
	private JPasswordField passwordField_2;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField textField_8;
	private JTable table_2;
	private JTable table_4;
	private JTable table;
	private JTable table_1;
	private JTable table_3;
	RoomType roomType = new RoomType();
	Customertype customertype=new Customertype();
	UserInfo userinfo=new UserInfo();
	RoomInfo roomInfo=new RoomInfo();
	Updata up=new Updata();
	
	String rt;//房间类型
	String rs;//房间状态
	
	public Operator() {
		super.setTitle("系统设置");
		this.setBounds(0, 0, 900, 605);//设置大小
		this.setLocationRelativeTo(null);//让窗口在屏幕中间显示
 	    this.setResizable(true);//让窗口大小不可改变
		getContentPane().setLayout(null);
				
		//创建选项卡面板
		JTabbedPane tabbedPane=new JTabbedPane();
		tabbedPane.setBounds(0, 0, 880,550);
		getContentPane().add(tabbedPane);
//		tabbedPane.setBackground(Color.decode("#DBC9ED"));
        
        
		//房间项目面板
		JPanel p1 = new JPanel() ;  //创建面板组件容器
//		p1.setBackground(new Color(231, 215, 183));
		tabbedPane.addTab("房间项目设置",new ImageIcon(this.getClass().getResource("img\\设置界面图标\\开头4个\\u01.gif")), p1);//添加按钮
		
		//创建下拉框
        JComboBox cmb=new JComboBox();    //创建JComboBox
        cmb.setBounds(231, 7, 89, 24);
        cmb.setFont(new Font("微软雅黑", Font.PLAIN, 15));
        cmb.addItem("不可使用");    //向下拉列表中添加一项
        cmb.addItem("可供使用");
        cmb.addItem("维修中");
        cmb.addItem("已被预定");
        cmb.setVisible(true);
        p1.setLayout(null);
        p1.add(cmb);
        
        JLabel label_24 = new JLabel("\u7ED3\u8D26\u540E\u623F\u95F4\u72B6\u6001\u53D8\u4E3A\uFF1A");
        label_24.setBounds(61, 8, 171, 18);
        label_24.setFont(new Font("微软雅黑", Font.PLAIN, 17));
        p1.add(label_24);
        
        JLabel label_25 = new JLabel("\u7ED3\u8D26\u540E");
        label_25.setBounds(394, 10, 72, 18);
        label_25.setFont(new Font("微软雅黑", Font.PLAIN, 17));
        p1.add(label_25);
        
        textField_8 = new JTextField();
        textField_8.setBounds(450, 7, 48, 24);
        p1.add(textField_8);
        textField_8.setColumns(10);
        
        JLabel label_27 = new JLabel("\u5206\u949F\u540E\u53D8\u4E3A\u53EF\u4F7F\u7528\u72B6\u6001");
        label_27.setBounds(499, 10, 179, 18);
        label_27.setFont(new Font("微软雅黑", Font.PLAIN, 17));
        p1.add(label_27);
        
        ImageIcon roomIcon=new ImageIcon(this.getClass().getResource("img\\保存.gif"));
        JButton button_7 = new JButton("  保存",roomIcon);
        button_7.setBounds(704, 6, 113, 27);
//        button_7.setBackground(new Color(231, 215, 183));
        button_7.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        	}
        });
        button_7.setFont(new Font("微软雅黑", Font.PLAIN, 17));
        p1.add(button_7);
        
        JLabel label_28 = new JLabel("\u623F\u95F4\u7C7B\u578B");
        label_28.setBounds(14, 39, 72, 18);
        p1.add(label_28);
        
        JButton button_8 = new JButton("\u6DFB\u52A0\u7C7B\u578B");
        button_8.setBounds(133, 234, 133, 27);
        button_8.setFont(new Font("微软雅黑", Font.PLAIN, 15));
//        button_8.setBackground(new Color(231, 215, 183));
        button_8.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        	}
        });
        button_8.setIcon(new ImageIcon(this.getClass().getResource("img\\添加类型单个添加.gif")));
        p1.add(button_8);
        //添加跳转
        button_8.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				AddRoomType art=new AddRoomType();
				art.setVisible(true);
			}
		});
        
        JButton button_9 = new JButton("\u4FEE\u6539\u7C7B\u578B");
        button_9.setBounds(307, 234, 133, 27);
        button_9.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		
        	}
        });
        button_9.setFont(new Font("微软雅黑", Font.PLAIN, 15));
//        button_9.setBackground(new Color(231, 215, 183));
        button_9.setIcon(new ImageIcon(this.getClass().getResource("img\\修改类型.gif")));
        p1.add(button_9);
        //修改房间类型跳转界面
        button_9.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (table_1.getSelectedColumn()<0) {
					JOptionPane.showMessageDialog(null, "请选择要修改的数据！");
				}else {
					UNalterRoomType ulrt = new UNalterRoomType(roomType);
					ulrt.setVisible(true);
				}
			}
		});
        
        
        JButton button_10 = new JButton("\u5220\u9664\u7C7B\u578B");
        button_10.setBounds(462, 234, 133, 27);
        button_10.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        	}
        });
        button_10.setFont(new Font("微软雅黑", Font.PLAIN, 15));
//        button_10.setBackground(new Color(231, 215, 183));
        button_10.setIcon(new ImageIcon(this.getClass().getResource("img\\删除类型.gif")));
        p1.add(button_10);
        //提示选中数据
        button_10.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (table_1.getSelectedColumn()<0) {
					JOptionPane.showMessageDialog(null, "请选择要删除的数据！");
				}else {
					//跳出提示删除窗口
							int result = JOptionPane.showConfirmDialog(null,"注意，删除【房间类型】操作将会于【房间类型】相关的所有【房间信息】一并删除。您确定要删除在表格中选中的类型条目吗？","删除类型提示",0,1);
							//判断用户选中
							if(result == JOptionPane.OK_OPTION){
								String sql="DELETE from roomtype where r_type='"+roomType.getR_Type()+"'";
								System.out.println(sql);
								int updata=up.addData(sql);
							if (updata>0) {
								JOptionPane.showMessageDialog(null, "删除成功！");
							}
			                }
						}
					};
			}
		);
        
        JButton button_11 = new JButton("\u623F\u8D39\u6253\u6298");
        button_11.setBounds(626, 234, 127, 27);
        button_11.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        	}
        });
        button_11.setFont(new Font("微软雅黑", Font.PLAIN, 15));
//        button_11.setBackground(new Color(231, 215, 183));
        button_11.setIcon(new ImageIcon(this.getClass().getResource("img\\房费打折.gif")));
        p1.add(button_11);
        
        JLabel label_29 = new JLabel("\u6309\u5305\u53A2\u7C7B\u578B\u8FC7\u6EE4\uFF1A");
        label_29.setBounds(14, 279, 127, 18);
        p1.add(label_29);
        
        JComboBox comboBox = new JComboBox();
        comboBox.setBounds(143, 276, 164, 24);
        comboBox.setFont(new Font("微软雅黑", Font.PLAIN, 15));
        comboBox.setModel(new DefaultComboBoxModel(new String[] {"\u663E\u793A\u5168\u90E8\u623F\u95F4\u4FE1\u606F", "\u663E\u793A\u5168\u90E8\u5355\u4EBA\u95F4\u7C7B\u578B", "\u663E\u793A\u5168\u90E8\u5957\u623F\u7C7B\u578B"}));
        p1.add(comboBox);
        
        JButton button_12 = new JButton("\u7B5B\u9009");
        button_12.setBounds(317, 276, 113, 27);
        button_12.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        	}
        });
        button_12.setFont(new Font("微软雅黑", Font.PLAIN, 15));
//        button_12.setBackground(new Color(231, 215, 183));
        button_12.setIcon(new ImageIcon(this.getClass().getResource("img\\筛选.gif")));
        p1.add(button_12);
        
        
        
        JButton button_13_1 = new JButton("\u6279\u91CF\u6DFB\u52A0");
        button_13_1.setBounds(159, 477, 127, 27);
       
        button_13_1.setFont(new Font("微软雅黑", Font.PLAIN, 15));
//        button_13_1.setBackground(new Color(231, 215, 183));
        button_13_1.setIcon(new ImageIcon(this.getClass().getResource("img\\批量添加.gif")));
        p1.add(button_13_1);
        //添加批量添加房间界面
        button_13_1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO 自动生成的方法存根
				BatchAddRoom bar=new BatchAddRoom();
				bar.setVisible(true);
				
			}
		});
        
        
        JButton button_13_2 = new JButton("\u5220\u9664\u623F\u95F4");
        button_13_2.setBounds(347, 477, 133, 27);
        button_13_2.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        	}
        });
        button_13_2.setFont(new Font("微软雅黑", Font.PLAIN, 15));
//        button_13_2.setBackground(new Color(231, 215, 183));
        button_13_2.setIcon(new ImageIcon(this.getClass().getResource("img\\cancel.gif")));
        p1.add(button_13_2);
        //添加删除房间提示窗口
        button_13_2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (table_3.getSelectedColumn()<0) {
					JOptionPane.showMessageDialog(null, "请选择要删除的数据！");
				}else {
					//跳出提示删除窗口
					int result = JOptionPane.showConfirmDialog(null,"您确定要删除房间信息吗？","删除房间提示",0,1);
					//判断用户选中
				if(result == JOptionPane.OK_OPTION){
					String sql="DELETE FROM roominfo WHERE roomid="+roomInfo.getRoomID();
					System.out.println(sql);
					int updata=up.addData(sql);
				if (updata>0) {
					JOptionPane.showMessageDialog(null, "删除成功！");
					}
			         }
							
						}
					};
				
			}
		);
        
        
        JButton button_13_3 = new JButton("\u4FEE\u6539\u623F\u95F4");
        button_13_3.setBounds(551, 477, 127, 27);
        button_13_3.setFont(new Font("微软雅黑", Font.PLAIN, 15));
//        button_13_3.setBackground(new Color(231, 215, 183));
        button_13_3.setIcon(new ImageIcon(this.getClass().getResource("img\\修改类型.gif")));
        p1.add(button_13_3);
        //添加修改房间信息界面
        button_13_3.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (table_3.getSelectedColumn()<0) {
					JOptionPane.showMessageDialog(null, "请选择要修改的数据！");
				}else {
					AlterRoomInfo ari=new AlterRoomInfo(roomInfo);
					ari.setVisible(true);
			    }
				}
			}
		);
        
        button_13_3.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
			}
		});
        
        JScrollPane scrollPane_2 = new JScrollPane();
        scrollPane_2.setBounds(24, 70, 837, 151);
        p1.add(scrollPane_2);
        
        String[] heads= {"房间类型","预设单价","钟点价格/小时","床位数量","能否按钟点计费"};
        Object[][] body= RoomSelect.getRoomType();
        DefaultTableModel roomDT=new DefaultTableModel(body,heads);
        table_1 = new JTable(roomDT);
        
        scrollPane_2.setViewportView(table_1);
        //表格监听事件
        table_1.addMouseListener(new MouseAdapter() {
        	@Override
			public void mouseClicked(MouseEvent e) {
        		int row = table_1.getSelectedRow();
        		roomType.setR_Type(table_1.getValueAt(row, 0).toString());
        		roomType.setPrice(Double.valueOf(table_1.getValueAt(row, 1).toString()));
        		roomType.setHour_Price(Integer.parseInt(table_1.getValueAt(row, 2).toString()));
        		roomType.setBed(Integer.parseInt(table_1.getValueAt(row, 3).toString()));
        		roomType.setWhethe(table_1.getValueAt(row, 4).toString());
        		
          	}
		});
        
       //房间信息表
        JScrollPane scrollPane_3 = new JScrollPane();//添加框架
        scrollPane_3.setBounds(14, 310, 837, 151);
        p1.add(scrollPane_3);
        String[] roomInfoHead={"房间号","房间类型","房间状态","所在区域","房间电话"};
        Object[][] roomInfoBody= RoomSelect.getRoomInfo();
        DefaultTableModel roomInfoDT=new DefaultTableModel(roomInfoBody,roomInfoHead);
        table_3 = new JTable(roomInfoDT);
        scrollPane_3.setViewportView(table_3);
        //表格监听事件
        table_3.addMouseListener(new MouseAdapter() {
        	@Override
			public void mouseClicked(MouseEvent e) {
        		int row = table_3.getSelectedRow();
        		roomInfo.setRoomID(Integer.parseInt(table_3.getValueAt(row, 0).toString()));
        		roomInfo.setRoomType(table_3.getValueAt(row, 1).toString());
        		rs=table_3.getValueAt(row, 2).toString();
        		roomInfo.setLocation(table_3.getValueAt(row, 3).toString());
        		roomInfo.setRoom_phone(table_3.getValueAt(row, 4).toString());
        		
          	}
		});
        
        
		//客户类型面板
		JPanel p2=new JPanel() ;
//		p2.setBackground(new Color(231, 215, 183));
		p2.setLayout(null);
		tabbedPane.addTab("客户类型设置",new ImageIcon(this.getClass().getResource("img\\设置界面图标\\开头4个\\u02.gif")), p2);
        
        JLabel label_26 = new JLabel("\u5BA2\u6237\u7C7B\u578B");
        label_26.setFont(new Font("微软雅黑", Font.PLAIN, 15));
        label_26.setBounds(14, 5, 72, 18);
        p2.add(label_26);
        
        Object[] coHead= {"客户类型编号","客户类型","打折比率"};
        Object[][] coBody= {
        		{"","",""},{"","",""},
        		{"","",""},{"","",""}
        };
        DefaultTableModel coDt=new DefaultTableModel(coBody,coHead);
        
        JButton button_5 = new JButton("\u6DFB\u52A0\u7C7B\u578B");
        button_5.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        	}
        });
        button_5.setFont(new Font("微软雅黑", Font.PLAIN, 15));
//        button_5.setBackground(new Color(231, 215, 183));
        button_5.setIcon(new ImageIcon(this.getClass().getResource("img\\添加类型单个添加.gif")));
        button_5.setBounds(185, 229, 126, 27);
        p2.add(button_5);
        //添加客户类型界面
        button_5.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO 自动生成的方法存根
				AddCustomer ac=new AddCustomer();
				ac.setVisible(true);
			}
		});
        
        JButton button_5_1 = new JButton("\u4FEE\u6539\u7C7B\u578B");//修改类型
        button_5_1.setFont(new Font("微软雅黑", Font.PLAIN, 15));
//        button_5_1.setBackground(new Color(231, 215, 183));
        button_5_1.setIcon(new ImageIcon(this.getClass().getResource("img\\修改类型.gif")));
        button_5_1.setBounds(379, 229, 126, 27);
        p2.add(button_5_1);
        //添加修改客户类型界面
        button_5_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {//提示用户选择数据
				if (table_4.getSelectedColumn()<0) {
					JOptionPane.showMessageDialog(null, "请选择要修改的数据！");
				}else {
					AlterCustomer unac=new AlterCustomer(customertype);
					unac.setVisible(true);
				}
			}
		});
        
        JButton button_5_1_1 = new JButton("\u5220\u9664\u7C7B\u578B");
        button_5_1_1.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        	}
        });
        button_5_1_1.setFont(new Font("微软雅黑", Font.PLAIN, 15));
//        button_5_1_1.setBackground(new Color(231, 215, 183));
        button_5_1_1.setIcon(new ImageIcon(this.getClass().getResource("img\\cancel.gif")));
        button_5_1_1.setBounds(550, 229, 126, 27);
        p2.add(button_5_1_1);
        //添加删除类型提示窗口
        button_5_1_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (table_4.getSelectedColumn()<0) {
					JOptionPane.showMessageDialog(null, "请选择要删除的数据！");
				}else {
					//删除房间提示
					int result = JOptionPane.showConfirmDialog(null,"注意，删除【客户类型】操作将会于【客户类型】相关的所有【客户信息】一并删除。您确定要删除在表格中选中的类型条目吗？","删除类型提示",0,1);
					//判断用户选中
					if(result == JOptionPane.OK_OPTION){
						String sql="DELETE from customertype where c_type='"+customertype.getcType()+"'";
						System.out.println(sql);
//						int updata=up.addData(sql);
//					if (updata>0) {
//						JOptionPane.showMessageDialog(null, "删除成功！");
//							}
			                }
							
						}
					};
				
			}
		);
        
        JLabel label_23 = new JLabel("\u623F\u95F4\u8D39\u6253\u6298");
        label_23.setFont(new Font("微软雅黑", Font.PLAIN, 15));
        label_23.setBounds(14, 277, 106, 18);
        p2.add(label_23);
        
        
        Object[] co2Heads= {"用户登录ID","用户权限"};
	    Object[][] co2Body= {
	    		{"",""},
	   			{"",""}
	     };
	    DefaultTableModel co2Dt=new DefaultTableModel(co2Body,co2Heads);
        
        JButton button_6 = new JButton("\u623F\u95F4\u8D39\u6253\u6298");
        button_6.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        	}
        });
        button_6.setFont(new Font("微软雅黑", Font.PLAIN, 15));
//        button_6.setBackground(new Color(231, 215, 183));
        button_6.setIcon(new ImageIcon(this.getClass().getResource("img\\房费打折.gif")));
        button_6.setBounds(379, 481, 135, 27);
        p2.add(button_6);
        
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(24, 36, 837, 182);
        p2.add(scrollPane);
        
        table_2 = new JTable();
        scrollPane.setColumnHeaderView(table_2);
        
        //客户类型表里添加数据
        String[] customerHead= {"客户类型编号","客户类型","打折比率"};
        Object[][] customerBody=CustomerTable.getRoomType();
        DefaultTableModel CustomerTypeDt=new DefaultTableModel(customerBody,customerHead);
        table_4 = new JTable(CustomerTypeDt);
        scrollPane.setViewportView(table_4);
        //客户类型表监听事件
        table_4.addMouseListener(new MouseAdapter() {
        	@Override
			public void mouseClicked(MouseEvent e) {
        		int row = table_4.getSelectedRow();
        		customertype.setcId(Integer.parseInt(table_4.getValueAt(row, 0).toString()));
        		customertype.setcType( table_4.getValueAt(row, 1).toString());
        		customertype.setDiscount( table_4.getValueAt(row, 2).toString());
        		
          	}
		});
        
        
        JScrollPane scrollPane_1 = new JScrollPane();
        scrollPane_1.setBounds(14, 304, 847, 164);
        p2.add(scrollPane_1);
        
        //房间费打折表
        String[] roomTypeHead= {"房间类型","预设单价"};
        Object[][] roomTypeBody=CustomerTable.getRoomDiscount();
        DefaultTableModel roomTypeDt=new DefaultTableModel(roomTypeBody,roomTypeHead);
        table = new JTable(roomTypeDt);
        scrollPane_1.setViewportView(table);
        
        // 操作员面板
	    
        String[] userHead= {"操作员账号","操作员类型"};
        Object[][] userBody=UserSelect.geUserInfo();
        DefaultTableModel userDt=new DefaultTableModel(userBody,userHead);
        JTable jTable=new JTable(userDt);
		JScrollPane jll=new JScrollPane(jTable);
		jll.setBounds(14, 13, 847, 238);
		jll.setVisible(true);
		//操作员表添加监控事件
		jTable.addMouseListener(new MouseAdapter() {
	        	@Override
				public void mouseClicked(MouseEvent e) {
	        		int row = jTable.getSelectedRow();
	        		textField.setText(jTable.getValueAt(row, 0).toString());
	        		userinfo.setUserType(jTable.getValueAt(row, 1).toString());
	          	}
			});
		
		
		JPanel p3=new JPanel() ;//面板组件容器
//		p3.setBackground(new Color(231, 215, 183));
		p3.setLayout(null);
		p3.add(jll);
		tabbedPane.addTab("操作员面板",new ImageIcon(this.getClass().getResource("img\\设置界面图标\\开头4个\\u03.gif")), p3);
		
		JLabel label = new JLabel("\u8BE6\u7EC6\u4FE1\u606F");
		label.setFont(new Font("微软雅黑", Font.PLAIN, 17));
		label.setBounds(14, 279, 72, 18);
		p3.add(label);
		
		JLabel label_1 = new JLabel("\u7528\u6237\u540D\uFF1A");
		label_1.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		label_1.setBounds(40, 310, 72, 18);
		p3.add(label_1);
		
		textField = new JTextField(userinfo.getAccount());//操作员用户名文本框
		textField.setBounds(108, 307, 173, 24);
		p3.add(textField);
		textField.setColumns(10);
		
		JLabel label_2 = new JLabel("\u539F\u5BC6\u7801\uFF1A");
		label_2.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		label_2.setBounds(40, 347, 72, 18);
		p3.add(label_2);
		
		JLabel label_3 = new JLabel("\u65B0\u5BC6\u7801\uFF1A");
		label_3.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		label_3.setBounds(40, 378, 72, 18);
		p3.add(label_3);
		
		JLabel label_4 = new JLabel("\u786E\u8BA4\u5BC6\u7801\uFF1A");
		label_4.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		label_4.setBounds(29, 415, 83, 18);
		p3.add(label_4);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(108, 341, 173, 24);
		p3.add(passwordField);
		
		passwordField_1 = new JPasswordField();
		passwordField_1.setBounds(108, 375, 173, 24);
		p3.add(passwordField_1);
		
		passwordField_2 = new JPasswordField();
		passwordField_2.setBounds(108, 412, 173, 24);
		p3.add(passwordField_2);
		
		JLabel label_5 = new JLabel("\u64CD\u4F5C\u8303\u56F4");
		label_5.setFont(new Font("微软雅黑", Font.PLAIN, 17));
		label_5.setBounds(337, 279, 72, 18);
		p3.add(label_5);
		
		JRadioButton radioButton = new JRadioButton("\u65B0\u7528\u6237\u767B\u8BB0");
		radioButton.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		radioButton.setForeground(Color.BLACK);
		radioButton.setBounds(365, 306, 157, 27);
		p3.add(radioButton);
		
		JRadioButton radioButton_1 = new JRadioButton("\u4FEE\u6539\u5BC6\u7801");
		radioButton_1.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		radioButton_1.setBounds(541, 306, 157, 27);
		p3.add(radioButton_1);
		
		JRadioButton radioButton_2 = new JRadioButton("\u5220\u9664\u7528\u6237");
		radioButton_2.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		radioButton_2.setBounds(704, 306, 157, 27);
		p3.add(radioButton_2);
		
		JLabel label_6 = new JLabel("\u64CD\u4F5C\u6743\u9650");
		label_6.setFont(new Font("微软雅黑", Font.PLAIN, 17));
		label_6.setBounds(337, 357, 83, 21);
		p3.add(label_6);
		
		JRadioButton radioButton_3 = new JRadioButton("\u666E\u901A\u64CD\u4F5C\u5458");
		radioButton_3.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		radioButton_3.setBounds(365, 397, 157, 27);
		p3.add(radioButton_3);
		
		JRadioButton radioButton_4 = new JRadioButton("\u7BA1\u7406\u5458");
		radioButton_4.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		radioButton_4.setBounds(557, 397, 157, 27);
		p3.add(radioButton_4);
		
		JButton button = new JButton("   \u767B\u8BB0");
		button.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		button.setIcon(new ImageIcon(this.getClass().getResource("img\\添加类型单个添加.gif")));
//		button.setBackground(new Color(231, 215, 183));
		button.setBounds(180, 478, 113, 27);
		p3.add(button);
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (passwordField_2.getText().equals("")) {
					JOptionPane.showMessageDialog(null,"请添加数据！");
				}else {
					JOptionPane.showMessageDialog(null,"修改成功");
				}
				
			}
		});
		
		
		
		JButton button_1 = new JButton("   \u4FEE\u6539");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		button_1.setFont(new Font("微软雅黑", Font.PLAIN, 15));
//		button_1.setBackground(new Color(231, 215, 183));
		button_1.setIcon(new ImageIcon(this.getClass().getResource("img\\\u4FEE\u6539.gif")));
		button_1.setBounds(337, 478, 113, 27);
		p3.add(button_1);
		
		JButton button_2 = new JButton("\u5220\u9664");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		button_2.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		button_2.setIcon(new ImageIcon(this.getClass().getResource("img\\cancel.gif")));
//		button_2.setBackground(new Color(231, 215, 183));
		button_2.setBounds(501, 478, 113, 27);
		p3.add(button_2);
		//添加删除操作员提示
		button_2.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if (jTable.getSelectedColumn()<0) {
						JOptionPane.showMessageDialog(null, "请选择要删除的数据！");
					}else {
						//提示确定删除
						int result = JOptionPane.showConfirmDialog(null,"注意，您确定要删除在表格中选中的操作员信息吗？","删除类型提示",0,1);
								//判断用户选中
								if(result == JOptionPane.OK_OPTION){
									String sql="DELETE from userinfo where account='"+userinfo.getAccount()+"'";
									System.out.println(sql);
//								int updata=up.addData(sql);
//								if (updata>0) {
//									JOptionPane.showMessageDialog(null, "删除成功！");
//								}
				                }
								
							}
				};
					}
				
			);
		
		//设置计费面板
		JPanel p4=new JPanel() ;
//		p4.setBackground(new Color(231, 215, 183));
		
		JLabel jlMain=new JLabel("普通房间标准计费");
		jlMain.setFont(new Font("微软雅黑", Font.PLAIN, 17));
		jlMain.setBounds(14, 13, 143, 18);
		
		
		tabbedPane.addTab("设置计费",new ImageIcon(this.getClass().getResource("img\\设置界面图标\\开头4个\\u04.gif")), p4);
		p4.setLayout(null);
		p4.add(jlMain);
		
		JList list = new JList();
		list.setBounds(200, 308, 1, 1);
		p4.add(list);
		
		JLabel label_7 = new JLabel("\u5BA2\u4EBA\u5F00\u623F\u65F6\u95F4\u5728");
		label_7.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		label_7.setBounds(55, 52, 117, 18);
		p4.add(label_7);
		
		textField_1 = new JTextField();
		textField_1.setBounds(171, 49, 86, 24);
		p4.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel label_8 = new JLabel("\u70B9\u4E4B\u540E\u6309\u65B0\u7684\u4E00\u5929\u5F00\u59CB\u8BA1\u7B97");
		label_8.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		label_8.setBounds(271, 52, 206, 18);
		p4.add(label_8);
		
		JLabel label_9 = new JLabel("\u5BA2\u4EBA\u9000\u623F\u65F6\u95F4\u5728");
		label_9.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		label_9.setBounds(55, 98, 117, 18);
		p4.add(label_9);
		
		textField_2 = new JTextField();
		textField_2.setBounds(171, 95, 86, 24);
		p4.add(textField_2);
		textField_2.setColumns(10);
		
		JLabel label_10 = new JLabel("\u70B9\u4E4B\u540E\u7684\u8BA1\u4EF7\u5929\u6570\u8FFD\u52A0\u534A\u5929");
		label_10.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		label_10.setBounds(271, 98, 180, 18);
		p4.add(label_10);
		
		JLabel label_11 = new JLabel("\u5BA2\u4EBA\u9000\u623F\u65F6\u95F4\u5728");
		label_11.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		label_11.setBounds(55, 146, 117, 18);
		p4.add(label_11);
		
		textField_3 = new JTextField();
		textField_3.setBounds(171, 143, 86, 24);
		p4.add(textField_3);
		textField_3.setColumns(10);
		
		JLabel label_12 = new JLabel("\u70B9\u4E4B\u540E\u7684\u8BA1\u4EF7\u5929\u6570\u81EA\u52A8\u8FFD\u52A0\u4E00\u5929");
		label_12.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		label_12.setBounds(271, 146, 274, 18);
		p4.add(label_12);
		
		JLabel label_13 = new JLabel("\u949F\u70B9\u623F\u6807\u51C6\u8BA1\u8D39");
		label_13.setFont(new Font("微软雅黑", Font.PLAIN, 17));
		label_13.setBounds(14, 222, 158, 24);
		p4.add(label_13);
		
		JLabel label_14 = new JLabel("\u5F00\u623F\u540E");
		label_14.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		label_14.setBounds(55, 272, 72, 18);
		p4.add(label_14);
		
		JLabel label_15 = new JLabel("\u6700\u5C11\u6309");
		label_15.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		label_15.setBounds(54, 311, 72, 18);
		p4.add(label_15);
		
		JLabel label_16 = new JLabel("\u82E5\u4E0D\u8DB3\u4E00\u5C0F\u65F6\u4F46\u8D85\u8FC7");
		label_16.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		label_16.setBounds(53, 348, 146, 18);
		p4.add(label_16);
		
		JLabel label_17 = new JLabel("\u4E0D\u8DB3\u4E0A\u9762\u5206\u949F\u6570\u4F46\u8D85\u8FC7");
		label_17.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		label_17.setBounds(55, 395, 170, 18);
		p4.add(label_17);
		
		textField_4 = new JTextField();
		textField_4.setBounds(120, 271, 86, 24);
		p4.add(textField_4);
		textField_4.setColumns(10);
		
		textField_5 = new JTextField();
		textField_5.setBounds(119, 308, 86, 24);
		p4.add(textField_5);
		textField_5.setColumns(10);
		
		textField_6 = new JTextField();
		textField_6.setBounds(207, 345, 86, 24);
		p4.add(textField_6);
		textField_6.setColumns(10);
		
		textField_7 = new JTextField();
		textField_7.setBounds(221, 392, 86, 24);
		p4.add(textField_7);
		textField_7.setColumns(10);
		
		JLabel label_18 = new JLabel("\u5206\u949F\u5F00\u59CB\u8BA1\u8D39");
		label_18.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		label_18.setBounds(220, 272, 143, 18);
		p4.add(label_18);
		
		JLabel label_19 = new JLabel("\u5C0F\u65F6\u8BA1\u8D39\uFF0C\u5C0F\u4E8E\u8FD9\u4E2A\u65F6\u95F4\u7684\u6309\u6B64\u65F6\u95F4\u8BA1\u8D39");
		label_19.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		label_19.setBounds(219, 311, 291, 18);
		p4.add(label_19);
		
		JLabel label_20 = new JLabel("\u5206\u949F\u7684\u90E8\u5206\u63091\u5C0F\u65F6\u8BA1\u8D39");
		label_20.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		label_20.setBounds(307, 348, 186, 18);
		p4.add(label_20);
		
		JLabel label_21 = new JLabel("\u5206\u949F\u7684\u90E8\u5206\u6309\u534A\u5C0F\u65F6\u8BA1\u8D39");
		label_21.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		label_21.setBounds(326, 395, 195, 18);
		p4.add(label_21);
		
		JLabel label_22 = new JLabel("\u6CE8\uFF1A\u6B64\u8BBE\u7F6E\u4EC5\u9650\u4E8E\u6807\u51C6\u8BA1\u8D39\u7684\u949F\u70B9\u623F\uFF01");
		label_22.setForeground(Color.RED);
		label_22.setBounds(486, 442, 266, 18);
		p4.add(label_22);
		
		JButton button_3 = new JButton("   \u4FDD\u5B58");
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		button_3.setFont(new Font("微软雅黑", Font.PLAIN, 15));
		button_3.setIcon(new ImageIcon(this.getClass().getResource("img\\保存.gif")));
		button_3.setBounds(397, 473, 113, 27);
		p4.add(button_3);
				
		//添加添加成功提示窗口
		button_3.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (textField_7.getText().equals("")) {
					JOptionPane.showMessageDialog(null,"请添加数据！");
				}else {
					JOptionPane.showMessageDialog(null,"修改成功");
				}
				
			}
		});
		
		
		JButton button_4 = new JButton("   \u8FD4\u56DE");
		button_4.setFont(new Font("微软雅黑", Font.PLAIN, 15));
//		button_4.setBackground(new Color(231, 215, 183));
		button_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		button_4.setIcon(new ImageIcon(this.getClass().getResource("img\\exit.gif")));
		button_4.setBounds(558, 473, 113, 27);
		p4.add(button_4);
	}
	
}
