package com.yfhms.View;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.UnsupportedLookAndFeelException;

import com.yfhms.Bean.Checkout_info;
import com.yfhms.Controller.Select;
import com.yfhms.Controller.Updata;

public class Checkout extends JFrame{
	Select select = new Select();
	Updata updata = new Updata();
	JLabel lblNewLabel_1,lblNewLabel_2,lblNewLabel_3,lblNewLabel_4,lblNewLabel_5,lblNewLabel_6,lblNewLabel_7,lblNewLabel_8;
	double price,day,discount,dd,d;
	public Checkout(Checkout_info che) {
		price = Double.valueOf(che.getPrice());//预订价格
		day = Double.valueOf(che.getConsumption_days());//实住天数
		discount = Double.valueOf(che.getDiscount());//折扣
		Font fBold1 = new Font("微软雅黑",Font.PLAIN,16);//设置字体
		getContentPane().setLayout(null);
		this.setTitle("收银结账");//标题
		this.setBounds(0, 0, 900, 600);
		this.setLocationRelativeTo(null);//让窗口在屏幕中间显示
		this.setResizable(false);//让窗口大小不可改变
		//this.getContentPane().setBackground(Color.decode("#E7D7B7"));//设置背景颜色
		
		JPanel jp=new JPanel();
		jp.setBounds(0, 5, 894, 50);
		jp.setLayout(null);
		getContentPane().add(jp);
		
		JLabel label1=new JLabel("结账单号：");
		label1.setFont(fBold1);
		label1.setBounds(30, 15, 100, 20);
		jp.add(label1);
		
		
		JLabel label2=new JLabel("结账房间：");
		label2.setFont(fBold1);
		label2.setBounds(340, 15,100, 20);
		jp.add(label2);
		
		JLabel label3=new JLabel("宾客姓名：");
		label3.setFont(fBold1);
		label3.setBounds(650, 15,100, 20);
		jp.add(label3);
		
		lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setForeground(Color.BLUE);
		lblNewLabel_1.setFont(new Font("宋体", Font.BOLD, 18));
		lblNewLabel_1.setBounds(105, 15, 221, 18);
		jp.add(lblNewLabel_1);
		
		lblNewLabel_2 = new JLabel("");
		lblNewLabel_2.setForeground(Color.BLUE);
		lblNewLabel_2.setFont(new Font("宋体", Font.BOLD, 18));
		lblNewLabel_2.setBounds(416, 15, 220, 18);
		jp.add(lblNewLabel_2);
		
		lblNewLabel_3 = new JLabel("");
		lblNewLabel_3.setForeground(Color.BLUE);
		lblNewLabel_3.setFont(new Font("宋体", Font.BOLD, 18));
		lblNewLabel_3.setBounds(727, 15, 153, 18);
		jp.add(lblNewLabel_3);
		
		//第二部分窗体
		
		JPanel jp2=new JPanel();
		jp2.setBounds(0,70, 894, 230);
		jp2.setLayout(null);
		getContentPane().add(jp2);
		
		JLabel label4=new JLabel("消费金额：");
		label4.setForeground(Color.BLUE);
		label4.setFont(new Font("微软雅黑", Font.BOLD, 16));
		label4.setBounds(30, 15, 100,40);
		jp2.add(label4);
		
		JLabel label5=new JLabel("应收金额：");
		label5.setForeground(Color.BLUE);
		label5.setFont(new Font("微软雅黑", Font.BOLD, 16));
		label5.setBounds(230, 15, 100,40);
		jp2.add(label5);
		
		JLabel label6=new JLabel("已收押金：");
		label6.setForeground(Color.BLUE);
		label6.setFont(new Font("微软雅黑", Font.BOLD, 16));
		label6.setBounds(430, 15, 100,40);
		jp2.add(label6);
		
		JLabel label7=new JLabel("优惠金额：");
		label7.setForeground(Color.BLUE);
		label7.setFont(new Font("微软雅黑", Font.BOLD, 16));
		label7.setBounds(630, 15, 100,40);
		jp2.add(label7);
		
		JLabel label8=new JLabel("实收金额：");
		label8.setFont(fBold1);
		label8.setBounds(30, 70, 100,40);
		JTextField text8=new JTextField();
		text8.setBounds(130,70, 140,40);
		jp2.add(label8);
		jp2.add(text8);
		text8.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dd = Double.valueOf(lblNewLabel_6.getText());
				System.out.println(dd);
				d = Double.valueOf(lblNewLabel_5.getText());
				System.out.println(d);
				lblNewLabel_8.setText(String.valueOf(dd-d));
			}
		});
		
		JLabel label9=new JLabel("宾客支付:");
		label9.setBounds(350, 70, 100, 40);
		label9.setFont(fBold1);
		JTextField text9=new JTextField();
		text9.setBounds(450,70, 140,40);
		jp2.add(label9);
		jp2.add(text9);
		text9.addFocusListener(new FocusListener() {
			@Override
			public void focusLost(FocusEvent e) {
				if(text9.getText().equals("")){
					JOptionPane.showMessageDialog(null,"宾客支付金额为空！请检查！");
				}
				dd = Double.valueOf(text9.getText());
				d = Double.valueOf(text8.getText());
				lblNewLabel_8.setText(String.valueOf(dd-d));
				//获取输入文本框中字符的长度
				int length=text9.getText().length();
			}
			@Override
			public void focusGained(FocusEvent e) {
				
			}
		});;
		
		
		JLabel label10=new JLabel("找零:");
		label10.setForeground(Color.BLUE);
		label10.setFont(new Font("微软雅黑", Font.BOLD, 16));
		label10.setBounds(640, 70,52,40);
		jp2.add(label10);
		
		JLabel label11=new JLabel("结账备注:");
		label11.setFont(fBold1);
		label11.setBounds(30, 150, 100,40);
		jp2.add(label11);
		JTextField text11=new JTextField();
		text11.setBounds(130,150, 400,40);
		jp2.add(text11);
		
		ImageIcon img=new ImageIcon(this.getClass().getResource("img/u04.gif"));
	    JButton jb=new JButton("结账",img);
	    jb.setBounds(560, 150, 90, 40);
	    jp2.add(jb);
	    jb.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				SimpleDateFormat sfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String strDate = sfDate.format(new Date());
				String bz = text11.getText();
				if (bz.equals("")) {
					bz = "无备注";
				} else {
					bz = text11.getText();
				}
				Login login = new Login();
				String user = login.getUser();
				System.out.println(user);
//				if (user.equals("")) {
//					user = "1";
//				}else {
//					user = Login.getUser();
//				}
				if (text8.getText().equals("")) {
					JOptionPane.showMessageDialog(null, "请输入实收金额！！！");
				} else {
					String sql = "INSERT INTO `checkout` VALUES (null, '"+lblNewLabel_1.getText()+"', '"+(int)day+"', '"+text8.getText()+"', '"+strDate+"', '"+user+"', '"+text11.getText()+"');";
					int i = updata.addData(sql);
					if (i>0) {
						String updateRoomintoSQL = "UPDATE roominfo set state=1 WHERE roomid='"+che.getRoomid()+"'";
						int reselt1 = updata.addData(updateRoomintoSQL);
						String updateLinSQL = "UPDATE livein set status=4 WHERE in_no='"+che.getRoomid()+"'";
						int reselt2 = updata.addData(updateRoomintoSQL);
						if (reselt1>0&&reselt2>0) {
							JOptionPane.showMessageDialog(null, "结账成功！");
							MainJFrame m = new MainJFrame();
							m.dispose();
							m.setVisible(true);
							dispose();
						} else {
							JOptionPane.showMessageDialog(null, "结账失败！");
						}
					} else {
						JOptionPane.showMessageDialog(null, "结账失败！！");
					}
				}
			}
		});
	   
	    ImageIcon img2=new ImageIcon(this.getClass().getResource("img/cancel.gif"));
	    JButton jb2=new JButton("取消",img2);
	    jb2.setBounds(700, 150, 90, 40);
	    jp2.add(jb2);
	    jb2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
	    
	    lblNewLabel_4 = new JLabel("");
	    lblNewLabel_4.setForeground(Color.BLUE);
	    lblNewLabel_4.setFont(new Font("宋体", Font.BOLD, 17));
	    lblNewLabel_4.setBounds(110, 27, 106, 18);
	    jp2.add(lblNewLabel_4);
	    
	    lblNewLabel_5 = new JLabel("");
	    lblNewLabel_5.setFont(new Font("宋体", Font.BOLD, 18));
	    lblNewLabel_5.setForeground(Color.RED);
	    lblNewLabel_5.setBounds(320, 25, 109, 18);
	    jp2.add(lblNewLabel_5);
	    
	    lblNewLabel_6 = new JLabel("");
	    lblNewLabel_6.setFont(new Font("宋体", Font.BOLD, 18));
	    lblNewLabel_6.setForeground(Color.RED);
	    lblNewLabel_6.setBounds(516, 25, 100, 18);
	    jp2.add(lblNewLabel_6);
	    
	    lblNewLabel_7 = new JLabel("");
	    lblNewLabel_7.setFont(new Font("宋体", Font.BOLD, 18));
	    lblNewLabel_7.setForeground(Color.RED);
	    lblNewLabel_7.setBounds(715, 25, 116, 18);
	    jp2.add(lblNewLabel_7);
	    
	    lblNewLabel_8 = new JLabel("");
	    lblNewLabel_8.setForeground(Color.RED);
	    lblNewLabel_8.setFont(new Font("宋体", Font.BOLD, 18));
	    lblNewLabel_8.setBounds(689, 82, 129, 18);
	    jp2.add(lblNewLabel_8);
	    //第三部分
	    JPanel jp3=new JPanel();
	    jp3.setLayout(null);
	    jp3.setBounds(0,310, 894, 230);
	    JLabel lableone=new JLabel("结账区内房间消费清单");
	    lableone.setFont(fBold1);
	    lableone.setBounds(340, 0, 180, 40);
	    jp3.add(lableone);
	    getContentPane().add(jp3);
		
	    String[] heads= {"房间号","单价","折扣","折扣价","消费天数","消费金额","消费时间"};
		Object[][] data= select.getCheckoutInfo();
		JTable jTable = new JTable(data,heads);
		jTable.setFillsViewportHeight(true);
		jTable.setBounds(0, 50, 300, 100);
		int v=ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED;
		int h=ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED;
	    JScrollPane jsp=new JScrollPane(jTable,v,h);
	    jsp.setSize(850, 200);
	    jsp.setBounds(14, 40, 866, 200);

	    jp3.add(jsp);
	    
		//如果不满一天按照一天计算
		if (day==0) {
			day=1;
		}
	    lblNewLabel_1.setText(che.getStatement_no());
	    System.out.println(che.getStatement_no());
	    lblNewLabel_2.setText(che.getRoomid());
	    System.out.println(che.getRoomid());
	    lblNewLabel_3.setText(che.getGuest_name());
	    lblNewLabel_4.setText(che.getPrice());
	    lblNewLabel_5.setText(String.valueOf(price*day*discount));
	    lblNewLabel_6.setText(che.getDeposit());
	    lblNewLabel_7.setText(String.valueOf((price-price*day*discount)));
	    text8.setText(lblNewLabel_5.getText());
	}
}
