package com.yfhms.View;
//预订信息主界面
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.DefaultTableModel;

import com.yfhms.Controller.Select;
import com.yfhms.Controller.Updata;

public class Custom_type extends JFrame {
	Select select = new Select();
	JComboBox jc;
	Updata up=new Updata();
	
//	Object[][] object=select.getRoom();
	public  Custom_type() {
		Font fBold = new Font("微软雅黑",Font.PLAIN,15);//设置字体
		Font fBold1 = new Font("微软雅黑",Font.PLAIN,18);//设置字体
	
		this.setLayout(null);
		this.setTitle("修改客房预订");//标题
		this.setBounds(0, 0, 700, 600);
		this.setLocationRelativeTo(null);//让窗口在屏幕中间显示
		this.setResizable(false);//让窗口大小不可改变
		//this.getContentPane().setBackground(Color.decode("#E7D7B7"));//设置背景颜色
		
		

		JLabel lablex=new JLabel("基本预订信息");
		lablex.setBounds(280, 0, 700, 30);
		lablex.setFont(fBold);
		
		JLabel lable1=new JLabel("宾客姓名：");
		lable1.setBounds(20, 45, 100, 30);
		lable1.setFont(fBold1);
		JTextField text1 = new JTextField();
		text1.setBounds(110,45, 150,30);

		JLabel lable2=new JLabel("联系电话：");
		lable2.setBounds(20, 90, 100, 30);
		lable2.setFont(fBold1);
		JTextField text2 = new JTextField();
		text2.setBounds(110,90, 150,30);
		
		JLabel lable3=new JLabel("预订规格：");
		lable3.setBounds(20, 140, 100, 30);
		lable3.setFont(fBold1);
		JTextField text3 = new JTextField();
		text3.setBounds(110,140, 150,30);
		this.add(text3);
		JLabel lable4=new JLabel("房间编号：");
		lable4.setBounds(20, 190, 100, 30);
		lable4.setFont(fBold1);
		JTextField text4 = new JTextField();
		text4.setBounds(110,190, 150,30);
		this.add(text4);

		
		JLabel lable5=new JLabel("预抵时间：");
		lable5.setBounds(20, 240, 100, 30);
		lable5.setFont(fBold1);
		JTextField text5 = new JTextField();
		text5.setBounds(110,240, 150,30);
		
		JLabel lable6=new JLabel("保留时间：");
		lable6.setBounds(20, 290, 100, 30);
		lable6.setFont(fBold1);
		JTextField text6 = new JTextField();
		text6.setBounds(110,290, 150,30);
		
		ImageIcon imgadd=new ImageIcon(this.getClass().getResource("img/add.gif"));
		  
		JButton jb1=new JButton("添加预订");
		jb1.setBounds(110, 335,150, 40);
	
		
		
		jb1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				Reserve re=new Reserve();
				re.setVisible(true);
//				dispose();
			}
		});
		//jb1.setBackground(Color.decode("#E7D7B7"));
		JPanel jp=new JPanel();//左边窗体
		jp.setLayout(null);
		jp.setBounds(300, 80, 350, 300);
		JLabel lableone=new JLabel("本次预订的房间");
		lableone.setBounds(410, 40, 150, 30);
		lableone.setFont(fBold);
		jp.add(lableone);

		String[] heads= {"预订规格","房间"};
		Object[][] data= {
	
				
		};
	    
	   
	    Object[][] ob1=select.getRoomInfoAndRoomtype();
	    DefaultTableModel dt=new DefaultTableModel(ob1,heads);	 
	    

		JTable jTable = new JTable(dt);
		int v=ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED;
		int h=ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED;
	    JScrollPane jsp=new JScrollPane(jTable,v,h);
	    jsp.setBounds(0, 0, 350, 280);
	   

	    JLabel lable7=new JLabel("备注:");
	    lable7.setBounds(30, 410, 40, 40);
	    lable7.setFont(fBold1);
	    JTextField text7 = new JTextField();
	    text7.setBounds(100, 390, 550, 70);
	    JCheckBox jk=new JCheckBox("到达时间是否自动取消预订");
	    jk.setBounds(30, 460, 200,40);
	      
    	
    	
	    jTable.addMouseListener(new MouseAdapter() {
	    	@Override
	    	public void mouseClicked(MouseEvent e) {
	    		int row = jTable.getSelectedRow();
	        	int col = jTable.getSelectedColumn();
	    	    String content = (String) jTable.getValueAt(row, 1);
	    	    Object[][] object = select.getRoom(content);	
		    	System.out.println(content);	    	
		    	text1.setText((String) object[0][0]);
		    	text2.setText((String) object[0][1]);
		    	text3.setText((String) object[0][2]);
			    text4.setText((String) object[0][3]);	
		    	text5.setText((String) object[0][4]);
		    	text6.setText((String) object[0][5]);
		    	text7.setText((String) object[0][6]);
			}
		});
	    
	   ImageIcon img=new ImageIcon(this.getClass().getResource("img/cancel.gif"));
	   JButton jb2=new JButton("删除",img);
	   jb2.setBounds(180, 505, 90, 40);
	   
	   //删除数据
	   
	   jb2.addActionListener(new ActionListener() {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			int result = JOptionPane.showConfirmDialog(null,"确定要删除改条信息吗？","删除提示",0,1);
			if (result == JOptionPane.OK_OPTION) {
				String sql="DELETE  FROM reserve WHERE r_name='"+text1.getText()+"'";
				System.out.println(sql);
     			int updata=up.addData(sql);

			}

		}
	});
	   
	   
	   ImageIcon img2=new ImageIcon(this.getClass().getResource("img/cancel.gif"));
	   JButton jb3=new JButton("取消",img2);
	   jb3.setBounds(400, 504, 90, 40);
	   jb3.addActionListener(new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {
			dispose();
		}
	});

		this.add(lablex);	
		this.add(lable1);
		this.add(lable2);
		this.add(lable3);
		this.add(lable4);
		this.add(lable5);
		this.add(lable6);
		this.add(text1);
		this.add(text2);
		this.add(text5);
		this.add(text6);
		this.add(jp);
		this.add(lableone);
		jp.add(jsp);
		this.add(jb1);
		this.add(lable7);
		this.add(text7);
		this.add(jk);
		this.add(jb2);
		this.add(jb3);
		
	}
	
	}
	
