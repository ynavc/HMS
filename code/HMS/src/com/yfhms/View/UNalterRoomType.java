package com.yfhms.View;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import com.yfhms.Bean.RoomType;
import com.yfhms.Controller.Updata;

import javax.swing.JCheckBox;
import javax.swing.JButton;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;

public class UNalterRoomType extends JFrame  {
	private JTextField textField_1_fjlx;
	private JTextField textField_2_cwwsl;
	private JTextField textField_3_ysdj;
	private JTextField textField_4_ysyj;
	private JTextField textField_5_zdjf;
	
	Updata up=new Updata();
	String roomtypes;                                                   
	private JTextField textField;
	public UNalterRoomType(RoomType roomType) {
		super.setTitle("当前房间类型");
		this.setBounds(0, 0, 500, 492);//设置大小
		this.setLocationRelativeTo(null);//让窗口在屏幕中间显示
 	    this.setResizable(true);//让窗口大小不可改变
		getContentPane().setLayout(null);
		
		JLabel label = new JLabel("\u5F53\u524D\u623F\u95F4\u7C7B\u578B");
		label.setBounds(14, 13, 128, 18);
		getContentPane().add(label);
		
		JLabel label_2 = new JLabel("\u623F\u95F4\u7C7B\u578B\uFF1A");
		label_2.setBounds(79, 55, 97, 18);
		getContentPane().add(label_2);
		
		JLabel label_3 = new JLabel("\u5E8A\u4F4D\u6570\u91CF\uFF1A");
		label_3.setBounds(79, 99, 97, 18);
		getContentPane().add(label_3);
		
		JLabel label_4 = new JLabel("\u9884\u8BBE\u5355\u4EF7\uFF1A");
		label_4.setBounds(79, 136, 97, 18);
		getContentPane().add(label_4);
		
		JLabel label_5 = new JLabel("\u9884\u8BBE\u62BC\u91D1\uFF1A");
		label_5.setBounds(79, 167, 97, 18);
		getContentPane().add(label_5);
		
		JLabel label_6 = new JLabel("\u949F\u70B9\u8BA1\u8D39\uFF1A");
		label_6.setBounds(79, 205, 97, 18);
		getContentPane().add(label_6);
		
		textField_1_fjlx = new JTextField();
		textField_1_fjlx.setBounds(167, 52, 191, 24);
		getContentPane().add(textField_1_fjlx);
//		textField_1.setColumns(10);
		
		textField_2_cwwsl = new JTextField();
		textField_2_cwwsl.setBounds(167, 96, 191, 24);
		getContentPane().add(textField_2_cwwsl);
		textField_2_cwwsl.setColumns(10);
		
		textField_3_ysdj = new JTextField();
		textField_3_ysdj.setBounds(167, 130, 191, 24);
		getContentPane().add(textField_3_ysdj);
		textField_3_ysdj.setColumns(10);
		
		textField_4_ysyj = new JTextField();
		textField_4_ysyj.setBounds(167, 167, 191, 24);
		getContentPane().add(textField_4_ysyj);
		textField_4_ysyj.setColumns(10);
		
		textField_5_zdjf = new JTextField();
		textField_5_zdjf.setBounds(167, 198, 191, 24);
		getContentPane().add(textField_5_zdjf);
		textField_5_zdjf.setColumns(10);
		
		JCheckBox checkBox = new JCheckBox("\u5141\u8BB8\u5F00\u949F\u70B9\u623F");
		checkBox.setBounds(97, 288, 133, 27);
		getContentPane().add(checkBox);
		//把客户选中的数据写入文本框
		textField_1_fjlx.setText(roomType.getR_Type());
		roomtypes = roomType.getR_Type();
		textField_2_cwwsl.setText(String.valueOf(roomType.getBed()));
		textField_3_ysdj.setText(String.valueOf(roomType.getPrice()));
		textField_4_ysyj.setText(String.valueOf(roomType.getHour_Price()));
		textField_5_zdjf.setText(roomType.getWhethe());
		
		JButton button = new JButton("保存");
		button.setIcon(new ImageIcon(this.getClass().getResource("img/保存.gif")));
		button.setBounds(79, 336, 113, 27);
		getContentPane().add(button);
		//点击保存保存数据
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {				
				String roomType=textField_1_fjlx.getText();//房间类型
				String bedNum=textField_2_cwwsl.getText();//床位数量
				String price=textField_3_ysdj.getText();//预设单价
				String hourPrice=textField_4_ysyj.getText();//钟点押金
				String hour=textField_5_zdjf.getText();//钟点计费
				String sql="UPDATE roomtype set r_type='"+roomType+"', price= "+price+", hour_price = "+hourPrice+", bed= "+bedNum+" WHERE r_type='"+roomtypes+"'";
				System.out.println(sql);
				int updata=up.addData(sql);
			}
		});
		
		
		JButton button_1 = new JButton("\u53D6\u6D88");
		button_1.setIcon(new ImageIcon(this.getClass().getResource("img/cancel.gif")));
		button_1.setBounds(281, 336, 113, 27);
		getContentPane().add(button_1);
		
		JLabel label_1 = new JLabel("\u80FD\u5426\u5F00\u949F\u70B9\u623F\uFF1A");
		label_1.setBounds(71, 250, 105, 18);
		getContentPane().add(label_1);
		
		textField = new JTextField();
		textField.setBounds(190, 247, 86, 24);
		getContentPane().add(textField);
		textField.setColumns(10);
		button_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
			
	}
}
