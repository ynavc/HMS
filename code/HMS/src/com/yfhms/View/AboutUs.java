package com.yfhms.View;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTable;

import java.awt.BorderLayout;
import java.awt.Font;
import javax.swing.JTextPane;

public class AboutUs extends JFrame {
	public AboutUs() {
		getContentPane().setLayout(null);
		this.setTitle("关于我们");//标题
		this.setBounds(0, 0, 601, 792);
		this.setLocationRelativeTo(null);//让窗口在屏幕中间显示
		this.setResizable(false);//让窗口大小不可改变
				
		JLabel jl3=new JLabel(new ImageIcon(this.getClass().getResource("img\\关于我们素材.jpg")));
        getContentPane().add(jl3);
        jl3.setBounds(-81, 0, 754, 267);
        getContentPane().add(jl3);
        
        JLabel label = new JLabel("\u5173\u4E8E\u6211\u4EEC");
        label.setFont(new Font("微软雅黑", Font.PLAIN, 22));
        label.setBounds(254, 281, 168, 41);
        getContentPane().add(label);
        
        JLabel lblNewLabel = new JLabel("\u5BF9\u4E8E\u4F20\u7EDF\u7684\u9152\u5E97\u7BA1\u7406\u65B9\u6CD5\u6765\u8BF4\uFF0C\u4E0D\u7BA1\u662F\u7EB8\u8D28\u7684\u767B\u8BB0\u4EE5\u53CA\u6570\u636E\u7684\u5B58\u50A8\u548C\u8D22\u52A1\u65B9\u9762");
        lblNewLabel.setFont(new Font("宋体", Font.PLAIN, 16));
        lblNewLabel.setBounds(49, 348, 558, 18);
        getContentPane().add(lblNewLabel);
        
        JLabel lblNewLabel_1 = new JLabel("\u65B9\u9762\u7684\u8BA1\u7B97\uFF0C\u5B89\u5168\u6027\u4F4E\u5E76\u4E14\u6548\u7387\u4F4E\u4E0B\uFF0C\u4F7F\u7528\u672C\u8F6F\u4EF6\u89E3\u51B3\u4E86\u9152\u5E97\u5404\u65B9\u9762\u7684\u8C03\u63A7\u4E0E\u7BA1\u7406");
        lblNewLabel_1.setFont(new Font("宋体", Font.PLAIN, 16));
        lblNewLabel_1.setBounds(14, 369, 585, 18);
        getContentPane().add(lblNewLabel_1);
        
        JLabel lblNewLabel_2 = new JLabel("\u5BA2\u4EBA\u5165\u4F4F\u7387\u5FEB\u901F\u67E5\u8BE2\uFF0C\u4F4F\u5BBF\u5BA2\u4EBA\u4FE1\u606F\u767B\u8BB0\u3001\u7A7A\u4F59\u623F\u95F4\u67E5\u8BE2\u7B49\u64CD\u4F5C\u3002\r\n\u9152\u5E97\u5BA2\u623F\u7BA1\u7406\u4F7F");
        lblNewLabel_2.setFont(new Font("宋体", Font.PLAIN, 16));
        lblNewLabel_2.setBounds(14, 385, 585, 29);
        getContentPane().add(lblNewLabel_2);
        
        JLabel lblNewLabel_3 = new JLabel("\u7528\u624B\u5DE5\u5904\u7406\u8D26\u52A1\uFF0C\u5B58\u5728\u8BB8\u591A\u73B0\u91D1\u6D41\u5931\u7684\u6F0F\u6D1E\uFF0C\u4F7F\u7528\u8F6F\u4EF6\u6765\u7BA1\u7406\u5BBE\u9986\u4E1A\u52A1\uFF0C\u7ED3\u8D26\u65E2\u51C6");
        lblNewLabel_3.setFont(new Font("宋体", Font.PLAIN, 16));
        lblNewLabel_3.setBounds(14, 413, 585, 18);
        getContentPane().add(lblNewLabel_3);
        
        JLabel lblNewLabel_4 = new JLabel("\u786E\uFF0C\u901F\u5EA6\u53C8\u5FEB\uFF0C\u800C\u4E14\u7EDF\u8BA1\u7684\u62A5\u8868\u4E5F\u5FEB\u6377\u3002\u5B83\u53EF\u4EE5\u6700\u5927\u9650\u5EA6\u5730\u53D1\u6325\u51C6\u786E\u3001\u5FEB\u6377\u3001\u9AD8\u6548");
        lblNewLabel_4.setFont(new Font("宋体", Font.PLAIN, 16));
        lblNewLabel_4.setBounds(14, 434, 585, 18);
        getContentPane().add(lblNewLabel_4);
        
        JLabel lblNewLabel_5 = new JLabel("\u7B49\u4F5C\u7528\uFF0C\u5BF9\u9152\u5E97\u7684\u4E1A\u52A1\u7BA1\u7406\u63D0\u4F9B\u5F3A\u6709\u529B\u7684\u652F\u6301\u3002");
        lblNewLabel_5.setFont(new Font("宋体", Font.PLAIN, 16));
        lblNewLabel_5.setBounds(14, 455, 566, 18);
        getContentPane().add(lblNewLabel_5);
        
        JLabel label_1 = new JLabel("\u827A\u8702\u9879\u76EE\u7EC4\u4ECB\u7ECD");
        label_1.setFont(new Font("微软雅黑", Font.PLAIN, 22));
        label_1.setBounds(223, 486, 159, 47);
        getContentPane().add(label_1);
        
        JLabel jl4=new JLabel(new ImageIcon(this.getClass().getResource("img\\logo.png")));//logo图片
        getContentPane().add(jl4);
        jl4.setBounds(31, 522, 178, 165);
        getContentPane().add(jl4);
        
        JLabel label_2 = new JLabel("\u7EC4\u957F\uFF1A\u51AF\u6DA6\u6CFD");
        label_2.setFont(new Font("宋体", Font.PLAIN, 16));
        label_2.setBounds(295, 576, 106, 18);
        getContentPane().add(label_2);
        
        JLabel label_3 = new JLabel("\u6210\u5458\uFF1A\u6768\u660E\u91D1");
        label_3.setFont(new Font("宋体", Font.PLAIN, 16));
        label_3.setBounds(294, 607, 96, 18);
        getContentPane().add(label_3);
        
        JLabel label_4 = new JLabel("\u90ED\u8D85");
        label_4.setFont(new Font("宋体", Font.PLAIN, 16));
        label_4.setBounds(418, 607, 40, 18);
        getContentPane().add(label_4);
        
        JLabel label_5 = new JLabel("\u989C\u6743\u660C");
        label_5.setFont(new Font("宋体", Font.PLAIN, 16));
        label_5.setBounds(478, 607, 53, 18);
        getContentPane().add(label_5);
        
        JLabel lblNewLabel_6 = new JLabel("\u6280\u672F\u535A\u5BA2\uFF1Ahttps://me.csdn.net/weixin_44893902");
        lblNewLabel_6.setFont(new Font("微软雅黑", Font.PLAIN, 17));
        lblNewLabel_6.setBounds(14, 700, 433, 18);
        getContentPane().add(lblNewLabel_6);
        
        JLabel lblNewLabel_7 = new JLabel("\u516C\u53F8\u5730\u5740\uFF1A\u4E91\u5357\u7701\u6606\u660E\u5E02\u4E94\u534E\u533A\u832D\u83F1\u8DEF128\u53F7 A-713\u53F7");
        lblNewLabel_7.setFont(new Font("微软雅黑", Font.PLAIN, 15));
        lblNewLabel_7.setBounds(14, 731, 433, 18);
        getContentPane().add(lblNewLabel_7);
       
		
	}
//	public static void main(String[] args) {
//		AboutUs a=new AboutUs();
//		a.setVisible(true);
//	}
}
