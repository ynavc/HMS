package com.yfhms.View;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import com.yfhms.Bean.Customertype;
import com.yfhms.Bean.RoomType;
import com.yfhms.Controller.Updata;

import java.awt.Color;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AlterCustomer extends JFrame {
	private JTextField textField_1;
	private JTextField textField_2;
	
	String customertypes;
	Updata up=new Updata();
	
	public AlterCustomer(Customertype customertype) {
		super.setTitle("修改客户类型");
		this.setBounds(0, 0, 500, 492);//设置大小
		this.setLocationRelativeTo(null);//让窗口在屏幕中间显示
 	    this.setResizable(true);//让窗口大小不可改变
		getContentPane().setLayout(null);
		
		JLabel label = new JLabel("\u5BA2\u6237\u7C7B\u578B\u4FE1\u606F");
		label.setBounds(14, 13, 104, 18);
		getContentPane().add(label);
		
		JLabel label_2 = new JLabel("\u5BA2\u6237\u7C7B\u578B\uFF1A");
		label_2.setBounds(65, 86, 93, 18);
		getContentPane().add(label_2);
		
		JLabel label_3 = new JLabel("\u6253\u6298\u6BD4\u4F8B\uFF1A");
		label_3.setBounds(65, 168, 93, 18);
		getContentPane().add(label_3);
		
		textField_1 = new JTextField();
		textField_1.setBounds(149, 83, 182, 24);
		getContentPane().add(textField_1);
//		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(149, 165, 182, 24);
		getContentPane().add(textField_2);
//		textField_2.setColumns(10);
		
		//把用户选中的值输入文本框
		textField_1.setText(customertype.getcType());
		textField_2.setText(customertype.getDiscount());
		
		
		JLabel lblNewLabel = new JLabel("\u6CE8\uFF1A\u6B64\u6253\u6298\u6BD4\u4F8B\u4EC5\u9002\u7528\u4E8E\u5546\u54C1\u9879\u76EE\uFF01\r\n8\u4E3A\u516B\u6298\uFF0C10\u4E3A\u4E0D\u6253\u6298\r\n");
		lblNewLabel.setForeground(Color.RED);
		lblNewLabel.setBounds(37, 234, 405, 18);
		getContentPane().add(lblNewLabel);
		
		JButton button = new JButton("   \u786E\u5B9A");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		button.setIcon(new ImageIcon(this.getClass().getResource("img\\保存.gif")));
		button.setBounds(65, 318, 113, 27);
		getContentPane().add(button);
		//添加sql语句来修改用户输入的数据
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {				
				customertypes=customertype.getcType();
				//修改语句
				String sql="UPDATE customertype set c_type='"+textField_1.getText()+"', discount='"+textField_2.getText()+"'  where c_type='"+customertypes+"'";
				System.out.println(sql);
				int updata=up.addData(sql);
			}
		});
		
		
		
		
		JButton button_1 = new JButton("   取消");
		button_1.setIcon(new ImageIcon(this.getClass().getResource("img\\cancel.gif")));
		button_1.setBounds(250, 318, 113, 27);
		getContentPane().add(button_1);
		
	}

}
