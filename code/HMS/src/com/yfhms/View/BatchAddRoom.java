package com.yfhms.View;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import com.yfhms.Controller.Updata;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.ImageIcon;

public class BatchAddRoom extends JFrame {
	Updata ud=new Updata(); 
	
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	
	public BatchAddRoom() {
		super.setTitle("批量添加房间信息");
		this.setBounds(0, 0, 500, 492);//设置大小
		this.setLocationRelativeTo(null);//让窗口在屏幕中间显示
 	    this.setResizable(true);//让窗口大小不可改变
		getContentPane().setLayout(null);
		
		JLabel label = new JLabel("\u6279\u91CF\u623F\u95F4\u53C2\u6570");
		label.setBounds(14, 13, 126, 18);
		getContentPane().add(label);
		
		JLabel label_1 = new JLabel("\u623F\u95F4\u7C7B\u578B\uFF1A");
		label_1.setBounds(125, 58, 90, 18);
		getContentPane().add(label_1);
		
		JLabel label_2 = new JLabel("\u8D77\u59CB\u623F\u53F7\uFF1A");
		label_2.setBounds(125, 89, 90, 18);
		getContentPane().add(label_2);
		
		JLabel label_3 = new JLabel("\u7EC8\u6B62\u623F\u53F7\uFF1A");
		label_3.setBounds(125, 120, 90, 18);
		getContentPane().add(label_3);
		
		JLabel label_4 = new JLabel("\u6807\u8BB0\u5B57\u7B26\uFF1A");
		label_4.setBounds(125, 151, 90, 18);
		getContentPane().add(label_4);
		
		JLabel label_5 = new JLabel("\u6240\u5728\u533A\u57DF\uFF1A");
		label_5.setBounds(125, 192, 90, 18);
		getContentPane().add(label_5);
		
		textField_1 = new JTextField();//起始房号
		textField_1.setBounds(210, 89, 164, 24);
		getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();//终止房号
		textField_2.setBounds(210, 120, 164, 24);
		getContentPane().add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();//标记字符
		textField_3.setBounds(210, 151, 164, 24);
		getContentPane().add(textField_3);
		textField_3.setColumns(10);
		
		textField_4 = new JTextField();//所在区域
		textField_4.setBounds(210, 189, 164, 24);
		getContentPane().add(textField_4);
		textField_4.setColumns(10);
		
		JComboBox comboBox = new JComboBox();//选择房间类型
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"\u6807\u51C6\u5355\u4EBA\u95F4", "\u6807\u51C6\u53CC\u4EBA\u95F4", "\u8C6A\u534E\u5355\u4EBA", "\u8C6A\u534E\u53CC\u4EBA", "\u5546\u52A1\u5957\u623F", "\u603B\u7EDF\u5957\u623F"}));
		comboBox.setBounds(210, 55, 104, 24);
		getContentPane().add(comboBox);
		
		
		JLabel label_6 = new JLabel("\u6CE8\u610F\uFF1A\u6279\u91CF\u6DFB\u52A0\u623F\u95F4\uFF0C\u623F\u95F4\u53F7\u7801\u4F4D\u5373\u4E3A\u623F\u95F4\u7535\u8BDD\u3002");
		label_6.setForeground(new Color(255, 0, 0));
		label_6.setBounds(81, 240, 369, 18);
		getContentPane().add(label_6);
		
		JButton button = new JButton("   \u4FDD\u5B58");
		button.setIcon(new ImageIcon(this.getClass().getResource("img\\保存.gif")));
		button.setBounds(91, 283, 113, 27);
		getContentPane().add(button);
		//保存按钮添加一个添加房间事件
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int openRoom=Integer.parseInt(textField_1.getText());
				int endRoom=Integer.parseInt(textField_2.getText());
				String roomType=comboBox.getSelectedItem().toString();
				String phone=textField_3.getText();
				String area=textField_4.getText();
				int and =endRoom-openRoom;
				//循环添加数据
				for (int i = 0; i <= and; i++) {
					String sql="INSERT roominfo VALUES('"+openRoom+"','"+roomType+"','1','"+area+"','"+(phone+openRoom)+"')";
					openRoom++;
					int updata=ud.addData(sql);
				}			
			}
		});
		
		JButton button_1 = new JButton("   \u53D6\u6D88");
		button_1.setIcon(new ImageIcon(this.getClass().getResource("img\\cancel.gif")));
		button_1.setBounds(285, 283, 113, 27);
		getContentPane().add(button_1);
		button_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
	}
	
	
}
