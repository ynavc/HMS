package com.yfhms.View;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import com.yfhms.Bean.RoomInfo;
import com.yfhms.Bean.RoomType;
import com.yfhms.Controller.Updata;

import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JList;
import javax.swing.JScrollBar;
import javax.swing.JSeparator;
import javax.swing.JComboBox;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;

public class AlterRoomInfo extends JFrame{
	private JTextField textField_1_fjhm;
	private JTextField textField_2_szqy;
	private JTextField textField_3_fjdh;
	private JTextField textField_yfjlx;
	Operator op=new Operator();
	String roomtype;//获取房间类型条件
	Updata up=new Updata();
	
	RoomInfo roominfo=new RoomInfo();
	public AlterRoomInfo(RoomInfo roomInfo) {
		super.setTitle("修改房间信息");
		this.setBounds(0, 0, 500, 492);//设置大小
		this.setLocationRelativeTo(null);//让窗口在屏幕中间显示
 	    this.setResizable(true);//让窗口大小不可改变
		getContentPane().setLayout(null);
				
		JLabel label = new JLabel("\u4FEE\u6539\u540E\u623F\u95F4\u7C7B\u578B\uFF1A");
		label.setBounds(48, 132, 129, 18);
		getContentPane().add(label);
		
		JLabel label_1 = new JLabel("\u623F\u95F4\u53F7\u7801\uFF1A");
		label_1.setBounds(89, 179, 88, 18);
		getContentPane().add(label_1);
		
		JLabel label_2 = new JLabel("\u6240\u5728\u533A\u57DF\uFF1A");
		label_2.setBounds(89, 234, 88, 18);
		getContentPane().add(label_2);
		
		JLabel label_3 = new JLabel("\u623F\u95F4\u7535\u8BDD\uFF1A");
		label_3.setBounds(89, 288, 88, 18);
		getContentPane().add(label_3);
		
		textField_1_fjhm = new JTextField();//房间号码
		textField_1_fjhm.setBounds(209, 176, 113, 24);
		getContentPane().add(textField_1_fjhm);
		textField_1_fjhm.setColumns(10);
		
		textField_2_szqy = new JTextField();//所在区域
		textField_2_szqy.setBounds(209, 231, 113, 24);
		getContentPane().add(textField_2_szqy);
		textField_2_szqy.setColumns(10);
		
		textField_3_fjdh = new JTextField();//房间电话
		textField_3_fjdh.setBounds(209, 285, 113, 24);
		getContentPane().add(textField_3_fjdh);
		textField_3_fjdh.setColumns(10);
		
		
		textField_yfjlx = new JTextField();//原房间类型文本框
		textField_yfjlx.setBounds(209, 78, 145, 24);
		getContentPane().add(textField_yfjlx);
		textField_yfjlx.setColumns(10);
		
		JComboBox comboBox = new JComboBox();//下拉框选项
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"1.\u6807\u51C6\u5355\u4EBA\u95F4", "2.\u8C6A\u534E\u5355\u4EBA\u95F4", "3.\u6807\u51C6\u53CC\u4EBA\u95F4", "4.\u8C6A\u534E\u53CC\u4EBA\u95F4", "5.\u5546\u52A1\u5957\u623F"}));
		comboBox.setBounds(209, 129, 113, 24);
		getContentPane().add(comboBox);
		
		JLabel label_4 = new JLabel("\u539F\u623F\u95F4\u7C7B\u578B\uFF1A");
		label_4.setBounds(76, 81, 103, 18);
		getContentPane().add(label_4);
		
		
		//将选中内容填入文本框
		textField_yfjlx.setText(roomInfo.getRoomType());
		roomtype=roomInfo.getRoomType();
		textField_1_fjhm.setText(String.valueOf(roomInfo.getRoomID()));
		textField_2_szqy.setText(roomInfo.getLocation());
		textField_3_fjdh.setText(roomInfo.getRoom_phone());
		
		JButton button = new JButton("   \u4FDD\u5B58");
		button.setIcon(new ImageIcon(this.getClass().getResource("img\\保存.gif")));
		button.setBounds(76, 353, 113, 27);
		getContentPane().add(button);//保存按钮
		//点击保存保存数据
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {				
				String roomType=comboBox.getSelectedItem().toString();//房间类型
				String roomID=textField_1_fjhm.getText();//房间号码
				String location=textField_2_szqy.getText();//所在区域
				String roomPhone=textField_3_fjdh.getText();//房间电话
				String sql="UPDATE roominfo set roomid='"+roomID+"', roomtype= substring('"+roomType+"',1,2), location = '"+location+"', room_phone= "+roomPhone+" WHERE roomtype='"+roomtype+"'";
				System.out.println(sql);
				int updata=up.addData(sql);
			}
		});
		
		
		
		JButton button_1 = new JButton("   \u53D6\u6D88");
		button_1.setIcon(new ImageIcon(this.getClass().getResource("img\\cancel.gif")));
		button_1.setBounds(280, 353, 113, 27);
		getContentPane().add(button_1);
		button_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				dispose();//点击取消关闭窗口
			}
		});
		
	
	
	}
}
