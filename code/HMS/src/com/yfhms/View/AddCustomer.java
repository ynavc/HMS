package com.yfhms.View;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import com.yfhms.Controller.Updata;

import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AddCustomer extends JFrame {
	private JTextField textField_1_khlx;
	private JTextField textField_2_dzbl;
	Updata up=new Updata();
	
	public AddCustomer() {
		super.setTitle("客户类型");
		this.setBounds(0, 0, 500, 492);//设置大小
		this.setLocationRelativeTo(null);//让窗口在屏幕中间显示
 	    this.setResizable(true);//让窗口大小不可改变
		getContentPane().setLayout(null);
		
		JLabel label = new JLabel("\u65B0\u589E\u5BA2\u6237\u7C7B\u578B\u4FE1\u606F");
		label.setBounds(14, 13, 131, 18);
		getContentPane().add(label);
		
		JLabel label_2 = new JLabel("\u5BA2\u6237\u7C7B\u578B\uFF1A");
		label_2.setBounds(73, 103, 94, 18);
		getContentPane().add(label_2);
		
		JLabel label_3 = new JLabel("\u6253\u6298\u6BD4\u7387\uFF1A");
		label_3.setBounds(73, 200, 94, 18);
		getContentPane().add(label_3);
		
		textField_1_khlx = new JTextField();
		textField_1_khlx.setBounds(165, 100, 180, 24);
		getContentPane().add(textField_1_khlx);
		textField_1_khlx.setColumns(10);
		
		textField_2_dzbl = new JTextField();
		textField_2_dzbl.setBounds(165, 200, 180, 24);
		getContentPane().add(textField_2_dzbl);
		textField_2_dzbl.setColumns(10);
		
		JLabel label_4 = new JLabel("\u6CE8\uFF1A\u6B64\u6253\u6298\u6BD4\u4F8B\u4EC5\u9002\u7528\u4E8E\u5546\u54C1\u9879\u76EE\uFF01\r\n8\u4E3A\u516B\u6298\uFF0C10\u4E3A\u4E0D\u6253\u6298\r\n");
		label_4.setForeground(Color.RED);
		label_4.setBounds(49, 273, 402, 24);
		getContentPane().add(label_4);
		
		JButton button = new JButton("   \u786E\u5B9A");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		button.setIcon(new ImageIcon(this.getClass().getResource("img/保存.gif")));
		button.setBounds(73, 347, 113, 27);
		getContentPane().add(button);//保存按钮
		//用户点击保存按钮，执行保存语句
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String customerType=textField_1_khlx.getText();//类型名称
				String dzbl=textField_2_dzbl.getText();//预设单价
				String sql="INSERT customertype VALUES(null,'"+customerType+"','"+dzbl+"')";
				System.out.println(sql);
				int updata=up.addData(sql);
				if (updata>0) {
					JOptionPane.showMessageDialog(null,"添加成功");					
				} 

			}
			});
		
		
		JButton button_1 = new JButton("   \u53D6\u6D88");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		button_1.setIcon(new ImageIcon(this.getClass().getResource("img\\cancel.gif")));
		button_1.setBounds(253, 347, 113, 27);
		getContentPane().add(button_1);
		//点击关闭窗口
		button_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		
	
			}
		}
	
		
