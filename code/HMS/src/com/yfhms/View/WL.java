package com.yfhms.View;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPasswordField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class WL  extends JFrame{
	private JTextField txtHms;
	private JTextField txtRoot;
	private JTextField textField_3;
	private JTextField textField_4;
	private JPasswordField passwordField;
	public  WL() {
		   super.setTitle("网络设置");
			this.setBounds(0, 0, 432, 500);//设置大小
			this.setLocationRelativeTo(null);//让窗口在屏幕中间显示
		    this.setResizable(false);//让窗口大小不可改变
			getContentPane().setLayout(null);
			
			JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
			tabbedPane.setBounds(0, 0, 426, 515);
			getContentPane().add(tabbedPane);
			
			JPanel panel = new JPanel();
			tabbedPane.addTab("网络连接方式", null, panel, null);
			panel.setLayout(null);
			
			JLabel label = new JLabel("\u8FDE\u63A5\u65B9\u5F0F\uFF1A");
			label.setBounds(85, 46, 84, 18);
			panel.add(label);
			
			JLabel lblIp = new JLabel("IP\u5730\u5740\uFF1A");
			lblIp.setBounds(85, 88, 72, 18);
			panel.add(lblIp);
			
			JLabel label_1 = new JLabel("\u7AEF\u53E3\u53F7\uFF1A");
			label_1.setBounds(85, 131, 72, 18);
			panel.add(label_1);
			
			JLabel label_2 = new JLabel("\u8FDE\u63A5\u8D26\u53F7 \uFF1A");
			label_2.setBounds(85, 171, 83, 18);
			panel.add(label_2);
			
			JLabel label_3 = new JLabel("\u8FDE\u63A5\u5BC6\u7801\uFF1A");
			label_3.setBounds(85, 218, 84, 18);
			panel.add(label_3);
			
			JLabel label_4 = new JLabel("\u6570\u636E\u5E93\u540D\u79F0\uFF1A");
			label_4.setBounds(83, 265, 109, 18);
			panel.add(label_4);
			
			txtHms = new JTextField();
			txtHms.setText("Hms");
			txtHms.setColumns(10);
			txtHms.setBounds(178, 262, 141, 24);
			panel.add(txtHms);
			
			txtRoot = new JTextField();
			txtRoot.setText("root");
			txtRoot.setColumns(10);
			txtRoot.setBounds(178, 165, 141, 24);
			panel.add(txtRoot);
			
			textField_3 = new JTextField();
			textField_3.setText("3306");
			textField_3.setColumns(10);
			textField_3.setBounds(178, 125, 141, 24);
			panel.add(textField_3);
			
			textField_4 = new JTextField();
			textField_4.setText("127.0.0.1");
			textField_4.setColumns(10);
			textField_4.setBounds(178, 82, 141, 24);
			panel.add(textField_4);
			
			JComboBox comboBox = new JComboBox();
			comboBox.setModel(new DefaultComboBoxModel(new String[] {"JDBC\u8FDE\u63A5", "\u672C\u5730\u8FDE\u63A5"}));
			comboBox.setBounds(178, 40, 141, 24);
			panel.add(comboBox);
			
			JButton button = new JButton("     确定",new ImageIcon(this.getClass().getResource("img/modi3.gif")));
			button.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					JOptionPane.showMessageDialog(null, "添加成功！");
					
				}
			});
			button.setBounds(79, 339, 113, 27);
			panel.add(button);
			
			JButton button_1 = new JButton("    取消",new ImageIcon(this.getClass().getResource("img/cancel.gif")));
			button_1.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					dispose();
				}
			});
			button_1.setBounds(218, 339, 113, 27);
			panel.add(button_1);
			
			passwordField = new JPasswordField();
			passwordField.setBounds(178, 215, 141, 24);
			panel.add(passwordField);
			
			
			
			
			
	}
//	public static void main(String[] args) {
//		WL wl=new WL();
//		wl.setVisible(true);
//	}
}
