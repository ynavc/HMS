package com.yfhms.View;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.plaf.metal.MetalBorders.OptionDialogBorder;
import javax.swing.table.DefaultTableModel;

import com.yfhms.Bean.Checkout_info;
import com.yfhms.Bean.MainRoomTypeInfo;
import com.yfhms.Bean.R_State;
import com.yfhms.Bean.RoomInfo;
import com.yfhms.Controller.Select;
import com.yfhms.Controller.Updata;

public class MainJFrame extends JFrame {
		//设置布局管理器
		GridLayout gl=new GridLayout();
		BorderLayout  bl=new BorderLayout();
		FlowLayout fl=new FlowLayout();
		Select select = new Select();
		Updata updata = new Updata();
		int v=ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED;//水平滚动条
		int h=ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED;//垂直滚动条

		Object[][] data;//结算信息二维数组
		JTabbedPane tabbedPane;//选项卡面板
		JLabel fjgg,Label_1,Label_2,Label_3,Label_4,Label_5,Label_6,Label_7,Label_8;//房间规格、详细信息
		String setroomtype = "1";//判断房间号
		
	public MainJFrame() {
		super("艺蜂酒店管理系统");
		this.setBounds(0, 0, 1600, 900);
		this.setLocationRelativeTo(null);//让窗口在屏幕中间显示
		this.setResizable(false);//让窗口大小不可改变
		getContentPane().setLayout(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//用户单击窗口的关闭按钮时程序执行的操作
//		this.getContentPane().setBackground(Color.decode("#E7D7B7"));//设置背景颜色
		
//		ImageIcon icon=new ImageIcon(this.getClass().getResource("img/background.jpg");//加载图片
////	Image im=new Image(icon);
//		JLabel label=new JLabel(icon);//将图片放入label中
//		label.setBounds(0,0,icon.getIconWidth(),icon.getIconHeight());//设置label的大小
//		this.getLayeredPane().add(label,new Integer(Integer.MIN_VALUE));//获取窗口的第二层，将label放入
//		JPanel j=(JPanel)this.getContentPane();//获取frame的顶层容器,并设置为透明
//		j.setOpaque(false);
		
		//1.创建菜单工具栏
		JMenuBar jmb = new JMenuBar();
		//2.创建一级菜单JMenu
		JMenu guestRegistration=new JMenu("来宾登记(B)");
		guestRegistration.setMnemonic('B');//设置快捷方式
		//3.创建【来宾登记】的二级菜单
		JMenuItem B_individual_item=new JMenuItem("散客开单(G)");
		B_individual_item.setMnemonic('G');
		guestRegistration.add(B_individual_item);//4、将菜单项加入文件菜单条中
		B_individual_item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "此功能暂未开放！");
			}
		});
		
		JMenuItem B_group_item=new JMenuItem("团体开单(M)");
		B_group_item.setMnemonic('M');
		guestRegistration.add(B_group_item);
		B_group_item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "此功能暂未开放！");
			}
		});
		
		JMenuItem B_renew_item=new JMenuItem("宾客续住(Z)");
		B_renew_item.setMnemonic('Z');
		guestRegistration.add(B_renew_item);
		B_renew_item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "此功能暂未开放！");
			}
		});
		
		JMenuItem B_change_item=new JMenuItem("更换房间(A)");
		B_change_item.setMnemonic('A');
		guestRegistration.add(B_change_item);
		B_change_item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "此功能暂未开放！");
			}
		});
		
		JMenuItem B_alter_item=new JMenuItem("修改登记(J)");
		B_alter_item.setMnemonic('J');
		guestRegistration.add(B_alter_item);
		B_alter_item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "此功能暂未开放！");
			}
		});
		
		JMenuItem B_state_item=new JMenuItem("房间状态(Z)");
		B_state_item.setMnemonic('Z');
		guestRegistration.add(B_state_item);
		B_state_item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "此功能暂未开放！");
			}
		});
		
		JMenuItem B_reserve_item=new JMenuItem("预订管理(T)");
		B_reserve_item.setMnemonic('T');
		guestRegistration.add(B_reserve_item);
		B_reserve_item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "此功能暂未开放！");
			}
		});
		
		JMenuItem B_remind_item=new JMenuItem("电子提醒(L)");
		B_remind_item.setMnemonic('L');
		guestRegistration.add(B_remind_item);
		B_remind_item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "此功能暂未开放！");
			}
		});
		
		JMenuItem B_exit_item=new JMenuItem("退出系统(X)");
		B_exit_item.setMnemonic('X');
		guestRegistration.add(B_exit_item);
		jmb.add(guestRegistration);//5、将菜单栏加入文件菜单条中
		B_exit_item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int result = JOptionPane.showConfirmDialog(null,"您现在要关闭系统吗?","退出提示",0,1);
				if(result == JOptionPane.OK_OPTION){
					int i = updata.addData("UPDATE login_status SET state='未登录' WHERE s_id=1;");
					JOptionPane.showMessageDialog(null, "已退出系统，欢迎下次使用！");
                    System.exit(0);
                }
			}
		});
		
		
		JMenu editor=new JMenu("收银结算(S)");
		editor.setMnemonic('S');
		
		JMenuItem S_checkOut_item=new JMenuItem("宾客结账(J)");
		S_checkOut_item.setMnemonic('J');
		editor.add(S_checkOut_item);
		S_checkOut_item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "此功能暂未开放！");
			}
		});
		
		JMenuItem S_Merg_item=new JMenuItem("合并账单(E)");
		S_Merg_item.setMnemonic('E');
		editor.add(S_Merg_item);
		S_Merg_item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "此功能暂未开放！");
			}
		});
		
		JMenuItem S_split_item=new JMenuItem("拆分账单(F)");
		S_split_item.setMnemonic('F');
		editor.add(S_split_item);
		jmb.add(editor);//5、将菜单栏加入文件菜单条中
		S_split_item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "此功能暂未开放！");
			}
		});
		
		JMenu maintain=new JMenu("系统维护(W)");
		maintain.setMnemonic('W');
		JMenuItem W_networkSet_item=new JMenuItem("网络设置(N)");
		W_networkSet_item.setMnemonic('N');
		maintain.add(W_networkSet_item);
		W_networkSet_item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "此功能暂未开放！");
			}
		});
		
		JMenuItem W_sysSet_item=new JMenuItem("系统设置(X)");
		W_sysSet_item.setMnemonic('X');
		maintain.add(W_sysSet_item);
		W_sysSet_item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "此功能暂未开放！");
			}
		});
		
		JMenuItem W_sysLog_item=new JMenuItem("系统日志(Z)");
		W_sysLog_item.setMnemonic('Z');
		maintain.add(W_sysLog_item);
		W_sysLog_item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "此功能暂未开放！");
			}
		});
		
		JMenuItem W_dataBackup_item=new JMenuItem("数据备份(R)");
		W_dataBackup_item.setMnemonic('R');
		maintain.add(W_dataBackup_item);
		W_dataBackup_item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "此功能暂未开放！");
			}
		});
		
		JMenuItem W_help_item=new JMenuItem("软件帮助(H)");
		W_help_item.setMnemonic('H');
		maintain.add(W_help_item);
		W_help_item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "此功能暂未开放！");
			}
		});
		
		JMenuItem W_about_item=new JMenuItem("关于我们(A)");
		W_about_item.setMnemonic('A');
		maintain.add(W_about_item);
		jmb.add(maintain);//5、将菜单栏加入文件菜单条中
		W_about_item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "此功能暂未开放！");
			}
		});
		
		//6.将菜单工具栏加入JFrame中
		this.setJMenuBar(jmb);
		
		
//		JPanel jPanel1 = new JPanel();
//		jPanel1.setLayout(bl);
//		jPanel1.setSize(1600, 100);
		//将button放置于jPaenl1中
		
		//创建图片ImageIcon对象
		ImageIcon icon1=new ImageIcon(this.getClass().getResource("img/icon/散客开单.png"));
		icon1.setImage(icon1.getImage().getScaledInstance(66,66,Image.SCALE_DEFAULT));
		JButton skkd = new JButton("散客开单",icon1);
		skkd.setBounds(10, 10, 90, 100);
		skkd.setContentAreaFilled(false);//设置按钮透明背景
		skkd.setFocusPainted(false);//去掉按钮周围的焦点框
//		sk.setBorderPainted(false);//去边框
		skkd.setHorizontalTextPosition(SwingConstants.CENTER);
		skkd.setVerticalTextPosition(SwingConstants.BOTTOM);
		getContentPane().add(skkd);
		skkd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				IndividualOrdersAdd individualOrdersAdd = new IndividualOrdersAdd();
				individualOrdersAdd.setVisible(true);
			}
		});
		ImageIcon icon2=new ImageIcon(this.getClass().getResource("img/icon/团体开单.png"));
		icon2.setImage(icon2.getImage().getScaledInstance(66,66,Image.SCALE_DEFAULT));
		JButton ttkd = new JButton("团体开单",icon2);
		ttkd.setBounds(115, 10, 90, 100);
		ttkd.setContentAreaFilled(false);
		ttkd.setFocusPainted(false);
		ttkd.setHorizontalTextPosition(SwingConstants.CENTER);
		ttkd.setVerticalTextPosition(SwingConstants.BOTTOM);
		getContentPane().add(ttkd);
		ttkd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				GroupOrdersAdd groupOrdersAdd=new GroupOrdersAdd();
				groupOrdersAdd.setVisible(true);
			}
		});
		ImageIcon icon3=new ImageIcon(this.getClass().getResource("img/icon/宾客结账.png"));
		icon3.setImage(icon3.getImage().getScaledInstance(66,66,Image.SCALE_DEFAULT));
		JButton bkjz = new JButton("宾客结账",icon3);
		bkjz.setBounds(220, 10, 90, 100);
		bkjz.setContentAreaFilled(false);
		bkjz.setFocusPainted(false);
		bkjz.setHorizontalTextPosition(SwingConstants.CENTER);
		bkjz.setVerticalTextPosition(SwingConstants.BOTTOM);
		getContentPane().add(bkjz);
		bkjz.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (setroomtype=="1") {
					JOptionPane.showMessageDialog(null, "请选择要结算的房间！");
				} else {
					String rjzt = select.getString("SELECT r_state.state FROM r_state,roominfo WHERE roominfo.state=number AND roomid='"+setroomtype+"'");
					if (rjzt.equals("使用中")) {
						Checkout_info che = select.getCheckoutInfo1(setroomtype);
						System.out.println(che.getGuest_name());
						Checkout checkout=new Checkout(che);
						checkout.setVisible(true);
					} else {
						JOptionPane.showMessageDialog(null, "您选择的房间暂未被使用！");
					}
				}
			}
		});
		ImageIcon icon4=new ImageIcon(this.getClass().getResource("img/icon/客房预订.png"));
		icon4.setImage(icon4.getImage().getScaledInstance(66,66,Image.SCALE_DEFAULT));
		JButton kfyd = new JButton("客房预订",icon4);
		kfyd.setBounds(325, 10, 90, 100);
		kfyd.setContentAreaFilled(false);
		kfyd.setFocusPainted(false);
		kfyd.setHorizontalTextPosition(SwingConstants.CENTER);
		kfyd.setVerticalTextPosition(SwingConstants.BOTTOM);
		getContentPane().add(kfyd);
		kfyd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Custom_type custom_type=new Custom_type();
				custom_type.setVisible(true);
			}
		});
		ImageIcon icon5=new ImageIcon(this.getClass().getResource("img/icon/营业查询.png"));
		icon5.setImage(icon5.getImage().getScaledInstance(66,66,Image.SCALE_DEFAULT));
		JButton yycx = new JButton("营业查询",icon5);
		yycx.setBounds(430, 10, 90, 100);
		yycx.setContentAreaFilled(false);
		yycx.setFocusPainted(false);
		yycx.setHorizontalTextPosition(SwingConstants.CENTER);
		yycx.setVerticalTextPosition(SwingConstants.BOTTOM);
		getContentPane().add(yycx);
		yycx.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Business business=new Business();
				business.setVisible(true);
			}
		});
		ImageIcon icon6=new ImageIcon(this.getClass().getResource("img/icon/客户管理.png"));
		icon6.setImage(icon6.getImage().getScaledInstance(66,66,Image.SCALE_DEFAULT));
		JButton khgl = new JButton("客户管理",icon6);
		khgl.setBounds(535, 10, 90, 100);
		khgl.setContentAreaFilled(false);
		khgl.setFocusPainted(false);
		khgl.setHorizontalTextPosition(SwingConstants.CENTER);
		khgl.setVerticalTextPosition(SwingConstants.BOTTOM);
		getContentPane().add(khgl);
		khgl.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ClientManagement clientManagement=new ClientManagement();
				clientManagement.setVisible(true);
			}
		});
		ImageIcon icon7=new ImageIcon(this.getClass().getResource("img/icon/网络设置.png"));
		icon7.setImage(icon7.getImage().getScaledInstance(66,66,Image.SCALE_DEFAULT));
		JButton wlsz = new JButton("网络设置",icon7);
		wlsz.setBounds(640, 10, 90, 100);
		wlsz.setContentAreaFilled(false);
		wlsz.setFocusPainted(false);
		wlsz.setHorizontalTextPosition(SwingConstants.CENTER);
		wlsz.setVerticalTextPosition(SwingConstants.BOTTOM);
		getContentPane().add(wlsz);
		wlsz.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				WL wl = new WL();
				wl.setVisible(true);
			}
		});
		ImageIcon icon8=new ImageIcon(this.getClass().getResource("img/icon/系统设置.png"));
		icon8.setImage(icon8.getImage().getScaledInstance(66,66,Image.SCALE_DEFAULT));
		JButton xtsz = new JButton("系统设置",icon8);
		xtsz.setBounds(745, 10, 90, 100);
		xtsz.setContentAreaFilled(false);
		xtsz.setFocusPainted(false);
		xtsz.setHorizontalTextPosition(SwingConstants.CENTER);
		xtsz.setVerticalTextPosition(SwingConstants.BOTTOM);
		getContentPane().add(xtsz);
		xtsz.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Operator operator=new Operator();
				operator.setVisible(true);
			}
		});
		ImageIcon icon9=new ImageIcon(this.getClass().getResource("img/icon/关于我们.png"));
		icon9.setImage(icon9.getImage().getScaledInstance(66,66,Image.SCALE_DEFAULT));
		JButton gywm = new JButton("关于我们",icon9);
		gywm.setBounds(850, 10, 90, 100);
		gywm.setContentAreaFilled(false);
		gywm.setFocusPainted(false);
		gywm.setHorizontalTextPosition(SwingConstants.CENTER);
		gywm.setVerticalTextPosition(SwingConstants.BOTTOM);
		getContentPane().add(gywm);
		gywm.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				AboutUs aboutUs = new AboutUs();
				aboutUs.setVisible(true);
			}
		});
		ImageIcon icon10=new ImageIcon(this.getClass().getResource("img/icon/退出系统.png"));
		icon10.setImage(icon10.getImage().getScaledInstance(66,66,Image.SCALE_DEFAULT));
		JButton tcxt = new JButton("退出系统",icon10);
		tcxt.setBounds(955, 10, 90, 100);
		tcxt.setContentAreaFilled(false);
		tcxt.setFocusPainted(false);
		tcxt.setHorizontalTextPosition(SwingConstants.CENTER);
		tcxt.setVerticalTextPosition(SwingConstants.BOTTOM);
		getContentPane().add(tcxt);
		tcxt.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int result = JOptionPane.showConfirmDialog(null,"您现在要关闭系统吗?","退出提示",0,1);
				if(result == JOptionPane.OK_OPTION){
					int i = updata.addData("UPDATE login_status SET state='未登录' WHERE s_id=1;");
					JOptionPane.showMessageDialog(null, "已退出系统，欢迎下次使用！");
                    System.exit(0);
                }
			}
		});
		
		ImageIcon logoimg = new ImageIcon(this.getClass().getResource("img/logo.png"));
		logoimg.setImage(logoimg.getImage().getScaledInstance(130,130,Image.SCALE_DEFAULT));
		JLabel logo = new JLabel(logoimg);
		logo.setBounds(1400, 10, 130, 130);
		getContentPane().add(logo);
//		gl.setColumns(1);//设置列数
//		gl.setHgap(1);//设置垂直间距
//		gl.setRows(17);//设置行数
//		gl.setVgap(1);//设置水平间距
//		jPanel_L.setLayout(gl);
		
		JLabel time = new JLabel();
		time.setForeground(Color.decode("#7784BD"));
		time.setBounds(10, 120, 170, 50);
		time.setFont(new Font("微软雅黑", Font.BOLD, 15));
		getContentPane().add(time);
		this.setTimer(time);
		
		Font font1 = new Font("微软雅黑",Font.BOLD,17);
		Font font2 = new Font("微软雅黑",Font.BOLD,15);
		
		fjgg = new JLabel("标准单人间 ：");
		fjgg.setFont(font1);//设置字体
		fjgg.setForeground(Color.red);//设置字体颜色
		fjgg.setBounds(20, 165, 118, 30);
		getContentPane().add(fjgg);
		int i = 1;
		JLabel bkxm = new JLabel("宾客姓名 ：");
		bkxm.setBounds(20, 200, 83, 20);
		getContentPane().add(bkxm);
		JLabel ysdj = new JLabel("预设单价 ：");
		ysdj.setBounds(20, 225, 83, 20);
		getContentPane().add(ysdj);
		JLabel fjdh = new JLabel("房间电话 ：");
		fjdh.setBounds(20, 250, 83, 20);
		getContentPane().add(fjdh);
		JLabel szqy = new JLabel("所在区域 ：");
		szqy.setBounds(20, 275, 83, 20);
		getContentPane().add(szqy);
		JLabel jdsj = new JLabel("进店时间 ：");
		jdsj.setBounds(20, 300, 83, 20);
		getContentPane().add(jdsj);
		JLabel yysj = new JLabel("已用时间 ：");
		yysj.setBounds(20, 325, 83, 20);
		getContentPane().add(yysj);
		JLabel yjyj = new JLabel("已交押金 ：");
		yjyj.setBounds(20, 350, 83, 20);
		getContentPane().add(yjyj);
		JLabel ysje = new JLabel("");//应收金额 ：
		ysje.setBounds(20, 375, 83, 20);
		getContentPane().add(ysje);
		
		JLabel fjzzt = new JLabel("房间总状态 ：");
		fjzzt.setFont(font1);//设置字体
		fjzzt.setForeground(Color.red);//设置字体颜色
		fjzzt.setBounds(20, 385, 130, 50);
		getContentPane().add(fjzzt);
		JLabel fjzs = new JLabel("房间总数 ：");
		fjzs.setBounds(20, 430, 130, 30);
		getContentPane().add(fjzs);
		String fjzsSql = "SELECT COUNT(*) FROM r_state,roominfo WHERE r_state.number=roominfo.state;";
		JLabel fjzs_t = new JLabel(select.getString(fjzsSql));
		fjzs_t.setForeground(Color.BLUE);
		fjzs_t.setFont(new Font("宋体", Font.BOLD, 18));
		fjzs_t.setBounds(97, 436, 72, 18);
		getContentPane().add(fjzs_t);
		
		JLabel dqzy = new JLabel("当前占用 ：");
		dqzy.setBounds(20, 448, 130, 40);
		getContentPane().add(dqzy);
		String dqzySql = "SELECT COUNT(*) FROM r_state,roominfo WHERE r_state.number=roominfo.state AND r_state.state='使用中';";
		JLabel dqzy_t = new JLabel(select.getString(dqzySql));
		dqzy_t.setFont(new Font("宋体", Font.BOLD, 18));
		dqzy_t.setForeground(Color.BLUE);
		dqzy_t.setBounds(97, 459, 72, 18);
		getContentPane().add(dqzy_t);
		
		JLabel dqkg = new JLabel("当前可供 ：");
		dqkg.setBounds(20, 467, 130, 50);
		getContentPane().add(dqkg);
		String dqkgSql = "SELECT COUNT(*) FROM r_state,roominfo WHERE r_state.number=roominfo.state AND r_state.state='可用';";
		JLabel dqkg_t = new JLabel(select.getString(dqkgSql));
		dqkg_t.setFont(new Font("宋体", Font.BOLD, 18));
		dqkg_t.setForeground(Color.BLUE);
		dqkg_t.setBounds(97, 483, 72, 18);
		getContentPane().add(dqkg_t);
		
		JLabel dqyd = new JLabel("当前预订 ：");
		dqyd.setBounds(20, 487, 130, 60);
		getContentPane().add(dqyd);
		String dqydSql = "SELECT COUNT(*) FROM r_state,roominfo WHERE r_state.number=roominfo.state AND r_state.state='被预订';";
		JLabel dqyd_t = new JLabel(select.getString(dqydSql));
		dqyd_t.setFont(new Font("宋体", Font.BOLD, 18));
		dqyd_t.setForeground(Color.BLUE);
		dqyd_t.setBounds(97, 508, 72, 18);
		getContentPane().add(dqyd_t);
		
		JLabel dqty = new JLabel("当前停用 ：");
		dqty.setBounds(20, 508, 130, 70);
		getContentPane().add(dqty);
		String dqtySql = "SELECT COUNT(*) FROM r_state,roominfo WHERE r_state.number=roominfo.state AND r_state.state='维护中';";
		JLabel dqty_t = new JLabel(select.getString(dqtySql));
		dqty_t.setFont(new Font("宋体", Font.BOLD, 18));
		dqty_t.setForeground(Color.BLUE);
		dqty_t.setBounds(97, 534, 72, 18);
		getContentPane().add(dqty_t);
		
		JPanel logoJPanel = new JPanel();
		
		/**=======================================================================**
		 *			中间的组件模块
		 **=======================================================================**
		 */
		roomTypeInfo();//调用组件模块你的方法
		
		//按钮
		ImageIcon i1 = new ImageIcon(this.getClass().getResource("img/browse.gif"));
		JButton xsqb = new JButton("   显示全部",i1);
		xsqb.setBounds(1150, 560, 110, 30);
		xsqb.setFocusPainted(false);//去掉按钮周围的焦点框
		xsqb.setContentAreaFilled(false);//设置按钮透明背景
		getContentPane().add(xsqb);
		xsqb.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "此功能暂未开放！");
			}
		});
		
		ImageIcon i2 = new ImageIcon(this.getClass().getResource("img/choose.gif"));
		JButton glzt = new JButton("   过滤状态",i2);
		glzt.setBounds(1280, 560, 110, 30);
		glzt.setFocusPainted(false);//去掉按钮周围的焦点框
		glzt.setContentAreaFilled(false);//设置按钮透明背景
		getContentPane().add(glzt);
		glzt.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "此功能暂未开放！");
			}
		});
		
		ImageIcon i3 = new ImageIcon(this.getClass().getResource("img/refurbish.gif"));
		JButton sx = new JButton("   刷      新",i3);
		sx.setBounds(1410, 560, 110, 30);
		sx.setFocusPainted(false);//去掉按钮周围的焦点框
		sx.setContentAreaFilled(false);//设置按钮透明背景
		getContentPane().add(sx);
		sx.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				MainJFrame m = new MainJFrame();
				dispose();
				m.setVisible(true);
				JOptionPane.showMessageDialog(null, "刷新成功！");
			}
		});
		Object[] columnNames = {"入住单号","主房间号","标准单价","客房类型","享受折扣","消费金额","消费时间","记账人"};
		data = select.getAllCheckoutInfo();//二维数组
		DefaultTableModel df = new DefaultTableModel(data, columnNames);
		JTable jTable=new JTable(df);
		jTable.getTableHeader().setFont(new Font(null, Font.BOLD, 14));  // 设置表头名称字体样式
//		jTable.getTableHeader().setForeground(Color.RED);                // 设置表头名称字体颜色
		jTable.getTableHeader().setResizingAllowed(false);               // 设置不允许手动改变列宽
		jTable.getTableHeader().setReorderingAllowed(false);
		JScrollPane jsp=new JScrollPane(jTable,v,h);//创建滚动容器
		jsp.setBackground(Color.decode("#E7D7B7"));//设置背景颜色
		jsp.setBounds(24, 603, 1546, 204);
		getContentPane().add(jsp);
		
		JLabel dqsj = new JLabel("当前时间 ：");
		dqsj.setForeground(Color.decode("#7784BD"));
		dqsj.setBounds(1110, 25, 200, 50);
		dqsj.setFont(new Font("微软雅黑", Font.BOLD, 25));
		getContentPane().add(dqsj);
		getContentPane().add(dqsj);
		
		JLabel time1 = new JLabel();
		time1.setForeground(Color.decode("#7784BD"));
		time1.setBounds(1110, 40, 407, 115);
		time1.setFont(new Font("宋体", Font.CENTER_BASELINE, 20));
		getContentPane().add(time1);
		this.setTimer(time1);
		
		Label_1 = new JLabel();//"宾客姓名"
		Label_1.setForeground(Color.BLUE);
		Label_1.setFont(new Font("宋体", Font.BOLD, 15));
		Label_1.setBounds(90, 201, 130, 18);
		getContentPane().add(Label_1);
		Label_2 = new JLabel();//"预设单价"
		Label_2.setForeground(Color.BLUE);
		Label_2.setFont(new Font("宋体", Font.BOLD, 15));
		Label_2.setBounds(90, 226, 130, 18);
		getContentPane().add(Label_2);
		Label_3 = new JLabel();//"房间电话"
		Label_3.setForeground(Color.BLUE);
		Label_3.setFont(new Font("宋体", Font.BOLD, 15));
		Label_3.setBounds(90, 251, 130, 18);
		getContentPane().add(Label_3);
		Label_4 = new JLabel();//"所在区域"
		Label_4.setForeground(Color.BLUE);
		Label_4.setFont(new Font("宋体", Font.BOLD, 15));
		Label_4.setBounds(90, 276, 130, 18);
		getContentPane().add(Label_4);
		Label_5 = new JLabel();//"进店时间"
		Label_5.setForeground(Color.BLUE);
		Label_5.setFont(new Font("宋体", Font.BOLD, 15));
		Label_5.setBounds(90, 301, 130, 18);
		getContentPane().add(Label_5);
		Label_6 = new JLabel();//"已用时间"
		Label_6.setForeground(Color.BLUE);
		Label_6.setFont(new Font("宋体", Font.BOLD, 15));
		Label_6.setBounds(90, 326, 130, 18);
		getContentPane().add(Label_6);
		Label_7 = new JLabel();//"已交押金"
		Label_7.setForeground(Color.BLUE);
		Label_7.setFont(new Font("宋体", Font.BOLD, 15));
		Label_7.setBounds(90, 351, 130, 18);
		getContentPane().add(Label_7);
		Label_8 = new JLabel();//"应收金额"
		Label_8.setForeground(Color.BLUE);
		Label_8.setFont(new Font("宋体", Font.BOLD, 15));
		Label_8.setBounds(90, 376, 130, 18);
		getContentPane().add(Label_8);
	}
	//界面中间房间状态的方法
	public void roomTypeInfo() {
		
		/**=======================================================================**
		 *			选项卡面板
		 **=======================================================================**
		 */
		tabbedPane=new JTabbedPane();
		tabbedPane.setBounds(220, 130, 1350, 420);
		getContentPane().add(tabbedPane);
		tabbedPane.setBackground(Color.decode("#DBC9ED"));
		ImageIcon ic = new ImageIcon(this.getClass().getResource("img/room/prov.gif"));//图片路径
		/**=======================================================================**
		 *			【标准单人间】  -  面板组件容器p1
		 **=======================================================================**
		 */
		JPanel p1 = new JPanel() ;  //创建面板组件容器
		p1.setLayout(new FlowLayout(FlowLayout.LEFT, 50, 50));//流式布局间距50
		String p1SQL = "SELECT * FROM roomtype WHERE r_type='标准单人间'";
		String p1roomtype = select.getString(p1SQL);
		
		ArrayList<RoomInfo> list_p1 = select.getMainInfoRoom(p1roomtype);//得到房间号和房间状态存入List集合
		for(int j=0;j<list_p1.size();j++){
			//可用的房间状态
			if(list_p1.get(j).getState()==1){
				ic=new ImageIcon(this.getClass().getResource("img/room/prov.gif"));
			//被预订的房间状态
			}else if(list_p1.get(j).getState()==3){
				ic=new ImageIcon(this.getClass().getResource("img/room/stop.gif"));
			//维护中的房间状态
			}else if(list_p1.get(j).getState()==2){
				ic=new ImageIcon(this.getClass().getResource("img/room/rese.gif"));
			//使用中的房间状态
			}else {
				ic=new ImageIcon(this.getClass().getResource("img/room/pree.gif"));
			}
			JButton btnNewButton = new JButton(""+list_p1.get(j).getRoomID(),ic);
			btnNewButton.setHorizontalTextPosition(SwingConstants.CENTER);
			btnNewButton.setVerticalTextPosition(SwingConstants.BOTTOM);
			btnNewButton.setContentAreaFilled(false);//设置按钮透明背景
			btnNewButton.setFocusPainted(false);
			//按钮监听事件
			btnNewButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					setroomtype = e.getActionCommand()+"";
					setRoomIn(setroomtype);
					System.out.println(setroomtype);
					
				}
			});
			btnNewButton.setPreferredSize(new Dimension(80,100));
			p1.add(btnNewButton);
		}
		tabbedPane.addTab("标准单人间", p1);//将p1面板添加进总面板控件
		
		/**=======================================================================**
		 *			【标准双人间】  -  面板组件容器p2
		 **=======================================================================**
		 */
		JPanel p2=new JPanel() ;
		p2.setLayout(new FlowLayout(FlowLayout.LEFT, 50, 50));//流式布局间距50
		String p2SQL = "SELECT * FROM roomtype WHERE r_type='标准双人间'";
		String p2roomtype = select.getString(p2SQL);
		ArrayList<RoomInfo> list_p2 = select.getMainInfoRoom(p2roomtype);//得到房间号和房间状态存入List集合
		for(int j=0;j<list_p2.size();j++){
			//可用的房间状态
			if(list_p2.get(j).getState()==1){
				ic=new ImageIcon(this.getClass().getResource("img/room/prov.gif"));
			//被预订的房间状态
			}else if(list_p2.get(j).getState()==3){
				ic=new ImageIcon(this.getClass().getResource("img/room/stop.gif"));
			//维护中的房间状态
			}else if(list_p2.get(j).getState()==2){
				ic=new ImageIcon(this.getClass().getResource("img/room/rese.gif"));
			//使用中的房间状态
			}else {
				ic=new ImageIcon(this.getClass().getResource("img/room/pree.gif"));
			}
			JButton btnNewButton = new JButton(""+list_p2.get(j).getRoomID(),ic);
			btnNewButton.setHorizontalTextPosition(SwingConstants.CENTER);
			btnNewButton.setVerticalTextPosition(SwingConstants.BOTTOM);
			btnNewButton.setContentAreaFilled(false);//设置按钮透明背景
			btnNewButton.setFocusPainted(false);
			//按钮监听事件
			btnNewButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					setroomtype = e.getActionCommand()+"";
					setRoomIn(setroomtype);
					System.out.println(setroomtype);
				}
			});
			btnNewButton.setPreferredSize(new Dimension(80,100));
			p2.add(btnNewButton);
		}
		tabbedPane.addTab("标准双人间", p2);//将p2面板添加进总面板控件
		
		/**=======================================================================**
		 *			【豪华单人间】  -  面板组件容器p3
		 **=======================================================================**
		 */
		JPanel p3=new JPanel() ;
		p3.setLayout(new FlowLayout(FlowLayout.LEFT, 50, 50));//流式布局间距50
		String p3SQL = "SELECT * FROM roomtype WHERE r_type='豪华单人间'";
		String p3roomtype = select.getString(p3SQL);
		ArrayList<RoomInfo> list_p3 = select.getMainInfoRoom(p3roomtype);//得到房间号和房间状态存入List集合
		for(int j=0;j<list_p3.size();j++){
			//可用的房间状态
			if(list_p3.get(j).getState()==1){
				ic=new ImageIcon(this.getClass().getResource("img/room/prov.gif"));
			//被预订的房间状态
			}else if(list_p3.get(j).getState()==3){
				ic=new ImageIcon(this.getClass().getResource("img/room/stop.gif"));
			//维护中的房间状态
			}else if(list_p3.get(j).getState()==2){
				ic=new ImageIcon(this.getClass().getResource("img/room/rese.gif"));
			//使用中的房间状态
			}else {
				ic=new ImageIcon(this.getClass().getResource("img/room/pree.gif"));
			}
			JButton btnNewButton = new JButton(""+list_p3.get(j).getRoomID(),ic);
			btnNewButton.setHorizontalTextPosition(SwingConstants.CENTER);
			btnNewButton.setVerticalTextPosition(SwingConstants.BOTTOM);
			btnNewButton.setContentAreaFilled(false);//设置按钮透明背景
			btnNewButton.setFocusPainted(false);//取消按钮焦点
			//按钮监听事件
			btnNewButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					setroomtype = e.getActionCommand()+"";
					setRoomIn(setroomtype);
				}
			});
			btnNewButton.setPreferredSize(new Dimension(80,100));
			p3.add(btnNewButton);
		}
		tabbedPane.addTab("豪华单人间", p3);//将p3面板添加进总面板控件
		
		/**=======================================================================**
		 *			【豪华双人间】  -  面板组件容器p4
		 **=======================================================================**
		 */
		JPanel p4=new JPanel() ;
		p4.setLayout(new FlowLayout(FlowLayout.LEFT, 50, 50));//流式布局间距50
		String p4SQL = "SELECT * FROM roomtype WHERE r_type='豪华双人间'";
		String p4roomtype = select.getString(p4SQL);
		ArrayList<RoomInfo> list_p4 = select.getMainInfoRoom(p4roomtype);//得到房间号和房间状态存入List集合
		for(int j=0;j<list_p4.size();j++){
			//可用的房间状态
			if(list_p4.get(j).getState()==1){
				ic=new ImageIcon(this.getClass().getResource("img/room/prov.gif"));
			//被预订的房间状态
			}else if(list_p4.get(j).getState()==3){
				ic=new ImageIcon(this.getClass().getResource("img/room/stop.gif"));
			//维护中的房间状态
			}else if(list_p4.get(j).getState()==2){
				ic=new ImageIcon(this.getClass().getResource("img/room/rese.gif"));
			//使用中的房间状态
			}else {
				ic=new ImageIcon(this.getClass().getResource("img/room/pree.gif"));
			}
			JButton btnNewButton = new JButton(""+list_p4.get(j).getRoomID(),ic);
			btnNewButton.setHorizontalTextPosition(SwingConstants.CENTER);
			btnNewButton.setVerticalTextPosition(SwingConstants.BOTTOM);
			btnNewButton.setContentAreaFilled(false);//设置按钮透明背景
			btnNewButton.setFocusPainted(false);
			//按钮监听事件
			btnNewButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					setroomtype = e.getActionCommand()+"";
					setRoomIn(setroomtype);
					System.out.println(setroomtype);
				}
			});
			btnNewButton.setPreferredSize(new Dimension(80,100));
			p4.add(btnNewButton);
		}
		tabbedPane.addTab("豪华双人间", p4);//将p4面板添加进总面板控件
		
		/**=======================================================================**
		 *			【商务套房】  -  面板组件容器p5
		 **=======================================================================**
		 */
		JPanel p5=new JPanel() ;
		p5.setLayout(new FlowLayout(FlowLayout.LEFT, 50, 50));//流式布局间距50
		String p5SQL = "SELECT * FROM roomtype WHERE r_type='商务套房'";
		String p5roomtype = select.getString(p5SQL);
		ArrayList<RoomInfo> list_p5 = select.getMainInfoRoom(p5roomtype);//得到房间号和房间状态存入List集合
		for(int j=0;j<list_p5.size();j++){
			//可用的房间状态
			if(list_p5.get(j).getState()==1){
				ic=new ImageIcon(this.getClass().getResource("img/room/prov.gif"));
			//被预订的房间状态
			}else if(list_p5.get(j).getState()==3){
				ic=new ImageIcon(this.getClass().getResource("img/room/stop.gif"));
			//维护中的房间状态
			}else if(list_p5.get(j).getState()==2){
				ic=new ImageIcon(this.getClass().getResource("img/room/rese.gif"));
			//使用中的房间状态
			}else {
				ic=new ImageIcon(this.getClass().getResource("img/room/pree.gif"));
			}
			JButton btnNewButton = new JButton(""+list_p5.get(j).getRoomID(),ic);
			btnNewButton.setHorizontalTextPosition(SwingConstants.CENTER);
			btnNewButton.setVerticalTextPosition(SwingConstants.BOTTOM);
			btnNewButton.setContentAreaFilled(false);//设置按钮透明背景
			btnNewButton.setFocusPainted(false);
			//按钮监听事件
			btnNewButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					setroomtype = e.getActionCommand()+"";
					setRoomIn(setroomtype);
					System.out.println(setroomtype);
				}
			});
			btnNewButton.setPreferredSize(new Dimension(80,100));
			p5.add(btnNewButton);
		}
		tabbedPane.addTab("商务套房", p5);//将p5面板添加进总面板控件
	
		/**=======================================================================**
		 *			【总统套房】  -  面板组件容器p6
		 **=======================================================================**
		 */
		JPanel p6=new JPanel() ;
		p6.setLayout(new FlowLayout(FlowLayout.LEFT, 50, 50));//流式布局间距50
		String p6SQL = "SELECT * FROM roomtype WHERE r_type='总统套房'";
		String p6roomtype = select.getString(p6SQL);
		ArrayList<RoomInfo> list_p6 = select.getMainInfoRoom(p6roomtype);//得到房间号和房间状态存入List集合
		for(int j=0;j<list_p6.size();j++){
			//可用的房间状态
			if(list_p6.get(j).getState()==1){
				ic=new ImageIcon(this.getClass().getResource("img/room/prov.gif"));
			//被预订的房间状态
			}else if(list_p6.get(j).getState()==3){
				ic=new ImageIcon(this.getClass().getResource("img/room/stop.gif"));
			//维护中的房间状态
			}else if(list_p6.get(j).getState()==2){
				ic=new ImageIcon(this.getClass().getResource("img/room/rese.gif"));
			//使用中的房间状态
			}else {
				ic=new ImageIcon(this.getClass().getResource("img/room/pree.gif"));
			}
			JButton btnNewButton = new JButton(""+list_p6.get(j).getRoomID(),ic);
			btnNewButton.setHorizontalTextPosition(SwingConstants.CENTER);
			btnNewButton.setVerticalTextPosition(SwingConstants.BOTTOM);
			btnNewButton.setContentAreaFilled(false);//设置按钮透明背景
			btnNewButton.setFocusPainted(false);
			//按钮监听事件
			btnNewButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					setroomtype = e.getActionCommand()+"";
					setRoomIn(setroomtype);
					System.out.println(setroomtype);
				}
			});
			btnNewButton.setPreferredSize(new Dimension(80,100));
			p6.add(btnNewButton);
		}
		tabbedPane.addTab("总统套房", p6);//将p6面板添加进总面板控件
	}
	//
	private void setRoomIn(String setroomtype) {
		if (setroomtype!="1") {
			String rjzt = select.getString("SELECT r_state.state FROM r_state,roominfo WHERE roominfo.state=number AND roomid='"+setroomtype+"'");
			System.out.println(rjzt);
			if (rjzt.equals("使用中")) {
				MainRoomTypeInfo m = select.getMainRoomTypeInfo(setroomtype);
				int index = tabbedPane.getSelectedIndex();//得到选项卡面板的位置
				fjgg.setText(tabbedPane.getTitleAt(index));//得到选项卡面板的值
				Label_1.setText(m.getC_name());
				Label_2.setText("¥ "+m.getPrice());
				Label_3.setText(m.getRoom_phone());
				Label_4.setText(m.getLocation());
				Label_5.setText(m.getIn_time());
				Label_6.setText(m.getElapsed_time()+"小时");
				Label_7.setText("¥ "+m.getForegift());
			} else {
				MainRoomTypeInfo m = select.getMainRoomTypeInfo1(setroomtype);
				int index = tabbedPane.getSelectedIndex();//得到选项卡面板的位置
				fjgg.setText(tabbedPane.getTitleAt(index));//得到选项卡面板的值
				Label_1.setText("");
				Label_2.setText("¥ "+m.getPrice());
				Label_3.setText(m.getRoom_phone());
				Label_4.setText(m.getLocation());
				Label_5.setText("");
				Label_6.setText("");
				Label_7.setText("");
			}
		}
		
//			Label_8.setText("¥ "+(Integer.valueOf(m.getForegift())-Integer.valueOf(m.getPrice()))+"");
		}
	
	// 设置Timer 1000ms实现一次动作 实际是一个线程
	private void setTimer(JLabel time) {
		final JLabel varTime = time;
		Timer timeAction = new Timer(100, new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				long timemillis = System.currentTimeMillis();
				// 转换日期显示格式
				SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				varTime.setText(df.format(new Date(timemillis)));
			}
		});
		timeAction.start();
	}
}
