package com.yfhms.View;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JCheckBox;
import javax.swing.JTextField;

import com.yfhms.Controller.RoomSelect;
import com.yfhms.Controller.Updata;

import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;

public class AddRoomType extends JFrame {
	Updata ud=new Updata();//导入方法
	private JTextField textField_fjlx;
	private JTextField textField_cwsl;
	private JTextField textField_ysdj;
	private JTextField textField_zdjf;
	private JTextField textField_sfjf;
	
	public AddRoomType() {
		super.setTitle("添加房间类型");
		this.setBounds(0, 0, 500, 492);//设置大小
		this.setLocationRelativeTo(null);//让窗口在屏幕中间显示
 	    this.setResizable(true);//让窗口大小不可改变
		getContentPane().setLayout(null);
				
		JLabel label = new JLabel("\u65B0\u623F\u95F4\u7C7B\u578B");
		label.setBounds(14, 13, 81, 18);
		getContentPane().add(label);
		
		JLabel label_2 = new JLabel("\u623F\u95F4\u7C7B\u578B\uFF1A");
		label_2.setBounds(104, 61, 86, 18);
		getContentPane().add(label_2);
		
		JLabel label_3 = new JLabel("\u5E8A\u4F4D\u6570\u91CF\uFF1A");
		label_3.setBounds(104, 115, 86, 18);
		getContentPane().add(label_3);
		
		JLabel label_4 = new JLabel("\u9884\u8BBE\u5355\u4EF7\uFF1A");
		label_4.setBounds(104, 168, 87, 18);
		getContentPane().add(label_4);
		
		JLabel label_6 = new JLabel("\u949F\u70B9\u8BA1\u8D39\uFF1A");
		label_6.setBounds(104, 216, 86, 18);
		getContentPane().add(label_6);
		
		textField_cwsl = new JTextField();//床位数量
		textField_cwsl.setBounds(213, 112, 186, 24);
		getContentPane().add(textField_cwsl);
		textField_cwsl.setColumns(10);
		
		textField_ysdj = new JTextField();//预设单价
		textField_ysdj.setBounds(213, 165, 186, 24);
		getContentPane().add(textField_ysdj);
		textField_ysdj.setColumns(10);
		
		textField_zdjf = new JTextField();//钟点计费
		textField_zdjf.setBounds(213, 213, 186, 24);
		getContentPane().add(textField_zdjf);
		textField_zdjf.setColumns(10);
		
		
		
		JButton button_1 = new JButton("   \u53D6\u6D88");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		
		button_1.setIcon(new ImageIcon(this.getClass().getResource("img\\cancel.gif")));
		button_1.setBounds(279, 339, 113, 27);
		getContentPane().add(button_1);
		//点击按钮关闭窗口
		button_1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		
		
		textField_fjlx = new JTextField();//房间类型
		textField_fjlx.setBounds(213, 58, 186, 24);
		getContentPane().add(textField_fjlx);
		textField_fjlx.setColumns(10);
		
		JLabel label_7 = new JLabel("\u80FD\u5426\u6309\u7167\u949F\u70B9\u8BA1\u8D39\uFF1A");
		label_7.setBounds(82, 268, 135, 18);
		getContentPane().add(label_7);
		
		textField_sfjf = new JTextField();//是否能按钟点计费
		textField_sfjf.setBounds(228, 265, 86, 24);
		getContentPane().add(textField_sfjf);
		textField_sfjf.setColumns(10);
		
		JLabel label_5 = new JLabel("");
		label_5.setBounds(118, 308, 72, 18);
		getContentPane().add(label_5);
		
		JButton button = new JButton("   \u4FDD\u5B58");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		button.setIcon(new ImageIcon(this.getClass().getResource("img\\保存.gif")));
		button.setBounds(82, 339, 113, 27);
		getContentPane().add(button);
		
		
		//添加点击事件
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String typeName=textField_fjlx.getText();//类型名称
				String price=textField_ysdj.getText();//预设单价
				String hourPrice=textField_zdjf.getText();//钟点房价格
				String bed=textField_cwsl.getText();//床位数量
				String chose=textField_sfjf.getText();//
				String sql="INSERT roomtype VALUES(null,'"+typeName+"','"+price+"','"+hourPrice+"','"+bed+"','"+chose+"')";
				System.out.println(sql);
				int updata=ud.addData(sql);
				if (updata>0) {
					JOptionPane.showMessageDialog(null,"添加成功");
					//关闭设置界面再打开
					
				}
			}
		});

		
	}
}
