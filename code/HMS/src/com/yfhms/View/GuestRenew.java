package com.yfhms.View;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

public class GuestRenew extends JFrame {
	public GuestRenew() {
		super("宾客续住");
		this.setBounds(0, 0, 500, 380);
		this.setLocationRelativeTo(null);//让窗口在屏幕中间显示
		this.setResizable(false);//让窗口大小不可改变
		getContentPane().setLayout(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//用户单击窗口的关闭按钮时程序执行的操作
//		this.getContentPane().setBackground(Color.decode("#E7D7B7"));//设置背景颜色
//		Font f = new Font("微软雅黑", Font.BOLD, 18);
		JLabel fjh = new JLabel("房  间 号 ："+"BD001");
		fjh.setBounds(120, 60, 200, 30);
//		fjh.setFont(f);
		getContentPane().add(fjh);
		JLabel bkxm = new JLabel("宾客姓名 ："+"颜权昌");
		bkxm.setBounds(120, 100, 200, 30);
//		bkxm.setFont(f);
		getContentPane().add(bkxm);
		JLabel xzts = new JLabel("续住天数 ：");
		xzts.setBounds(120, 140, 83, 30);
//		xzts.setFont(f);
		getContentPane().add(xzts);
		JTextField t1 = new JTextField();
		t1.setBounds(206, 140, 184, 30);
		getContentPane().add(t1);
		JLabel xzyj = new JLabel("续费押金 ：");
		xzyj.setBounds(120, 180, 92, 30);
//		xzyj.setFont(f);
		getContentPane().add(xzyj);
		JTextField t2 = new JTextField();
		t2.setBounds(206, 180, 184, 30);
		getContentPane().add(t2);
		ImageIcon i1 = new ImageIcon(this.getClass().getResource("img/modi3.gif"));
//		i1.setImage(i1.getImage().getScaledInstance(30,30,Image.SCALE_DEFAULT));
		JButton qd = new JButton("   确定",i1);
//		qd.setFont(f);
		qd.setBounds(113, 257, 117, 33);
//		qd.setContentAreaFilled(false);
//		qd.setFocusPainted(false);
		getContentPane().add(qd);
		ImageIcon i2 = new ImageIcon(this.getClass().getResource("img/cancel.gif"));
//		i2.setImage(i2.getImage().getScaledInstance(30,30,Image.SCALE_DEFAULT));
		JButton qx = new JButton("   取消",i2);
//		qx.setFont(f);
		qx.setBounds(260, 257, 117, 33);
//		qx.setContentAreaFilled(false);
//		qx.setFocusPainted(false);
		getContentPane().add(qx);
		qx.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
	}
}
