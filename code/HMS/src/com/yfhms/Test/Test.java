package com.yfhms.Test;

import java.awt.Font;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JButton;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import com.yfhms.Controller.Select;
import com.yfhms.DAO.DbConnection;
import com.yfhms.Utils.FontSet;
import com.yfhms.View.Business;
import com.yfhms.View.Checkout;
import com.yfhms.View.Custom_type;
import com.yfhms.View.GroupOrdersAdd;
import com.yfhms.View.GuestRenew;
import com.yfhms.View.IndividualOrdersAdd;
import com.yfhms.View.Login;
import com.yfhms.View.MainJFrame;
import com.yfhms.View.Operator;
import com.yfhms.View.WelcomeScreen;

public class Test {
	public static void main(String[] args) {
		Select select = new Select();
		try {
//			javax.swing.UIManager.setLookAndFeel("com.jtattoo.plaf.bernstein.BernsteinLookAndFeel");
			javax.swing.UIManager.setLookAndFeel("com.jtattoo.plaf.mcwin.McWinLookAndFeel");
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		
		MainJFrame m = new MainJFrame();
		Login l = new Login();
		WelcomeScreen welcomeScreen = new WelcomeScreen();
		TimerTask task = new TimerTask() {
			@Override
			public void run() {
				welcomeScreen.setVisible(false);
				String state = select.getString("SELECT state FROM login_status WHERE s_id = 1;");
				if (state.equals("已登录")) {
					m.setVisible(true);
				} else {
					l.setVisible(true);
				}
			}
		};
		Timer timer = new Timer();
		long delay = 2000;
		long intevalPeriod = 360000 * 1000;
		//安排任务以一定的时间间隔运行
		timer.scheduleAtFixedRate(task, delay, intevalPeriod);//delay时间执行run。间隔intevalPeriod再次执行

		

//		MainJFrame m = new MainJFrame();
//		IndividualOrdersAdd i = new IndividualOrdersAdd();
//		GroupOrdersAdd ga = new GroupOrdersAdd();
//		GuestRenew g = new GuestRenew();
		
//		m.setVisible(true);
//		i.setVisible(true);
//		ga.setVisible(true);
//		g.setVisible(true);
		
//		Custom_type cust=new Custom_type();
//		cust.setVisible(true);
		
//		Checkout checkout = new Checkout();
//		checkout.setVisible(true);
		
//		Business bus=new Business();
//		bus.setVisible(true);
//		
//		
//		Login login=new Login();
//		login.setVisible(true);
//		
//		Operator op=new Operator();
//		op.setVisible(true);
		
	}
}
