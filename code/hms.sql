/*
Navicat MySQL Data Transfer

Source Server         : test
Source Server Version : 50646
Source Host           : localhost:3306
Source Database       : hms

Target Server Type    : MYSQL
Target Server Version : 50646
File Encoding         : 65001

Date: 2020-07-06 23:18:38
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `checkout`
-- ----------------------------
DROP TABLE IF EXISTS `checkout`;
CREATE TABLE `checkout` (
  `chk_no` int(20) NOT NULL AUTO_INCREMENT COMMENT '结算单号',
  `in_no` int(20) DEFAULT NULL COMMENT '入住单号',
  `days` varchar(6) NOT NULL COMMENT '入住天数',
  `money` decimal(10,2) NOT NULL COMMENT '金额',
  `chk_time` datetime NOT NULL COMMENT '结算时间',
  `u_id` int(11) NOT NULL,
  `remark` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`chk_no`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of checkout
-- ----------------------------
INSERT INTO `checkout` VALUES ('1', '2', '1', '300.00', '2020-06-30 14:48:30', '2', '无备注');
INSERT INTO `checkout` VALUES ('2', '19595665', '1', '300.00', '2020-07-06 10:53:07', '1', '');
INSERT INTO `checkout` VALUES ('3', '19595666', '1', '300.00', '2020-07-06 11:22:11', '1', '');
INSERT INTO `checkout` VALUES ('4', '19595655', '5', '2400.00', '2020-07-06 11:25:02', '1', '');
INSERT INTO `checkout` VALUES ('5', '66565', '6', '1620.00', '2020-07-06 14:09:21', '1', '');
INSERT INTO `checkout` VALUES ('6', '19595666', '1', '300.00', '2020-07-06 15:09:54', '1', '');

-- ----------------------------
-- Table structure for `customerinfo`
-- ----------------------------
DROP TABLE IF EXISTS `customerinfo`;
CREATE TABLE `customerinfo` (
  `c_id` int(20) NOT NULL AUTO_INCREMENT COMMENT '客户编号',
  `c_type` int(11) NOT NULL COMMENT '客户类型编号',
  `c_name` varchar(6) NOT NULL COMMENT '客户名字',
  `c_sex` varchar(5) NOT NULL COMMENT '性别',
  `d_type` varchar(255) NOT NULL COMMENT '证件类型',
  `no` varchar(18) NOT NULL COMMENT '证件编号',
  `address` varchar(50) NOT NULL COMMENT '详细地址',
  `v_phone` varchar(11) NOT NULL COMMENT '联系电话',
  `notes` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`c_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of customerinfo
-- ----------------------------
INSERT INTO `customerinfo` VALUES ('1', '1', '岩奔', '女', '身份证', '532123198011116622', '云南省西双版纳', '13843812738', '老客户');
INSERT INTO `customerinfo` VALUES ('2', '2', '李韬', '男', '身份证', '532254199506014532', '云南省昆明市五华区派出所', '13578065412', null);
INSERT INTO `customerinfo` VALUES ('4', '3', '羊禹飞', '男', '身份证', '532123200006020056', '云南省昭通市昭阳区', '18265489547', '测试数据');
INSERT INTO `customerinfo` VALUES ('10', '1', '杨海红', '男', '身份证', '532629200012041963', '云南昆明', '13578096324', '');
INSERT INTO `customerinfo` VALUES ('11', '1', '杨海红', '男', '身份证', '532123200006020056', '云南昆明', '18214217246', '');
INSERT INTO `customerinfo` VALUES ('12', '1', '饶文丽', '男', '身份证', '532123200006025693', '云南省昆明市五华区', '13145923612', '');
INSERT INTO `customerinfo` VALUES ('13', '1', '杨明金', '男', '身份证', '532123200006020056', '云南省昭通市巧家县', '18214217244', '');

-- ----------------------------
-- Table structure for `customertype`
-- ----------------------------
DROP TABLE IF EXISTS `customertype`;
CREATE TABLE `customertype` (
  `c_id` int(20) NOT NULL COMMENT '客户类型编号',
  `c_type` varchar(20) NOT NULL COMMENT '客户类型',
  `discount` decimal(20,2) NOT NULL COMMENT '打折比例',
  PRIMARY KEY (`c_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of customertype
-- ----------------------------
INSERT INTO `customertype` VALUES ('1', '普通宾客', '1.00');
INSERT INTO `customertype` VALUES ('2', '团体宾客', '0.90');
INSERT INTO `customertype` VALUES ('3', '会员宾客', '0.80');
INSERT INTO `customertype` VALUES ('4', '协议单位', '0.70');

-- ----------------------------
-- Table structure for `livein`
-- ----------------------------
DROP TABLE IF EXISTS `livein`;
CREATE TABLE `livein` (
  `in_no` int(20) NOT NULL AUTO_INCREMENT COMMENT '入住单号',
  `roomid` int(20) DEFAULT NULL COMMENT '房间编号',
  `c_id` int(11) DEFAULT NULL COMMENT '客户编号',
  `number` int(11) NOT NULL COMMENT '人数',
  `foregift` decimal(10,2) NOT NULL COMMENT '押金',
  `days` int(11) NOT NULL COMMENT '预住天数',
  `status` int(20) DEFAULT NULL COMMENT '当前状态',
  `in_time` datetime NOT NULL COMMENT '入住时间',
  `chk_time` datetime NOT NULL COMMENT '结账时间',
  PRIMARY KEY (`in_no`)
) ENGINE=InnoDB AUTO_INCREMENT=19595669 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of livein
-- ----------------------------
INSERT INTO `livein` VALUES ('2', '103', '1', '1', '800.00', '3', '1', '2020-06-16 15:56:04', '2020-06-21 15:56:09');
INSERT INTO `livein` VALUES ('66565', '102', '2', '1', '600.00', '1', '1', '2020-06-29 14:44:03', '2020-06-30 14:44:08');
INSERT INTO `livein` VALUES ('19595655', '105', '4', '2', '500.00', '3', '1', '2020-06-30 14:42:38', '2020-07-02 14:42:43');
INSERT INTO `livein` VALUES ('19595665', '107', '10', '1', '300.00', '1', '1', '2020-07-06 10:52:57', '2020-07-06 10:52:57');
INSERT INTO `livein` VALUES ('19595666', '101', '11', '1', '300.00', '1', '1', '2020-07-06 11:14:50', '2020-07-06 11:14:50');
INSERT INTO `livein` VALUES ('19595667', '117', '12', '1', '300.00', '1', '1', '2020-07-06 13:56:28', '2020-07-06 13:56:28');
INSERT INTO `livein` VALUES ('19595668', '101', '4', '1', '300.00', '1', '1', '2020-07-06 14:08:08', '2020-07-06 14:08:08');

-- ----------------------------
-- Table structure for `login_status`
-- ----------------------------
DROP TABLE IF EXISTS `login_status`;
CREATE TABLE `login_status` (
  `s_id` int(11) NOT NULL AUTO_INCREMENT,
  `state_no` int(11) DEFAULT NULL,
  `state` varchar(20) DEFAULT NULL,
  `u_name` varchar(20) DEFAULT NULL,
  `u_time` datetime DEFAULT NULL,
  PRIMARY KEY (`s_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of login_status
-- ----------------------------
INSERT INTO `login_status` VALUES ('1', '1', '已登录', null, null);

-- ----------------------------
-- Table structure for `p_state`
-- ----------------------------
DROP TABLE IF EXISTS `p_state`;
CREATE TABLE `p_state` (
  `pnumber` int(20) NOT NULL COMMENT '状态编号',
  `state` varchar(20) NOT NULL COMMENT '客户状态',
  PRIMARY KEY (`pnumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of p_state
-- ----------------------------
INSERT INTO `p_state` VALUES ('1', '入住中');
INSERT INTO `p_state` VALUES ('2', '已离店');

-- ----------------------------
-- Table structure for `reserve`
-- ----------------------------
DROP TABLE IF EXISTS `reserve`;
CREATE TABLE `reserve` (
  `booking_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '预定信息单号',
  `r_name` varchar(255) NOT NULL COMMENT '预定客户名称',
  `roomtype` int(11) DEFAULT NULL COMMENT '房间类型编号',
  `r_phone` varchar(255) NOT NULL COMMENT '联系电话',
  `roomid` varchar(255) NOT NULL COMMENT '房间编号',
  `pa_time` date NOT NULL COMMENT '抵达时间',
  `keep_time` int(2) NOT NULL COMMENT '保留时间',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`booking_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of reserve
-- ----------------------------
INSERT INTO `reserve` VALUES ('6', '龙诚', '3', '18687296626', '203', '2020-03-05', '5', '无');
INSERT INTO `reserve` VALUES ('7', '丁宇', '1', '18987264462', '108', '2020-06-06', '3', '无');
INSERT INTO `reserve` VALUES ('8', '银源福', '6', '18687296654', '306', '2020-06-09', '3', '无');
INSERT INTO `reserve` VALUES ('9', '丁宇', '3', '18987264462', '106', '2020-06-06', '3', '');

-- ----------------------------
-- Table structure for `roominfo`
-- ----------------------------
DROP TABLE IF EXISTS `roominfo`;
CREATE TABLE `roominfo` (
  `roomid` int(20) NOT NULL AUTO_INCREMENT COMMENT '房间编号',
  `roomtype` int(20) DEFAULT NULL COMMENT '房间类型编号',
  `state` int(20) DEFAULT NULL COMMENT '房间状态',
  `location` varchar(50) NOT NULL COMMENT '所在区域',
  `room_phone` varchar(11) NOT NULL COMMENT '房间电话',
  PRIMARY KEY (`roomid`)
) ENGINE=InnoDB AUTO_INCREMENT=307 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of roominfo
-- ----------------------------
INSERT INTO `roominfo` VALUES ('101', '1', '1', '一楼', '100001');
INSERT INTO `roominfo` VALUES ('102', '1', '1', '一楼', '100002');
INSERT INTO `roominfo` VALUES ('103', '4', '4', '一楼', '100003');
INSERT INTO `roominfo` VALUES ('104', '2', '1', '一楼', '100004');
INSERT INTO `roominfo` VALUES ('105', '2', '1', '一楼', '100005');
INSERT INTO `roominfo` VALUES ('106', '2', '2', '一楼', '100006');
INSERT INTO `roominfo` VALUES ('107', '1', '1', '一楼', '100006');
INSERT INTO `roominfo` VALUES ('108', '1', '2', '一楼', '100008');
INSERT INTO `roominfo` VALUES ('109', '1', '3', '一楼', '100009');
INSERT INTO `roominfo` VALUES ('110', '1', '1', '一楼', '100010');
INSERT INTO `roominfo` VALUES ('111', '1', '1', '一楼', '100011');
INSERT INTO `roominfo` VALUES ('112', '1', '1', '一楼', '100012');
INSERT INTO `roominfo` VALUES ('113', '1', '1', '一楼', '100013');
INSERT INTO `roominfo` VALUES ('114', '1', '1', '一楼', '100014');
INSERT INTO `roominfo` VALUES ('115', '1', '1', '一楼', '100015');
INSERT INTO `roominfo` VALUES ('116', '1', '1', '一楼', '100016');
INSERT INTO `roominfo` VALUES ('117', '1', '4', '一楼', '100017');
INSERT INTO `roominfo` VALUES ('118', '1', '1', '一楼', '100018');
INSERT INTO `roominfo` VALUES ('119', '1', '1', '一楼', '100019');
INSERT INTO `roominfo` VALUES ('201', '3', '2', '二楼', '200001');
INSERT INTO `roominfo` VALUES ('202', '3', '1', '二楼', '200002');
INSERT INTO `roominfo` VALUES ('203', '3', '2', '二楼', '200003');
INSERT INTO `roominfo` VALUES ('204', '4', '1', '二楼', '200004');
INSERT INTO `roominfo` VALUES ('205', '4', '1', '二楼', '200005');
INSERT INTO `roominfo` VALUES ('206', '4', '3', '二楼', '200006');
INSERT INTO `roominfo` VALUES ('207', '2', '1', '二楼', '200007');
INSERT INTO `roominfo` VALUES ('208', '2', '1', '二楼', '200008');
INSERT INTO `roominfo` VALUES ('209', '2', '3', '二楼', '200009');
INSERT INTO `roominfo` VALUES ('210', '2', '1', '二楼', '200010');
INSERT INTO `roominfo` VALUES ('211', '2', '1', '二楼', '200011');
INSERT INTO `roominfo` VALUES ('212', '2', '1', '二楼', '200012');
INSERT INTO `roominfo` VALUES ('213', '2', '1', '二楼', '200013');
INSERT INTO `roominfo` VALUES ('214', '2', '1', '二楼', '200014');
INSERT INTO `roominfo` VALUES ('215', '2', '1', '二楼', '200015');
INSERT INTO `roominfo` VALUES ('216', '2', '1', '二楼', '200016');
INSERT INTO `roominfo` VALUES ('217', '2', '1', '二楼', '200017');
INSERT INTO `roominfo` VALUES ('218', '2', '1', '二楼', '200018');
INSERT INTO `roominfo` VALUES ('301', '5', '1', '三楼', '300001');
INSERT INTO `roominfo` VALUES ('302', '5', '1', '三楼', '300002');
INSERT INTO `roominfo` VALUES ('303', '5', '1', '三楼', '300003');
INSERT INTO `roominfo` VALUES ('304', '6', '1', '三楼', '300004');
INSERT INTO `roominfo` VALUES ('305', '6', '1', '三楼', '300005');
INSERT INTO `roominfo` VALUES ('306', '6', '1', '三楼', '300006');

-- ----------------------------
-- Table structure for `roomtype`
-- ----------------------------
DROP TABLE IF EXISTS `roomtype`;
CREATE TABLE `roomtype` (
  `roomtype` int(11) NOT NULL AUTO_INCREMENT,
  `r_type` varchar(20) DEFAULT NULL,
  `price` decimal(15,2) DEFAULT NULL,
  `hour_price` decimal(20,2) DEFAULT NULL,
  `bed` int(11) DEFAULT NULL COMMENT '床位数量',
  `whethe` varchar(1) NOT NULL COMMENT '能否按小时计费',
  PRIMARY KEY (`roomtype`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of roomtype
-- ----------------------------
INSERT INTO `roomtype` VALUES ('1', '标准单人间', '300.00', '30.00', '1', '是');
INSERT INTO `roomtype` VALUES ('2', '豪华单人间', '600.00', '60.00', '1', '否');
INSERT INTO `roomtype` VALUES ('3', '标准双人间', '500.00', '50.00', '2', '是');
INSERT INTO `roomtype` VALUES ('4', '豪华双人间', '1000.00', '100.00', '2', '否');
INSERT INTO `roomtype` VALUES ('5', '商务套房', '1200.00', '120.00', '1', '是');
INSERT INTO `roomtype` VALUES ('6', '总统套房', '3000.00', '300.00', '2', '否');

-- ----------------------------
-- Table structure for `r_state`
-- ----------------------------
DROP TABLE IF EXISTS `r_state`;
CREATE TABLE `r_state` (
  `number` int(20) NOT NULL COMMENT '状态编号',
  `state` varchar(20) NOT NULL COMMENT '房间状态',
  PRIMARY KEY (`number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of r_state
-- ----------------------------
INSERT INTO `r_state` VALUES ('1', '可用');
INSERT INTO `r_state` VALUES ('2', '被预订');
INSERT INTO `r_state` VALUES ('3', '维护中');
INSERT INTO `r_state` VALUES ('4', '使用中');

-- ----------------------------
-- Table structure for `userinfo`
-- ----------------------------
DROP TABLE IF EXISTS `userinfo`;
CREATE TABLE `userinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `account` varchar(20) NOT NULL COMMENT '账号',
  `password` varchar(20) NOT NULL COMMENT '密码',
  `usertype` varchar(20) NOT NULL COMMENT '用户类型',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of userinfo
-- ----------------------------
INSERT INTO `userinfo` VALUES ('1', 'admin', '123123', '操作员');
INSERT INTO `userinfo` VALUES ('2', '冯润泽', '123456', '操作员');
INSERT INTO `userinfo` VALUES ('3', '杨明金', '123456', '操作员');
INSERT INTO `userinfo` VALUES ('4', '颜权昌', '123456', '操作员');
INSERT INTO `userinfo` VALUES ('5', '郭超', '123456', '操作员');
